--[[
    退出游戏
]]
local ExitGame =
    class(
    "ExitGame",
    function()
        return display.newLayer()
    end
)
function ExitGame:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initTouch()
    self:init()
end
function ExitGame:init( )
    local bg = display.newSprite("common/bg-u.png")
    bg:addTo(self):align(display.CENTER, display.cx, display.cy)

    local q = display.newTTFLabel({text = "确认退出游戏嘛？", size = 50, x = display.cx, y = display.cy + 100})
    q:addTo(self)

    self.b1 = display.newTTFLabel({text = "取消退出", size = 40, x = display.cx - 80, y = display.cy - 30})
    self.b1:addTo(self)
    self.b2 = display.newTTFLabel({text = "确认退出", size = 40, x = display.cx + 80, y = display.cy - 30})
    self.b2:addTo(self)
end

function ExitGame:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 606
    runScene.index = appdf.getIndex(0)
    runScene.touchTable = {
        [2] = {
            say = "确定退出",
            func = function()
                os.exit(0)
            end
        },
        [1] = {
            say = "取消退出",
            func = function()
                runScene:upCallback()
            end
        }
    }
    tts.say("确认退出游戏嘛")
end
return ExitGame
