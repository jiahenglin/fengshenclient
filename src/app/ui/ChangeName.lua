--[[
    ChangeName
]]
local ChangeName =
    class(
    "ChangeName",
    function()
        return display.newLayer()
    end
)
local MultiPlatform = require("app.external.MultiPlatform")
function ChangeName:ctor(parent)
    self._parent = parent
    self:addTo(self._parent, 20)
    self:initCsb()
    self:initTouch()
    self:show()
end

function ChangeName:initCsb()
    local csbNode = cc.CSLoader:createNode("home/ChangeName.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.name = csbNode:getChildByName("name")
    self.name:setString(GlobalUserItem.nick)
end

function ChangeName:initTouch()
    self._parent.state = 507
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("右滑在昵称区双击可直接粘贴复制内容，修改昵称需消耗100金条")
end

function ChangeName:initTouchTable()
    self._parent.touchTable = {
        {
            say = "当前昵称为：" .. self.name:getString(),
            func = function()
                MultiPlatform:getClipboard()
                tts.say("当前昵称为：" .. ClipboardText)
                self.name:setString(ClipboardText)
                self._parent.touchTable[1].say = "当前昵称为：" .. self.name:getString()
            end
        },
        {
            say = "确认修改，修改昵称需消耗100金条，双击确定",
            func = function()
                if GlobalUserItem.nick == self.name:getString() then
                    tts.say("无修改")
                else
                    mGameWorld:getNetMgr():send({REQUST.RENAMENICK, self.name:getString()})
                end
            end
        }
    }
end

function ChangeName:show()
end

function ChangeName:refresh(data)
    self:show()
    self:initTouchTable()
end

return ChangeName
