--[[
    游戏帮助
]]
local HelpLayer =
    class(
    "HelpLayer",
    function()
        return display.newLayer()
    end
)
function HelpLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initTouch()
    self:init()
end
function HelpLayer:init( )
    local bg = display.newColorLayer(cc.c4b(77, 77, 77, 255))
    bg:addTo(self):align(display.CENTER, display.cx, display.cy)
end
function HelpLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 605
    runScene.index = appdf.getIndex(0)
    runScene.touchTable = {
        [1] = {say = "基本手势操作, 请以听筒在左话筒在右的方式持手机。左右划动切换菜单，双击确认，上划返回上一级菜单，下划回到主界面。"},
        [2] = {say = "战役战斗界面,"..
        "左右划动选择关卡，双击屏幕继续。"..
        "进入人物选择，左右划动进行选择最多选择5人最少选择一人。双击人物可切换出场状态。长按三秒开始战斗。"..
        "进入战斗双击可释放大招，上划可切换播报方式，播报方式有只播报己方血量，只播报敌方血量和全部播报。"..
        "左右划动可以切换想要的英雄释放已积攒好的大招。"},
        [3] = {say = "封神榜界面,"..
        "兑换挑战值-左右划动切换到挑战值选框，双击进入，选择需要兑换的挑战值数量，双击确认兑换。"..
        "划动到好友或者随机匹配界面，双击进入匹配。"..
        "匹配成功进入挑战，90秒内如未选择上阵英雄则视为放弃战斗，自动失败。"..
        "左右划动到封神榜单，双击进入榜单后左右划动可查询当前封神榜排名。"},
        [4] = {say = "灵台界面"..
        "双击进入灵台，"..
        "左右划动选择初级或者高级召唤，双击确认开始召唤。"..
        "召唤结束后可左右划动选择再来一次和返回上一级。"},
        [5] = {say = "英雄界面"..
        "左右划动选择英雄，双击进入英雄属性。"..
       " 左右划动选择升级英雄等级，双击确认升级。"..
        "左右划动选择升级英雄星级，双击确认升级"..
        "左右划动选择增加技能点，双击确认增加。"..
        "穿上装备"..
        "左右划动选择部位，双击进入装备选择。"..
        "左右划动选择装备，双击确认装备成功。"..
        "脱下装备"..
        "上划退回英雄属性界面，选到已经装备的部位，双击脱下装备。"..
        "合成英雄"..
        "左右划动选择已经集满碎片的英雄，双击合成。"},
    }
    for i,v in ipairs(runScene.touchTable) do
        v.func = function()
            print("~~~~~")
        end
    end
    tts.say("封神-万神来朝操作指南")
end
return HelpLayer
