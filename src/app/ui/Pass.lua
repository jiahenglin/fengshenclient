--[[
    -- 父类
    -- 过度播报语音
    -- 时间
    --1 upCallback 2 downCallback
]]
local Pass =
    class(
    "Pass",
    function()
        return display.newLayer()
    end
)
function Pass:ctor(parent, str, time, calltype)
    self._parent = parent
    self:addTo(parent, 20)
    self._str = str
    self._time = time
    self._calltype = calltype
    self:initTouch()
end

function Pass:initTouch()
    local state = self._parent.state
    self._parent.state = 1101
    self._parent.touchTable = {}
    tts.say(self._str)
    self:performWithDelay(
        function()
            self._parent.state = state
            self:removeFromParent()
            if self._calltype == 1 then
                self._parent:upCallback()
            else
                self._parent:downCallback()
            end
        end,
        self._time
    )
end

return Pass
