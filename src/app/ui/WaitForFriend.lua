--[[
    等待好友接受PK请求界面
]]
local WaitForFriend =
    class(
    "WaitForFriend",
    function()
        return display.newLayer()
    end
)

function WaitForFriend:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self:initTouch()
end

function WaitForFriend:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/WaittingLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.text = csbNode:getChildByName("panel"):getChildByName("Text_1")
    self.text:setString("邀请已发送，等待好友回应")
end

function WaitForFriend:initTouch()
    mGameWorld:getSoundMgr():playEff("wait")
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 504
    runScene.index = appdf.getIndex(1)
    runScene.touchTable = {}
    runScene.touchTable = {
        [1] = {
            say = "邀请已发送，等待好友回应, 双击取消",
            func = function()
                runScene:upCallback()
            end
        }
    }
    tts.say("邀请已发送，等待好友回应,双击取消")
end

return WaitForFriend
