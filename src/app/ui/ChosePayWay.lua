--[[
    选择支付方式
]]
local ChosePayWay =
    class(
    "ChosePayWay",
    function()
        return display.newLayer()
    end
)

function ChosePayWay:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self:initTouch()
end

function ChosePayWay:initTouch()
    self._parent.state = 304
    self._parent.index = appdf.getIndex(1)
    self._parent.touchTable = {
        [1] = {
            say = "支付宝支付",
            func = function()
                utils.createPayView(self.data[1], self.data[2], self.data[3], self.data[4], 1)
            end
        },
        -- [2] = {
        --     say = "微信支付",
        --     func = function()
        --         utils.createPayView(self.data[1], self.data[2], self.data[3], self.data[4], 2)
        --     end
        -- }
    }
    tts.say("请选择支付方式")
end

return ChosePayWay
