--[[
    语速设置
]]
local SetSpeed =
    class(
    "SetSpeed",
    function()
        return display.newLayer()
    end
)

function SetSpeed:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initTouch()
    self:init()
end

function SetSpeed:init( )
    local bg = display.newSprite("common/bg-u.png")
    bg:addTo(self):align(display.CENTER, display.cx, display.cy)

    self.txt = display.newTTFLabel({text = "当前语速为"..tts.speed, size = 64})
    self.txt:addTo(self):align(display.CENTER, display.cx, display.cy)
end

function SetSpeed:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 602
    runScene.index = appdf.getIndex(1)
    runScene.touchTable = {
        [1] = {
            func = function()
                runScene:upCallback()
            end,
            left = function()
                tts.speed = tts.speed - 1
                if tts.speed < MIN then
                    tts.speed = MIN
                end
                cc.UserDefault:getInstance():setFloatForKey("speed", tts.speed)
                tts.say("当前语速为" .. tts.speed)
                self.txt:setString("当前语速为" .. tts.speed)
            end,
            right = function()
                tts.speed = tts.speed + 1
                if tts.speed > MAX then
                    tts.speed = MAX
                end
                cc.UserDefault:getInstance():setFloatForKey("speed", tts.speed)
                tts.say("当前语速为" .. tts.speed)
                self.txt:setString("当前语速为" .. tts.speed)
            end
        }
    }
    tts.say("当前语速为" .. tts.speed .. "左右滑动调节语速")
end

return SetSpeed
