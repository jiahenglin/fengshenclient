--[[
    设置背景音乐音效
]]
local SetEffVolume =
    class(
    "SetEffVolume",
    function()
        return display.newLayer()
    end
)

function SetEffVolume:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initTouch()
    self:init()
end


function SetEffVolume:init( )
    local bg = display.newSprite("common/bg-u.png")
    bg:addTo(self):align(display.CENTER, display.cx, display.cy)

    self.txt = display.newTTFLabel({text = "当前音效音量大小为"..mGameWorld:getSoundMgr():getEffVolume(), size = 50})
    self.txt:addTo(self):align(display.CENTER, display.cx, display.cy)
end

function SetEffVolume:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 604
    runScene.index = appdf.getIndex(1)
    runScene.touchTable = {
        [1] = {
            func = function()
                runScene:upCallback()
            end,
            left = function (  )
                local vol = mGameWorld:getSoundMgr():getEffVolume()
                vol = vol - 1
                if vol < 1 then
                    vol = 1
                end
                mGameWorld:getSoundMgr():setEffVolume(vol)
                tts.say("当前音效音量大小为"..vol)
                self.txt:setString("当前音乐音量大小为"..vol)
            end,
            right = function (  )
                local vol = mGameWorld:getSoundMgr():getEffVolume()
                vol = vol + 1
                if vol > 100 then
                    vol = 100
                end
                mGameWorld:getSoundMgr():setEffVolume(vol)
                tts.say("当前音效音量大小为"..vol)
                self.txt:setString("当前音乐音量大小为"..vol)
            end
        }
    }
end

return SetEffVolume
