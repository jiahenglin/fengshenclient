--[[
    ChangeGuildName
]]
local ChangeGuildName =
    class(
    "ChangeGuildName",
    function()
        return display.newLayer()
    end
)
local MultiPlatform = require("app.external.MultiPlatform")
function ChangeGuildName:ctor(parent, data)
    self._parent = parent
    self.data = data
    self:addTo(self._parent, 20)
    self:initCsb()
    self:initTouch()
    self:show()
end

function ChangeGuildName:initCsb()
    local csbNode = cc.CSLoader:createNode("home/ChangeName.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.name = csbNode:getChildByName("name")
    self.name:setString(self.data)
end

function ChangeGuildName:initTouch()
    self._parent.state = 911
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("右滑后再昵称区双击可直接粘贴复制内容，修改公会昵称需消耗100金条")
end

function ChangeGuildName:initTouchTable()
    self._parent.touchTable = {
        {
            say = "当前公会昵称为：" .. self.name:getString(),
            func = function()
                MultiPlatform:getClipboard()
                local tip = ""
                if #ClipboardText > 30 then
                    tip = "长度过长"
                end
                tts.say("当前公会昵称为：" .. ClipboardText .. tip)
                self.name:setString(ClipboardText)
                self._parent.touchTable[1].say = "当前公会昵称为：" .. self.name:getString()
            end
        },
        {
            say = "确认修改，修改昵称需消耗100金条，双击确定",
            func = function()
                if self.data == self.name:getString() then
                    tts.say("无修改")
                else
                    mGameWorld:getNetMgr():send({REQUST.RENAMEFAMILY, self.name:getString()})
                end
            end
        }
    }
end

function ChangeGuildName:show()
end

function ChangeGuildName:refresh(data)
    self:show()
    self:initTouchTable()
end

return ChangeGuildName
