--[[
    设置背景音乐音效
]]
local SetBGMVolume =
    class(
    "SetBGMVolume",
    function()
        return display.newLayer()
    end
)

function SetBGMVolume:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initTouch()
    self:init()
end

function SetBGMVolume:init( )
    local bg = display.newSprite("common/bg-u.png")
    bg:addTo(self):align(display.CENTER, display.cx, display.cy)

    self.txt = display.newTTFLabel({text = "当前音乐音量大小为"..mGameWorld:getSoundMgr():getBGMVolume(), size = 50})
    self.txt:addTo(self):align(display.CENTER, display.cx, display.cy)
end

function SetBGMVolume:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 603
    runScene.index = appdf.getIndex(1)
    runScene.touchTable = {
        [1] = {
            func = function()
                runScene:upCallback()
            end,
            left = function (  )
                local vol = mGameWorld:getSoundMgr():getBGMVolume()
                vol = vol - 1
                if vol < 1 then
                    vol = 1
                end
                mGameWorld:getSoundMgr():setBGMVolume(vol)
                tts.say("当前音乐音量大小为"..vol)
                self.txt:setString("当前音乐音量大小为"..vol)
            end,
            right = function (  )
                local vol = mGameWorld:getSoundMgr():getBGMVolume()
                vol = vol + 1
                if vol > 100 then
                    vol = 100
                end
                mGameWorld:getSoundMgr():setBGMVolume(vol)
                tts.say("当前音乐音量大小为"..vol)
                self.txt:setString("当前音乐音量大小为"..vol)
            end
        }
    }
end

return SetBGMVolume
