--[[
    收到好友PK 邀请
]]
local PKMessage =
    class(
    "PKMessage",
    function()
        return display.newLayer()
    end
)

function PKMessage:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self:initCsb()
    self:initTouch()
end

function PKMessage:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/FriendPk.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.name = csbNode:getChildByName("panel"):getChildByName("name")
    self.name:setString(self.data[2])
end

function PKMessage:initTouch()
    self._parent.state = 505
    self._parent.touchTable = {
        [1] = {
            say = "接受邀请",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.ACCEPTAPPLYFRIENDPK, self.data[1]})
            end
        },
        [2] = {
            say = "拒绝邀请",
            func = function()
                self._parent:upCallback()
            end
        }
    }
    tts.say("好友"..self.data[2].."想要和您打一架")
end

return PKMessage
