

cc.Registry 	=import(".Registry")
cc.GameObject 	=import(".GameObject")
cc.Component 	=import(".Component")

local ccmt = {}
ccmt.__call = function(self,target)
	if target then
		return cc.GameObject.extend(target)
	end
end
setmetatable(cc,ccmt)