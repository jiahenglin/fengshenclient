local NetTcp = class("NetTcp")
local SimpleTCP = require("framework.SimpleTCP")
local cjson = require("cjson")
if device.platform ~= "windows" then
    local lz4 = require("lz4")
end
require("app.utils.bit")

NetTcp.EVENT_RECEIVEDATA = "onTcpReceive"
NetTcp.EVENT_SERVER_CLOSED = "onServerClosed"
NetTcp.EVENT_SERVER_CONNECTED = "onServerConnected"

function NetTcp:ctor()
    cc(self):addComponent("app.event.EventProtocol"):exportMethods()
    -- dump(cc.Registry.classes_)
    -- dump(self)
    self._recvData = ""
end

function NetTcp:connect(host, port)
    self.stcp = SimpleTCP.new(host, port, handler(self, self.onTCPEvent))
    self.stcp:connect()
end

function NetTcp:onTCPEvent(event, data)
    if event == SimpleTCP.EVENT_DATA then
        print("==receive data:", data)
        print("==receive len:", #data)
        self:receive(data)
    elseif event == SimpleTCP.EVENT_CONNECTING then
        print("==connecting")
    elseif event == SimpleTCP.EVENT_CONNECTED then
        print("==connected")
        self:dispatchEvent({name = NetTcp.EVENT_SERVER_CONNECTED})
    elseif event == SimpleTCP.EVENT_CLOSED then
        print("==closed")
        self:dispatchEvent({name = NetTcp.EVENT_SERVER_CLOSED})
    elseif event == SimpleTCP.EVENT_FAILED then
        print("==failed")
        self:connect(HOST, PORT)
    end
end

function NetTcp:send(data)
    local str = cjson.encode(data)
    local pLen = string.len(str) + 1
    local t = {}
    for i = 4, 1, -1 do
        t[i] = pLen % 256
        pLen = bit.brshift(pLen, 8)
    end
    local ret = string.char(t[1], t[2], t[3], t[4], 0) .. str
    self.stcp:send(ret)
end

function NetTcp:receive(data)
    self._recvData = self._recvData .. data
    local readed = 0
    local total = string.len(self._recvData)
    while true do
        local pLen = 0
        for i = 1, 4 do
            readed = readed + 1
            local ch = string.byte(self._recvData, readed)
            pLen = pLen * 256 + ch
        end
        if total - readed < pLen then
            self._recvData = string.sub(self._recvData, readed - 3)
            break
        end
        readed = readed + 1
        local protocol = string.byte(self._recvData, readed)
        local isEncryp = bit.band(protocol, 64)
        local isCompress = bit.band(protocol, 16)
        local transfer = bit.band(protocol, 1)

        local source = string.sub(self._recvData, readed + 1, readed + pLen - 1)
        readed = readed + pLen - 1
        if isCompress > 0 then
            print("decompress")
            source = lz4.decompress(source)
        end
        local pack = cjson.decode(source)
        self:dispatchEvent({name = NetTcp.EVENT_RECEIVEDATA, params = pack})
        if readed >= total then
            -- dump(pack)
            self._recvData = ""
            break
        end
    end
end

function NetTcp:close()
    self.stcp:close()
end

function NetTcp:isConnected()
    return self.stcp.stat == SimpleTCP.STAT_CONNECTED
end

return NetTcp
