local NetMgr = class("NetMgr")
local mNetTcp = require("app.net.NetTcp")

function NetMgr:ctor()
	self._net = mNetTcp.new()
	self._listenerList = {}
	self._sendBufs = {}

	-- dump(self._net)
	self._net:addEventListener(mNetTcp.EVENT_RECEIVEDATA,function(event) 
		print("~~~~~~~EVENT_RECEIVEDATA~~~~~~")
		local funcName = event.params[1]
		table.remove(event.params,1)
		self:receive(funcName,unpack(event.params))
		end)

	self._net:addEventListener(mNetTcp.EVENT_SERVER_CLOSED,function() 
		print("~~~~~~~~event server closed~~~~~~~~~~~~")
		self:onServerClosed()
		end)

	self._net:addEventListener(mNetTcp.EVENT_SERVER_CONNECTED,function() 
		print("~~~~~~~~event server connected~~~~~~~~~~~~")
		self:onServerConnected()
		end)
end

function NetMgr:connect()
	self._net:connect(HOST,PORT)
end

function NetMgr:close()
	self._net:close()
end

function NetMgr:isConnceted()
	self._net:isConnected()
end

function NetMgr:onServerClosed()
	mGameWorld:getTimeMgr():stopTick()
	mGameWorld:getEventMgr():fire(EventConst.EVENT_SERVER_CLOSED)
end

function NetMgr:onServerConnected()
	mGameWorld:getEventMgr():fire(EventConst.EVENT_SERVER_CONNECTED)
end

function NetMgr:addRegist(funcName,callback)
	self._listenerList[funcName] = self._listenerList[funcName] or {}
	table.insert(self._listenerList[funcName],callback)
end

function NetMgr:remRegist(funcName,callback)
	if self._listenerList[funcName] == nil then return end
	for k,cb in pairs(self._listenerList[funcName]) do
		if cb == callback then
			table.remove(self._listenerList[funcName],k)
		end
	end
end

function NetMgr:send(data)
	if self._net:isConnected() then
		while #self._sendBufs>0 do
			self._net:send(table.remove(self._sendBufs,1))
		end
		self._net:send(data)
	else
		table.insert(self._sendBufs,data)
	end
end

function NetMgr:receive(funcName,...)
	if self._listenerList[funcName] == nil then return end
	for _,cb in pairs(self._listenerList[funcName]) do
		cb(...)
	end
end

return NetMgr