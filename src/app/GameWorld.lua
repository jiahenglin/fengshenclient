local GameWorld = class("GameWorld")

local mNetMgr = require("app.net.NetMgr")
local mSoundMgr = require("app.manager.SoundManager")
local mEventMgr = require("app.manager.EventManager")
local mTimeMgr = require("app.manager.TimeManager")


function GameWorld:ctor()
	self._netmgr = nil
	self._soundmgr = nil
	self._timemgr = nil
end

function GameWorld:getNetMgr()
	if self._netmgr == nil then
		self._netmgr = mNetMgr.new()
	end
	return self._netmgr
end

function GameWorld:getSoundMgr()
	if self._soundmgr == nil then
		self._soundmgr = mSoundMgr.new()
	end
	return self._soundmgr
end

function GameWorld:getEventMgr()
	if self._eventmgr == nil then
		self._eventmgr = mEventMgr.new()
	end
	return self._eventmgr
end

function GameWorld:getTimeMgr()
	if self._timemgr == nil then
		self._timemgr = mTimeMgr.new()
	end
	return self._timemgr
end

return GameWorld