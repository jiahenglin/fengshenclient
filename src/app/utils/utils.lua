do

local function create_webview(url,pos,size,callback)
    utils.changeViewDirection(2)
	local web = ccexp.WebView:create()
	web:setPosition(pos)
    web:setContentSize(size)
    web:loadURL(url)
    web:setJavascriptInterfaceScheme("lua")
    web:setOnJSCallback(function(sender,str)
        local data = string.split(str,"//")
        callback(sender,data[2])
    end)
    return web
end

local function webview_callback(data)
    local deviceId = device:getOpenUDID();
    print("~~~~~~@@@@@~~~~~~",REQUST.REGISTER, deviceId, data, "password", 1, "1")
    mGameWorld:getNetMgr():send({REQUST.REGISTER, deviceId, data, "password", 1, "1"})
end

local function create_payview(name,price,uid,pid,type)
    if device.platform == "android" then
        luaj.callStaticMethod("org/cocos2dx/lua/AppActivity", "pay", {name,price,uid,pid,type}, "(Ljava/lang/String;IIII)V")
    elseif device.platform == "ios" then
        luaoc.callStaticMethod("RootViewController", "pay", {name=name,price=price,uid=uid,pid=pid,type=type})
    end 
end

local function payview_callback(data)
    print("~~~@@@@~~~payview_callback~~")
    dump(data)
end

--1=LANDSCAPE 2=PORTRAIT
local function changeviewdirection(dir)
    luaj.callStaticMethod("org/cocos2dx/lua/AppActivity", "changeViewDirection", {dir}, "(I)V")
end

utils = {
	createWebView = create_webview,
    WebViewCallback = webview_callback,
    createPayView = create_payview,
    PayViewCallback = payview_callback,
    changeViewDirection = changeviewdirection,
}

function ThirdPartLoginSuccess(data)
    local deviceId = device:getOpenUDID()
    print("~~~~~~@@@@@~~~~~~",REQUST.REGISTER, deviceId, data, "password", 1, "1")
    mGameWorld:getNetMgr():send({REQUST.REGISTER, deviceId, data, "password", 1, "1"})
end

end