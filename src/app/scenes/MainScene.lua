
local MainScene = class("MainScene", function()
    return display.newScene("MainScene")
end)
local mGameData = require("app.template.gamedata")

function MainScene:ctor()
    display.newTTFLabel({text = "Hello, World", size = 64})
        :align(display.CENTER, display.cx, display.cy)
        :addTo(self)

    local tts = require "tts"
    tts.speech("123121321",0.1)
    -- audio.loadFile("tts_sample.ogg",function(path,succ) 
    -- 	print(path,succ)
    -- 	end)
    -- local sharedEngine = cc.SimpleAudioEngine:getInstance()
    -- dump(sharedEngine)

    dump(mGameData.data["monster"][1])

    mGameWorld:getNetMgr():connect()

    self:performWithDelay(function()
    	mGameWorld:getNetMgr():send({REQUST.TEST,"aaa"})
    	-- tts.speech(GAME_WORDS[1])
        tts.speech("双卡双待纳斯达克你收到了你看")


        mGameWorld:getSoundMgr():playEff("hero_chongheihu")

        end,3)
        
    self.web = WebView:create();
    self.web:pos(display.width/2,display.height/2)
    self.web:setContentSize(cc.size(display.width,display.height))
    self.web:loadUrl("http://114.55.170.29/fengshen/index.html");
    self.web:setJavascriptInterfaceScheme("lua");
    self.web:setOnJSCallback(function(...)
        dump({...})
    end);
    self.web:addTo(self)


    self.events = {}

end

function MainScene:onEnter()
	mGameWorld:getNetMgr():addRegist(RESPONSE.TEST,handler(self,self.onTest))
    self.events[1] = mGameWorld:getEventMgr():addEvent(EventConst.EVENT_SERVER_CLOSED,function() 
        print("~~~~~~~EVENT_SERVER_CLOSED~~~~~~~~~~")
        end)	
end

function MainScene:onExit()
	mGameWorld:getNetMgr():remRegist(RESPONSE.TEST,handler(self,self.onTest))

    for _,evt in pairs(self.events) do
        mGameWorld:getEventMgr():remEvent(evt)
    end
    self.events={}
end

function MainScene:onTest(data)
	dump(data)
end

return MainScene
