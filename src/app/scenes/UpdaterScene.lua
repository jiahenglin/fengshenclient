local Updater = require("app.utils.Updater")

-- hot update scene
local UpdaterScene =
    class(
    "UpdaterScene",
    function()
        return display.newScene("UpdaterScene")
    end
)

function UpdaterScene:ctor()
    -- call init before initUI
    Updater.init(
        "UpdaterScene",
        "http://114.55.170.29/update/update",
        function(code, param1, param2)
            print(code, param1, param2)
            if 1 == code then
                app:enterScene("LoadingScene")
            elseif 2 == code then -- 更新中
                self.loadingBar:setPercent(param2 / param1 * 100)
                self.loadingText:setString(param2 / param1 * 100 .. "%")
            elseif 6 == code then
                tts.say("请至官方下载最新的客户端，当前版本可能无法进行正常游戏")
            elseif 7 == code then
                tts.say("当前网络不可用")
            end
            -- TODO other code deal
        end
    )
    self:initUI() -- init Loading scene UI
end

function UpdaterScene:initUI()
    local csbNode = cc.CSLoader:createNode("update/UpdateScene.csb")
    csbNode:addTo(self)
    csbNode:setPosition(display.cx, display.cy)
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.loadingBar = csbNode:getChildByName("LoadingBg"):getChildByName("LoadingBar")
    self.loadingText = csbNode:getChildByName("LoadingBg"):getChildByName("LoadingBar"):getChildByName("text")
end

return UpdaterScene
