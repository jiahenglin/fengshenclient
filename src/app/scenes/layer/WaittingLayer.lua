--[[
    任务
]]
local WaittingLayer =
    class(
    "WaittingLayer",
    function()
        return display.newLayer()
    end
)

function WaittingLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self:initTouch()
end

function WaittingLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/WaittingLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
end

function WaittingLayer:initTouch()
    mGameWorld:getSoundMgr():playEff("wait")
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 404
    runScene.index = appdf.getIndex(1)
    runScene.touchTable = {}
    runScene.touchTable = {
        [1] = {
            say = "正在匹配实力相当的对手，双击取消匹配",
            func = function()
                runScene:upCallback()
            end
        }
    }
    tts.say("正在匹配实力相当的对手，双击取消匹配")
end

return WaittingLayer
