--[[
    EquipLayer
]]
local EquipLayer =
    class(
    "EquipLayer",
    function()
        return display.newLayer()
    end
)

function EquipLayer:ctor(parent)
    self._parent = parent
    self:addTo(self._parent, 20)
    self:initCsb()
    self:initTouch()
    self:show()
end

function EquipLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("equip/EquipLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
end

function EquipLayer:initTouch()
    self._parent.state = 1011
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("欢迎来到万工坊")
end

function EquipLayer:initTouchTable()
    self._parent.touchTable = {
        -- {
        --     say = "当前金币" .. GlobalUserItem.gold,
        --     func = function()
        --         print("金币")
        --     end
        -- },
        -- {
        --     say = "当前强化石数量",
        --     func = function()
        --         print("当前强化石数量")
        --     end
        -- },
        {
            say = "宝石合成",
            func = function()
                MAKESTONE = true
                mGameWorld:getNetMgr():send({REQUST.GETSTONELIST})
            end
        }
    }
    local equipList = {"头部装备", "饰品装备", "武器装备", "盾牌装备", "身体装备", "腿部装备"}

    for i, v in ipairs(equipList) do
        local t = {
            say = v,
            func = function()
                MAKEEQUIP = true
                EQUIP_NUM = i
                mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, i})
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function EquipLayer:show()
end

function EquipLayer:refresh(data)
    self:show()
    self:initTouchTable()
end

return EquipLayer
