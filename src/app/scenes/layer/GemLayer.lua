--[[
    兑换体力
]]
local GemLayer =
    class(
    "GemLayer",
    function()
        return display.newLayer()
    end
)
local ChosePayWay = require("app.ui.ChosePayWay")
local mGameData = require("app.template.gamedata")
function GemLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self.typeList = {4} -- 集市页面的 itemtype
    self:initCsb()
    self._H = 4 -- 横排数量
    self._V = 2 -- 竖排数量
    self:getShopData()
    self:show()
    self:initTouch()
end

function GemLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("shop/GemLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageList")
    self.gem = csbNode:getChildByName("box"):getChildByName("text_left")
    self.gold = csbNode:getChildByName("box2"):getChildByName("text_left")
end

function GemLayer:initTouch()
    self.gem:setString(GlobalUserItem.gem)
    self.gold:setString(GlobalUserItem.gold)
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.index = appdf.getIndex(0)
    runScene.state = 302
    runScene.touchTable[1] = {
        say = "当前金条数" .. GlobalUserItem.gem,
        func = function()
            print("兑换金条")
        end
    }
    runScene.touchTable[2] = {
        say = "当前金币数" .. GlobalUserItem.gold,
        func = function()
            print("兑换金条")
        end
    }
    for i, v in ipairs(self.data) do
        local t = {
            say = v["name"] .. "," .. v["rmb"] .. "元,"..v["des"],
            func = function()
                runScene.currentLayer[#runScene.currentLayer + 1] = true
                runScene.chosePayWay = ChosePayWay.new(runScene, {v.name, v.rmb, GlobalUserItem.id, i})
                runScene.currentLayer[#runScene.currentLayer] = runScene.chosePayWay
            end
        }
        table.insert(runScene.touchTable, t)
    end
    tts.say("当前可以兑换金条,左右滑动查看商品吧")
end

function GemLayer:show()
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = appdf.createPayItem(itemData) 
                local h = math.ceil(j / 4)
                local w = j - (h - 1) * 4
                local x = 1070 * ((w + w - 1) / 8)
                local y = (4 - 2 * h + 1) / 4 * 488
                print("x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function GemLayer:getShopData()
    local bIn = function(value)
        for i, v in ipairs(self.typeList) do
            if v == value then
                return true
            end
        end
        return false
    end
    local store = mGameData.data["store"]
    local data = {}
    for i, v in ipairs(store) do
        if bIn(v["itemtype"]) then
            table.insert(data, v)
        end
    end
    self.data = mGameData.data["charge"]
end

return GemLayer
