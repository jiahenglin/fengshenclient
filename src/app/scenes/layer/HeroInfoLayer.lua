--[[
    英雄信息
]]
local HeroInfoLayer =
    class(
    "HeroInfoLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function HeroInfoLayer:ctor(parent, data)
    -- dump(data)
    self.stype = {"攻击型", "速度型", "血厚型"}
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    HEROID = data[10]
    self:initCsb()
    self:show()
    if GlobalUserItem.guid_pont ~= 100 then
        self:initGuidTouch()
    else
        self:initTouch()
    end
end

function HeroInfoLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("herobag/HeroInfo.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    local bg = csbNode:getChildByName("bg")
    self.bg = bg
    self.text_name = bg:getChildByName("text_name")
    self.text_star = bg:getChildByName("text_star")
    self.text_lv = bg:getChildByName("text_lv")
    self.text_exp = bg:getChildByName("text_exp")
    self.text_atk = bg:getChildByName("text_atk")
    self.text_skillname = bg:getChildByName("text_skillname")
    self.text_skillpoint = bg:getChildByName("text_skillpoint")
    self.head = bg:getChildByName("ckb1")
    self.body = bg:getChildByName("ckb2")
    self.shoes = bg:getChildByName("ckb3")
    self.jewelry = bg:getChildByName("ckb4")
    self.shield = bg:getChildByName("ckb5")
    self.weapon = bg:getChildByName("ckb6")
    self.text_type = bg:getChildByName("stype")
    self.text_quality = bg:getChildByName("quality")
    self.hp = bg:getChildByName("hp")
    self.atk = bg:getChildByName("atk")
    self.def = bg:getChildByName("def")
    self.speed = bg:getChildByName("speed")
    local head = appdf.createHeroHead(self.data[11])
    head:setScale(2)
    head:setPosition(cc.p(200, 480))
    head:addTo(self.bg)
end

function HeroInfoLayer:show()
    local data = self.data
    self.text_name:setString(data[1])
    self.text_lv:setString(data[2])
    self.text_exp:setString(data[3])
    self.text_star:setString(data[4] .. "星")
    self.text_atk:setString(data[5])
    self.text_skillname:setString(mGameData.data["skill_zs"][data[6]].name)
    self.text_type:setString(self.stype[mGameData.data["hero"][data[11]].stype])
    self.text_skillpoint:setString(data[7])
    self.hp:setString(self.data[12])
    self.atk:setString(self.data[13])
    self.def:setString(self.data[14])
    self.speed:setString(self.data[15])
    self.textTable = {}
    self.funcTable = {}
    for i, v in ipairs(data[9]) do
        local ckb = self.bg:getChildByName("ckb" .. i)
        local text = ckb:getChildByName("Text_21")
        local say = ""
        local func
        if v == 0 then
            ckb:setSelected(false)
            text:setString("无装备")
            say = "无装备"
            func = function()
                MAKEEQUIP = false
                mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, i})
                EQUIP_NUM = i
            end
        elseif v == 1 then
            ckb:setSelected(false)
            text:setString("可装备")
            say = "可装备"
            func = function()
                MAKEEQUIP = false
                mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, i})
                EQUIP_NUM = i
            end
        else
            local name = mGameData.data["equip"][v[1]].name
            ckb:setSelected(true)
            text:setString("已装备")
            local level = v[10] or 0
            say = "已装备" .. name .. "强化等级" .. level .. "伤害" .. v[4] .. "防御" .. v[5] .. "生命值" .. v[6] .. "法力值" .. v[7] .. "速度" .. v[8]
            func = function()
                mGameWorld:getNetMgr():send({REQUST.TAKEOFFEQUIP, HEROID, v[1]})
                EQUIP_NUM = i
            end
        end
        table.insert(self.textTable, say)
        table.insert(self.funcTable, func)
    end
end

function HeroInfoLayer:initTouch()
    tts.say("当前在英雄详情,右滑动查看英雄各个属性")
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
end

function HeroInfoLayer:initGuidTouch()
    tts.say("进入姬昌英雄详情，右滑看出各个属性吧")
    local SKILLEXP = {50, 60, 90, 140, 200, 280, 0}
    local skilllv = mGameData.data["skill"][self.data[6]].lv
    local QUA = {"初级", "中级", "高级", "稀有"}
    local quality = mGameData.data["hero"][self.data[11]].quality
    self.text_quality:setString(QUA[quality])
    local exp = "已最高"
    local lv = "已最高"
    local con_num = ""
    local itemcnt = ""
    if self.data[2] + 1 < 75 then
        exp = "升级需要" .. mGameData.data["exp"][self.data[2] + 1].exp .. "经验,"
        itemcnt = mGameData.data["exp"][self.data[2] + 1].itemcnt .. "命之源。"
    end
    if self.data[4] < 3 then
        lv = "升星需要达到" .. mGameData.data["star"][self.data[4] + 1]["con_lv"] .. "等级，"
        con_num = "英雄碎片" .. mGameData.data["star"][self.data[4] + 1]["con_num" .. quality] .. "个,当前拥有碎片" .. self.data[16]
    end
    self._parent.state = 11
    self._parent.touchTable = {}
    self._parent.index = appdf.getIndex(0)
    local guid = function()
        tts.say("右滑查看更多详情吧")
    end
    self._parent.touchTable = {
        [1] = {
            say = self.data[1] .. ",品质：" .. QUA[quality] .. "," .. self.text_type:getString(),
            func = guid
        },
        [2] = {
            say = "当前等级：" .. self.data[2] .. "级," .. exp .. itemcnt .. ",当前累计经验" .. self.data[3],
            func = guid
        },
        [3] = {
            say = "当前星级：" .. self.data[4] .. "星," .. lv .. con_num,
            func = guid
        },
        [4] = {
            say = "血量:" .. self.data[12] .. ",攻击：" .. self.data[13] .. ",防御：" .. self.data[14] .. ",速度：" .. self.data[15] .. ",战斗力" .. self.data[5],
            func = guid
        },
        [5] = {
            say = "升级（双击为英雄升级）",
            func = guid
        },
        [6] = {
            say = "升星（双击为英雄升星）",
            func = guid
        },
        [7] = {
            say = "技能" .. self.text_skillname:getString() .. "," .. mGameData.data["skill_zs"][self.data[6]].des,
            func = guid
        },
        [8] = {
            say = "，技能等级" .. skilllv .. ",升级需" .. (SKILLEXP[skilllv] - self.data[7]) .. "技能点" .. " 当前技能点" .. self.data[7] .. ",双击加技能点",
            func = guid
        }
    }
    local equip = {"头部装备", "饰品装备", "武器装备", "盾牌装备", "身体装备", "腿部装备"}
    for i = 1, 6 do
        local n = {
            say = equip[i] .. "双击可编辑该部位装备",
            func = guid
        }
        if i == 6 then
            n = {
                say = equip[i] .. "双击可编辑该部位装备，以上就是英雄详情的所有内容啦，下滑返回主菜单吧",
                func = function()
                    print("以上就是英雄详情的所有内容啦，下滑返回主菜单吧")
                end
            }
        end
        table.insert(self._parent.touchTable, n)
    end
end

function HeroInfoLayer:initTouchTable()
    self._parent.currentHeroId = self.data[10]
    local QUA = {"初级", "中级", "高级", "稀有"}
    local SKILLEXP = {50, 60, 90, 140, 200, 280, 0}
    local skilllv = mGameData.data["skill_zs"][self.data[6]].lv
    local quality = mGameData.data["hero"][self.data[11]].quality
    self.text_quality:setString(QUA[quality])
    local exp = "已最高"
    local lv = "已最高"
    local con_num = ""
    local itemcnt = ""
    if self.data[2] + 1 < 75 then
        exp = "升级需要" .. mGameData.data["exp"][self.data[2] + 1].exp .. "经验,"
        itemcnt = mGameData.data["exp"][self.data[2] + 1].itemcnt .. "命之源。"
    end
    if self.data[4] < 3 then
        lv = "升星需要达到" .. mGameData.data["star"][self.data[4] + 1]["con_lv"] .. "等级，"
        con_num = "英雄碎片" .. mGameData.data["star"][self.data[4] + 1]["con_num" .. quality] .. "个,当前拥有碎片" .. self.data[16]
    end

    self._parent.state = 11
    self._parent.touchTable = {}
    self._parent.touchTable = {
        [1] = {
            say = self.data[1] .. ",品质：" .. QUA[quality] .. "," .. self.text_type:getString(),
            func = function()
                print("英雄名——品质——类型")
            end
        },
        [2] = {
            say = "当前等级：" .. self.data[2] .. "级," .. exp .. itemcnt .. ",当前累计经验" .. self.data[3],
            func = function()
                print(" 等级")
            end
        },
        [3] = {
            say = "当前星级：" .. self.data[4] .. "星," .. lv .. con_num,
            func = function()
                print("星级")
            end
        },
        [4] = {
            say = "血量:" .. self.data[12] .. ",攻击：" .. self.data[13] .. ",防御：" .. self.data[14] .. ",速度：" .. self.data[15] .. ",战斗力" .. self.data[5],
            func = function()
                print("属性战斗力")
            end
        },
        [5] = {
            say = "升级（双击为英雄升级）",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.UPHEROLEVEL, self.data[10]})
            end
        },
        [6] = {
            say = "升星（双击为英雄升星）",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.UPHEROSTAR, self.data[10]})
            end
        },
        [7] = {
            say = "技能" .. self.text_skillname:getString() .. "," .. mGameData.data["skill_zs"][self.data[6]].des,
            func = function()
                print("技能")
            end
        },
        [8] = {
            say = "，技能等级" .. skilllv .. ",升级需" .. (SKILLEXP[skilllv] - self.data[7]) .. "技能点" .. " 当前技能点" .. self.data[7] .. ",双击加技能点",
            func = function()
                if skilllv ~= 7 then
                    mGameWorld:getNetMgr():send({REQUST.ADDHEROSKILLPOINT, self.data[10]})
                else
                    tts.say("技能等级上限")
                end
            end
        }
    }
    local equip = {"头部装备", "饰品装备", "武器装备", "盾牌装备", "身体装备", "腿部装备"}
    for i = 1, 6 do
        table.insert(
            self._parent.touchTable,
            {
                say = equip[i] .. self.textTable[i],
                func = self.funcTable[i]
            }
        )
    end
end

function HeroInfoLayer:refresh(data)
    self.data = data
    self:show()
    self:initTouchTable()
end

return HeroInfoLayer
