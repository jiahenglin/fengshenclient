--[[
    公会菜单
]]
local GuildMenu = class("GuildMenu", function (  )
    return display.newLayer()
end)
local GuildSearch = require("app.scenes.layer.GuildSearch")

function GuildMenu:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    -- self:initCsb()
    self:initTouch()
end

function GuildMenu:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildMenu.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
end

function GuildMenu:initTouch( )
    self._parent.touchTable = {}
    self._parent.state = 901
    self._parent.index = appdf.getIndex(0)
    self._parent.touchTable = {
        [1] = {say = "搜索公会", func = function (  )
            self._parent.currentLayer[#self._parent.currentLayer + 1] = true
            self._parent.guildSearch = GuildSearch.new(self._parent)
            self._parent.currentLayer[#self._parent.currentLayer] = self._parent.guildSearch
        end},
        [2] = {say = "公会列表", func = function (  )
            mGameWorld:getNetMgr():send({REQUST.GETFAMILYLIST})
        end}
    }
    tts.say("您未加入公会，请选择加入喜欢的公会")
end

return GuildMenu