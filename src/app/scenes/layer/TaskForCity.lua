--[[
    TaskForCity
]]
local TaskForCity =
    class(
    "TaskForCity",
    function()
        return display.newLayer()
    end
)

local mGameData = require("app.template.gamedata")
function TaskForCity:ctor(parent, data)
    self._parent = parent
    self.data = data
    self._H = 1
    self._V = 5
    self:addTo(self._parent, 20)
    self:initCsb()
    self:dealData()
    self:initTouch()
    self:show()
end

function TaskForCity:initCsb()
    local csbNode = cc.CSLoader:createNode("task/DayTask.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageList")
end

function TaskForCity:initTouch()
    self._parent.state = 33
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
end
-- -1=金条0=金币1=灵魂碎片2=装备3=物品4=体力5=战斗值6=技能点

function TaskForCity:initTouchTable()
    self._parent.touchTable = {}
    for i, v in ipairs(self.data) do
        local index = math.ceil(i / (self._H * self._V))
        local state = ""
        if v.state == 1 then
            state = "完成,双击领取奖励"
        elseif v.state == 2 then
            state = "已领取"
        elseif v.state == 0 then
            state = "未完成"
        end
        local award = "任务奖励："
        if v.itemtype == -1 then
            award = award .. "金条数量" .. v.itemnum
        elseif v.itemtype == 0 then
            award = award .. "金币数量" .. v.itemnum
        elseif v.itemtype == 1 then
            award = award .. mGameData.data["hero"][v.itemid].name .. "英雄碎片数量" .. v.itemnum
        elseif v.itemtype == 2 then
            award = award .. "装备" .. mGameData.data["equip"][v.itemid].name .. "数量" .. v.itemnum
        elseif v.itemtype == 3 then
            award = award .. mGameData.data["item"][v.itemid].name .. "数量" .. v.itemnum
        elseif v.itemtype == 4 then
            award = award .. "体力值" .. v.itemnum
        elseif v.itemtype == 5 then
            award = award .. "战斗值" .. v.itemnum
        elseif v.itemtype == 6 then
            award = award .. "技能点" .. v.itemnum
        end
        local t = {
            say = v.name .. "," .. award .. "." .. state,
            func = function()
                if v.state == 1 then
                    mGameWorld:getNetMgr():send({REQUST.FETCHCITYTASKAWARD, v.id})
                elseif v.state == 2 then
                    tts.say("已领取")
                elseif v.state == 0 then
                    tts.say(state)
                end
            end,
            touch = function()
                self.pageList:scrollToPage(index - 1)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function TaskForCity:show()
    self.pageList:removeAllPages()
    local pageW = self.pageList:getContentSize().width
    local pageH = self.pageList:getContentSize().height
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(750, 550))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = pageW * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * pageH
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end
function TaskForCity:createItem(data)
    local statestr = ""
    if data.state == 1 then
        statestr = "完成,双击领取奖励"
    elseif data.state == 2 then
        statestr = "已领取"
    elseif data.state == 0 then
        statestr = "未完成"
    end
    local award = "任务奖励："
    if data.itemtype == -1 then
        award = award .. "金条数量" .. data.itemnum
    elseif data.itemtype == 0 then
        award = award .. "金币数量" .. data.itemnum
    elseif data.itemtype == 1 then
        award = award .. mGameData.data["hero"][data.itemid].name .. "英雄碎片数量" .. data.itemnum
    elseif data.itemtype == 2 then
        award = award .. "装备" .. mGameData.data["equip"][data.itemid].name .. "数量" .. data.itemnum
    elseif data.itemtype == 3 then
        award = award .. mGameData.data["item"][data.itemid].name .. "数量" .. data.itemnum
    elseif data.itemtype == 4 then
        award = award .. "体力值" .. data.itemnum
    elseif data.itemtype == 5 then
        award = award .. "战斗值" .. data.itemnum
    elseif data.itemtype == 6 then
        award = award .. "技能点" .. data.itemnum
    end
    local item = cc.CSLoader:createNode("task/TaskItem.csb")
    local name = item:getChildByName("item"):getChildByName("name")
    local state = item:getChildByName("item"):getChildByName("state")
    name:setString(data.name .. "\n" .. award)
    state:setString(statestr)
    return item
end
function TaskForCity:dealData()
    local taskdata = mGameData.data["task_city"]
    for i, v in ipairs(taskdata) do
        v["state"] = 0
        for k, t in ipairs(self.data) do
            if v.id == t[1] then
                v["state"] = t[3]
            end
        end
    end
    local function sortA(a, b)
        local r
        local a1 = tonumber(a.state)
        local b1 = tonumber(b.state)
        local a3 = tonumber(a.id)
        local b3 = tonumber(b.id)
        if a1 == 1 then
            a1 = 3
        end
        if b1 == 1 then
            b1 = 3
        end
        if a1 == b1 then
            r = a3 < b3
        else
            r = a1 > b1
        end
        return r
    end
    self.data = taskdata
    table.sort(self.data, sortA)
    dump(self.data)
end

function TaskForCity:refresh(data)
    self.data = data
    self:dealData()
    self:show()
    self:initTouchTable()
end

return TaskForCity
