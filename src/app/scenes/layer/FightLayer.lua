local FightLayer =
    class(
    "FightLayer",
    function()
        return display.newLayer()
    end
)
local Hero = require("app.const.Hero")
local Monster = require("app.const.Monster")
local mGameData = require("app.template.gamedata")
local LEFT_POS = {
    cc.p(-94, -50 - 150),
    cc.p(-94, 150 - 150),
    cc.p(-255, -50 - 150),
    cc.p(-255, 150 - 150),
    cc.p(-410, 80 - 150)
}
local RIGHT_POS = {
    cc.p(94, -50 - 150),
    cc.p(94, 150 - 150),
    cc.p(255, -50 - 150),
    cc.p(255, 150 - 150),
    cc.p(410, 80 - 150)
}
local SCALE = 0.4
local ROT = 180

function FightLayer:ctor(parent, data)
    mGameWorld:getSoundMgr():playBGM("fight_bg")
    self._parent = parent
    self._parent.touchTable = {}
    self._parent.battleInfo = ""
    self.hero = {} -- 英雄对象
    self.monster = {}
    --怪物对象
    self.delay = 0
    self._csbNode = cc.CSLoader:createNode("fight/FightLayer.csb")
    local csbNode = self._csbNode
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.blood = csbNode:getChildByName("blood")
    self:start(data)
end

function FightLayer:start(data)
    -- dump(data)
    local fighttype = data["type"]
    FIGHTTYPE = fighttype
    self._round = 0
    self._mid = false
    local alldata = data["teams"]
    local hero = {}
    local monster = {}
    for i, v in ipairs(alldata) do
        if v[1] == GlobalUserItem.id then
            table.insert(hero, v)
        else
            table.insert(monster, v)
        end
    end
    -- 试炼战场
    if data["type"] == 5 then
        for i, v in ipairs(monster) do
            v[4] = 1
            v[5] = mGameData.data["shilian"][v[5]]["itemid"]
            mGameWorld:getSoundMgr():playEff("hero_begin" .. v[5])
            v[7] = v[6]
        end
    end
    for i, v in ipairs(hero) do
        self.hero[i] = Hero.new(v)
        self.hero[i]:setScale(SCALE):setPosition(LEFT_POS[i]):setAnchorPoint(cc.p(0.5, 0))
        -- self.hero[i].hpView:setRotationSkewY(ROT)
        self._csbNode:addChild(self.hero[i])
        --测试代码
        -- self._parent.touchTable[i] = self.hero[i]
        --测试代码
    end
    local l = #self.hero
    local headBg = self._csbNode:getChildByName("hero_box")
    for i, v in ipairs(self.hero) do
        v.head:addTo(headBg):setPosition(cc.p(1024 * i / (l + 1), 234 / 2))
    end
    headBg:setVisible(false)

    for i, v in ipairs(monster) do
        if v[4] == 2 then
            self.monster[i] = Monster.new(v)
        elseif v[4] == 1 then
            self.monster[i] = Hero.new(v)
            self.monster[i]:setRotationSkewY(ROT)
        end
        self.monster[i]:setScale(SCALE):setPosition(RIGHT_POS[i]):setAnchorPoint(cc.p(0.5, 0))
        self._csbNode:addChild(self.monster[i])
    end
    self.maxHp = {0, 0}
    for i, v in ipairs(self.hero) do
        self.maxHp[1] = self.maxHp[1] + self.hero[i].hp
    end
    self.mh = self.maxHp[1]
    for i, v in ipairs(self.monster) do
        self.maxHp[2] = self.maxHp[2] + self.monster[i].hp
    end
    local a = {
        ",前位,",
        ",坤位,",
        ",巽位,",
        ",坎位,",
        ",离位,"
    }
    local str = "战斗开始,"
    if self._parent.bpvp then
        str = str .. "对方阵型,"
        for i, v in ipairs(data["formation"]) do
            if v[2] ~= GlobalUserItem.id then
                if v[1] == 0 then
                    str = str .. "未选择"
                else
                    for k, t in ipairs(mGameData.data["formation"]) do
                    end
                    str = str .. mGameData.data["formation"][v[1]].name
                end
            end
        end
        for i, v in ipairs(monster) do
            if v[1] == -1 then
                v[13] = i
            end
        end
        for i, v in ipairs(a) do
            local b = a[i] .. "无英雄"
            for k, m in ipairs(monster) do
                if m[13] == i then
                    b = a[i] .. m[10] .. "等级," .. m[11]
                end
            end
            str = str .. b
        end
    end
    tts.say(str)
end

function FightLayer:getLeftHp()
    local hp = 0
    for i, v in ipairs(self.hero) do
        hp = hp + self.hero[i].hp
    end

    return hp
end
-- 根据位置获得队伍和名字
function FightLayer:posToNameAndTeams(pos)
    local all = {}
    for i, v in ipairs(self.hero) do
        if v.pos == pos then
            return v
        end
    end
    for i, v in ipairs(self.monster) do
        if v.pos == pos then
            return v
        end
    end
    return nil
end
-- 回合战斗
function FightLayer:onAttack(data)
    mGameWorld:getSoundMgr():stopEff()
    self._round = self._round + 1
    local dnum = #data
    local dtargetT = {}
    local hurtT = {}
    local atarget = self:posToNameAndTeams(data[1][1])
    local hurt = data[1][2][1][1][2]
    local bUse = data[1][2][1][1][3]
    for i, v in ipairs(data) do
        local ddata = v[2][1][1]
        local dpos = ddata[1]
        local hurt = ddata[2]
        local dtarget = self:posToNameAndTeams(dpos)
        dtarget:injured(hurt)
        table.insert(dtargetT, dtarget)
        table.insert(hurtT, hurt)
        self.maxHp[dtarget.team] = self.maxHp[dtarget.team] + hurt
    end
    local path = "left"
    if atarget then
        if atarget.team == 2 then
            local x = self.maxHp[dtargetT[1].team] / self.mh
            if x < 0 then
                x = 0
            end
            self.blood:setScale(x, 1)
            path = "right"
        end
        if atarget.type == 1 then
            if bUse == 0 then
                -- 主创英雄
                if tonumber(atarget.id) >= 46 then
                    local a = math.random(1, 2)
                    mGameWorld:getSoundMgr():playEff("hero_atk_" .. path .. atarget.id .. "_" .. a)
                else
                    mGameWorld:getSoundMgr():playEff("hero_atk_" .. path .. atarget.id)
                end
            else
                mGameWorld:getSoundMgr():playEff("hero_skill_" .. path .. atarget.id)
            end
            if tonumber(atarget.id) >= 46 and self.fighttype == 5 and self._round >= 6 then
                if not self._mid then
                    mGameWorld:getSoundMgr():playEff("hero_mid" .. atarget.id)
                    self._mid = true
                end
            end
        else
            local i = math.random(1, 2)
            mGameWorld:getSoundMgr():playEff("monster_" .. i)
        end
        atarget:injured(0)
    end
    local str = self:playMode(hurt, dtargetT)
    local info = self:allInfo(atarget, bUse, hurtT, dtargetT)
    self._parent.battleInfo = self._parent.battleInfo .. info .. "\n"
    for i, v in ipairs(dtargetT) do
        if v.hp <= 0 then
            if v.type == 1 then
                local p = "left"
                if v.team == 2 then
                    p = "right"
                end
                mGameWorld:getSoundMgr():playEff("hero_die_" .. p .. v.id)
            else
                local i = math.random(1, 3)
                print("~~~~~~~~~~~~monsetdie~~~~~~~~~~" .. i)
                mGameWorld:getSoundMgr():playEff("monster_die" .. i)
            end
        end
    end
    if str ~= "" then
    tts.say(str)
    end
end

function FightLayer:playMode(hurt, dtarget)
    local str = ""
    local a = "受到伤害"
    if hurt >= 0 then
        a = "恢复生命"
    end
    if #dtarget <= 3 then
        for i, v in ipairs(dtarget) do
            if v.hp <= 0 then
                v.hp = 0
            end
            if PLAY_MODE == 1 then
                str = str .. v.name .. "剩余血量" .. v.hp .. ","
            elseif PLAY_MODE == 2 then
                if v.team == 1 then
                    str = ""
                else
                    str = v.name .. "剩余血量" .. v.hp .. ","
                end
            else
                if v.team == 1 then
                    str = v.name .. "，剩余血量" .. v.hp .. ","
                else
                    str = ""
                end
            end
        end
    else
        if PLAY_MODE == 1 then
            if dtarget[1].team == 1 then
                a = "我方全体受到伤害"
                if hurt >= 0 then
                    a = "我方全体恢复"
                end
            else
                a = "对方全体受到伤害"
                if hurt >= 0 then
                    a = "敌方全体恢复"
                end
            end
        elseif PLAY_MODE == 2 then
            if dtarget[1].team == 1 then
                a = ""
                if hurt >= 0 then
                    a = ""
                end
            else
                a = "对方全体受到伤害"
                if hurt >= 0 then
                    a = "敌方全体恢复"
                end
            end
        else
            if dtarget[1].team == 1 then
                a = "我方全体受到伤害"
                if hurt >= 0 then
                    a = "我方全体恢复"
                end
            else
                a = ""
                if hurt >= 0 then
                    a = ""
                end
            end
        end
    end
    if str == "" then
        return str
    end
    str = str .. a .. math.abs(-hurt)
    return str
end

function FightLayer:allInfo(atarget, bUse, hurt, dtarget)
    local str = ""
    if atarget.team == 1 then
        str = "[我方]"
    else
        str = "[敌方]"
    end
    if bUse == 0 then
        str = str .. atarget.name .. "普通攻击，"
    else
        str = str .. atarget.name .. "使用技能，"
    end

    for i, v in ipairs(dtarget) do
        local a = "受到伤害"
        if hurt[i] >= 0 then
            a = "恢复生命"
        end
        if v.hp <= 0 then
            v.hp = 0
        end
        str = str .. v.name .. "剩余血量" .. v.hp .. "," .. a .. math.abs(-hurt[i]) .. ";"
    end
    if str == "" then
        return str
    end
    return str
end

--英雄可使用大招
function FightLayer:onCanUseSkill(heroid, skillid)
    for i, v in ipairs(self.hero) do
        if v.heroid == heroid then
            for k, t in ipairs(self._parent.touchTable) do
                if v == t then
                    table.removebyvalue(self._parent.touchTable, t)
                end
            end
            table.insert(self._parent.touchTable, v)
            tts.say(v.name .. "双击可释放大招")
            self.delay = 0
        end
    end
    self._parent.index = #self._parent.touchTable
    print("接收" .. self._parent.touchTable[self._parent.index].name)
end

function FightLayer:useSkill(target)
    print("发送大招")
    mGameWorld:getNetMgr():send({REQUST.USESKILL, target.heroid})
end

return FightLayer
