--[[
    DayGetLayer
]]
local DayGetLayer =
    class(
    "DayGetLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function DayGetLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(self._parent, 20)
    self.data = data
    self:initCsb()
    self:initTouch()
    self:show()
end
-- -1 金条 0 金币 1 英雄碎片  2装备 3物品 4 体力 5 战斗值 6 技能点7 英雄 8阵法9 宝石
function DayGetLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("day/DayGetLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.text = csbNode:getChildByName("text")
end

function DayGetLayer:initTouch()
    self._parent.state = -1
    self._parent.index = appdf.getIndex(1)
    self:initTouchTable()
end

function DayGetLayer:initTouchTable()
    local a = mGameData.data["login_award"][self.data[1]]["day" .. self.data[2]]
    print(a)
    local t = {}
    string.gsub(
        a,
        "[^" .. "|" .. "]+",
        function(w)
            table.insert(t, tonumber(w))
        end
    )
    -- 1 type 2 id 3 number
    local str = ""
    local endstr = "数量" .. t[3]
    if t[1] == -1 then
        str = "金条"
    elseif t[1] == 0 then
        str = "金币"
    elseif t[1] == 1 then
        local name = mGameData.data["hero"][t[2]].name
        str = name .. "碎片"
    elseif t[1] == 2 then
        local name = mGameData.data["equip"][t[2]].name
        str = name
    elseif t[1] == 3 then
        local name = mGameData.data["item"][t[2]].name
        str = name
    elseif t[1] == 4 then
        str = "体力值"
    elseif t[1] == 5 then
        str = "战斗值"
    elseif t[1] == 6 then
        str = "技能点"
    elseif t[1] == 7 then
        local name = mGameData.data["hero"][t[2]].name
        str = name .. "英雄卡牌"
    elseif t[1] == 8 then
        local name = mGameData.data["formation_simple"][t[2]].name
        str = name
    elseif t[1] == 9 then
        local name = mGameData.data["stone"][t[2]].name
        str = name
    end
    str = str .. endstr
    tts.say(str .. "双击领取今日奖励")
    self._parent.touchTable = {
        {
            say = str .. "双击领取今日奖励",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.FETCHLOGINAWARD})
                self._parent:upCallback()
            end
        }
    }
    self.text:setString(str)
end

function DayGetLayer:show()
end

function DayGetLayer:refresh(data)
    self:show()
    self:initTouchTable()
end

return DayGetLayer
