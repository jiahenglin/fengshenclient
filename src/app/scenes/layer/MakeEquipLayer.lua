--[[
    装备穿戴
]]
local MakeEquipLayer =
    class(
    "MakeEquipLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
local EquipInfoLayer = require("app.scenes.layer.EquipInfoLayer")
function MakeEquipLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self._H = 5 -- 横排数量
    self._V = 4 -- 竖排数量
    self.data = data
    self:show()
    self:initTouch()
end

function MakeEquipLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/RankListLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.pageList = csbNode:getChildByName("rankList")
end

function MakeEquipLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.state = 1012
    runScene.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("装备列表，双击可对装备进行强化镶嵌")
end

function MakeEquipLayer:initTouchTable()
    local runScene = cc.Director:getInstance():getRunningScene()
    for i, v in ipairs(self.data) do
        local name = mGameData.data["equip"][v[2][1]].name
        local level = v[2][10] or 0
        local t = {
            say = name .. "强化等级" .. level,
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self._parent.equipInfoLayer = EquipInfoLayer.new(self._parent, v)
                self._parent.currentLayer[#self._parent.currentLayer] = self._parent.equipInfoLayer
            end
        }
        table.insert(runScene.touchTable, t)
    end
end

--刷新
function MakeEquipLayer:refresh(data)
    self.data = data
    self:initTouchTable()
    self:show()
end

function MakeEquipLayer:show()
    self.pageList:removeAllChildren()
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 806 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                -- print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function MakeEquipLayer:createItem(data)
    --1 物品 -- 2装备 -- 碎片
    local item = cc.CSLoader:createNode("common/Equip.csb")
    local img = item:getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    local num = item:getChildByName("bg"):getChildByName("num")
    img:setTexture("equip/" .. math.ceil(data[2][1] / 4) .. ".png")
    num:setString(data[2][2])

    return item
end

return MakeEquipLayer
