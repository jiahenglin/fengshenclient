--[[
    设置页面
]]
local SettingLayer =
    class(
    "SettingLayer",
    function()
        return display.newLayer()
    end
)
local SetSpeed = require("app.ui.SetSpeed")
local SetBGMVolume = require("app.ui.SetBGMVolume")
local SetEffVolume = require("app.ui.SetEffVolume")
local HelpLayer = require("app.ui.HelpLayer")
local ExitGame = require("app.ui.ExitGame")
function SettingLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initTouch()
    self:initCsb()
end

function SettingLayer:initCsb( )
    local csbNode = cc.CSLoader:createNode("setting/SettingLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    
end

function SettingLayer:initTouch()
    self._parent.state = 601
    self._parent.index = appdf.getIndex(0)
    local bgm = mGameWorld:getSoundMgr():getBGMVolume()
    local eff = mGameWorld:getSoundMgr():getEffVolume()
    tts.say("系统设置")
    self._parent.touchTable = {
        [1] = {
            say = "语速设置,当前语速为" .. tts.speed,
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self.setSpeed = SetSpeed.new(self)
                self._parent.currentLayer[#self._parent.currentLayer] = self.setSpeed
            end
        },
        [2] = {
            say = "音乐音量，当前音量为" .. bgm,
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self.setBGMVolume = SetBGMVolume.new(self)
                self._parent.currentLayer[#self._parent.currentLayer] = self.setBGMVolume
            end
        },
        [3] = {
            say = "音效音量, 当前音量为" .. eff,
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self.setEffVolume = SetEffVolume.new(self)
                self._parent.currentLayer[#self._parent.currentLayer ] = self.setEffVolume
            end
        },
        [4] = {
            say = "游戏帮助",
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self.helpLayer = HelpLayer.new(self)
                self._parent.currentLayer[#self._parent.currentLayer] = self.helpLayer
            end
        },
        [5] = {
            say = "退出游戏",
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self.exitGame = ExitGame.new(self)
                self._parent.currentLayer[#self._parent.currentLayer] = self.exitGame
            end
        }
    }
end

return SettingLayer
