--[[
    公会成员列表
]]
local GuildWareShare =
    class(
    "GuildWareShare",
    function()
        return display.newLayer()
    end
)

function GuildWareShare:ctor(parent, data, num)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self.num = num
    self._H = 1
    self._V = 5
    self:initCsb()
    self:initTouch()
    self:show()
end

function GuildWareShare:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildListLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageView")
end

function GuildWareShare:initTouch()
    tts.say("成员列表，双击可为该成员分配" .. self.num .. "金条")
    self._parent.state = 910
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
end

function GuildWareShare:initTouchTable()
    local function sortA(a, b)
        local r
        local a1 = tonumber(a[7])
        local b1 = tonumber(b[7])
        r = a1 > b1
        return r
    end
    table.sort(self.data, sortA)
    self._parent.touchTable = {}
    local sex = {"男", "女"}
    for i, v in ipairs(self.data) do
        local t = {
            say = ",玩家昵称：" .. v[4] .. ",性别：" .. sex[v[5]] .. ",封神榜排名：" .. v[6] .. ",封神榜战绩:" .. v[7],
            func = function()
                mGameWorld:getNetMgr():send({REQUST.DISPATCHFAMILYITEM, self.num, v[1]})
            end,
            touch = function()
                self.pageList:scrollToPage(math.ceil(i / 5) - 1)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function GuildWareShare:show()
    self.pageList:removeAllPages()
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(805, 666))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 805 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function GuildWareShare:createItem(data)
    local item = cc.CSLoader:createNode("guild/PlayerItem.csb")
    local name = item:getChildByName("name")
    local id = item:getChildByName("idbg"):getChildByName("id")
    local gender = item:getChildByName("gender")
    local post = item:getChildByName("post")
    local rank = item:getChildByName("rank")
    local rankpoint = item:getChildByName("rankpoint")
    local sex = {"男", "女"}
    local t = {
        [0] = "会员",
        [1] = "会长"
    }
    name:setString(data[4])
    id:setString(data[1])
    gender:setString(sex[data[5]])
    post:setString(t[data[3]])
    rank:setString(data[6])
    rankpoint:setString(data[7])
    return item
end

function GuildWareShare:refresh(data)
    self.data = data
    self:show()
    self:initTouchTable()
end

return GuildWareShare
