--[[
    战役战斗
]]
local HomeLayer =
    class(
    "HomeLayer",
    function()
        return display.newLayer()
    end
)
local ChangeName = require("app.ui.ChangeName")

function HomeLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self:initTouch()
end

function HomeLayer:initCsb()
    local GENDER = {"男", "女"}
    local csbNode = cc.CSLoader:createNode("home/HomeLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.text_name = csbNode:getChildByName("name")
    self.text_sex = csbNode:getChildByName("sex")
    self.text_id = csbNode:getChildByName("id")
    self.text_phone = csbNode:getChildByName("phone")

    self.text_gold = csbNode:getChildByName("box1"):getChildByName("text")
    self.text_gem = csbNode:getChildByName("box2"):getChildByName("text")
    self.text_skill = csbNode:getChildByName("box3"):getChildByName("text")
    self.power = csbNode:getChildByName("power")

    self.text_name:setString(GlobalUserItem.nick)
    self.text_sex:setString(GENDER[GlobalUserItem.gender])
    self.text_id:setString(GlobalUserItem.id)
    self.text_phone:setString(GlobalUserItem.account)

    self.text_gold:setString(GlobalUserItem.gold)
    self.text_gem:setString(GlobalUserItem.gem)
    self.text_skill:setString(GlobalUserItem.skill_point)
    self.power:setString(GlobalUserItem.power)
end

function HomeLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.index = appdf.getIndex(0)
    runScene.state = 501
    runScene.touchTable = {
        [1] = {
            say = "好友列表",
            func = function()
                -- mGameWorld:getNetMgr():send({REQUST.GETONLINEFRIENDS})
                mGameWorld:getNetMgr():send({REQUST.GETFRIENDS})
            end
        },
        [2] = {
            say = "好友申请",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.GETFRIENDAPPLYS})
            end
        },
        [3] = {
            say = "用户名" .. GlobalUserItem.nick .. "账号ID" .. GlobalUserItem.id .. "手机号" .. GlobalUserItem.account .. "当前服务器 一区 变幻莫测",
            func = function()
                print("个人信息")
            end
        },
        [4] = {
            say = "修改昵称",
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self._parent.changeName = ChangeName.new(self._parent)
                self._parent.currentLayer[#self._parent.currentLayer] = self._parent.changeName
            end
        },
        [5] = {
            say = "战斗力" .. GlobalUserItem.power,
            func = function()
                print("战斗力")
            end
        },
        [6] = {
            say = "金币" .. GlobalUserItem.gold,
            func = function()
                print("金币数")
            end
        },
        [7] = {
            say = "金条" .. GlobalUserItem.gem,
            func = function()
                print("金条数")
            end
        },
        [8] = {
            say = "技能点" .. GlobalUserItem.skill_point,
            func = function()
                print("技能点")
            end
        }
    }
    tts.say("当前为家园,左右滑动查看更多信息")
end

return HomeLayer
