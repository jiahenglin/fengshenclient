--[[
    灵台页面
]]
local SummonLayer =
    class(
    "SummonLayer",
    function()
        return display.newLayer()
    end
)

function SummonLayer:ctor(parent)
    self._parent = parent
    parent.touchTable = {}
    self:addTo(parent, 20)
    self:initCsb()
    self:initTouch()
end

function SummonLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("summon/SummonLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width/a.width, display.height/a.height)
    self.gold1 = csbNode:getChildByName("text_goldCoin")
    self.gold2 = csbNode:getChildByName("text_goldArticle")
    
end

function SummonLayer:initTouch( )
    self.gold1:setString(GlobalUserItem.gold)
    self.gold2:setString(GlobalUserItem.gem)
    if GlobalUserItem.guid_pont ~= 100 then
        self:initGuidTouch()
    else
        self:initNormalTouch()
    end
end
function SummonLayer:initNormalTouch()
    tts.say("当前在灵台召唤，左右滑动切换灵台功能吧")
    self._parent.touchTable = {
        [1] = {
            say = "当前金币数" .. GlobalUserItem.gold,
            func = function()
                print("金币数")
            end
        },
        [2] = {
            say = "当前金条数" .. GlobalUserItem.gem,
            func = function()
                print("金条数")
            end
        },
        [3] = {
            say = "初级召唤-可召唤英雄碎片-消耗500金币；双击开始初级召唤",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.CALLHERO, 1})
            end
        },
        [4] = {
            say = "高级召唤-可直接召唤英雄-消耗80金条；双击开始高级召唤",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.CALLHERO, 2})
            end
        },
        [5] = {
            say = "初级十连抽,必出不少于1枚高级英雄碎片；消耗5000金币",
            func = function (  )
                mGameWorld:getNetMgr():send({REQUST.CALLHEROTEN,1})
                print("CALLHEROTEN")
            end
        },
        [6] = {
            say = "高级十连抽,必出1一个高级英雄；消耗800金条",
            func = function (  )
                mGameWorld:getNetMgr():send({REQUST.CALLHEROTEN,2})
                print("CALLHEROTEN")
            end
        }
    }
    self._parent.index = appdf.getIndex(0)
    self._parent.state = 5
end

function SummonLayer:initGuidTouch()
    self._parent.index = appdf.getIndex(1)
    self._parent.state = 5
    if GlobalUserItem.guid_pont == 5 then
        tts.say("灵台可以召唤英雄或英雄碎片，首次免费，双击召唤吧")
        self._parent.touchTable = {
            [1] = {
                say = "初级召唤-可召唤英雄碎片-首次免费-双击开始召唤",
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.CALLHERO, 1})
                    GlobalUserItem.guid_pont = 6
                end
            }
        }
    else
        tts.say("英雄碎片，是不是不满足，双击进行高级召唤吧，首次免费")
        self._parent.touchTable = {
            [1] = {
                say = "高级召唤-可直接召唤英雄卡牌-首次免费-双击开始召唤",
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.CALLHERO, 2})
                    GlobalUserItem.guid_pont = 7
                end
            }
        }
    end
end

return SummonLayer
