--[[
    公会搜索
]]
local GuildSearch =
    class(
    "GuildSearch",
    function()
        return display.newLayer()
    end
)

function GuildSearch:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self.guidNum = ""
    self:initCsb()
    self:initTouch()
end

function GuildSearch:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildSearch.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.text_num = csbNode:getChildByName("bg"):getChildByName("text_input")
end

function GuildSearch:initTouch()
    self._parent.touchTable = {}
    self._parent.state = 902
    self._parent.index = appdf.getIndex(0)
    for i = 0, 9 do
        local t = {
            say = i.."",
            func = function()
                if #self.guidNum <= 6 then
                    self.guidNum = self.guidNum .. i
                    self.text_num:setString(self.guidNum)
                    tts.say(i.."")
                else
                    tts.say("最多为6位数")
                end
            end
        }
        table.insert(self._parent.touchTable, t)
    end
    table.insert(
        self._parent.touchTable,
        {
            say = "删除",
            func = function()
                self.guidNum = string.sub(self.guidNum, 1, -2)
                self.text_num:setString(self.guidNum)
            end
        }
    )
    table.insert(
        self._parent.touchTable,
        {
            say = "搜索公会",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.GETFAMILYLIST, tonumber(self.guidNum)})
            end
        }
    )
    tts.say("您还未加入公会，请输入公会ID申请加入公会吧")
end

return GuildSearch
