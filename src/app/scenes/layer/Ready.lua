--[[
    发送准备消息后无法操作
]]
local Ready =
    class(
    "Ready",
    function()
        return display.newLayer()
    end
)

function Ready:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:init()
    self:initTouch()
end

function Ready:init( )
    local bg = display.newColorLayer(cc.c4b(0, 0, 0, 180)):addTo(self):align(display.CENTER, display.cx, display.cy)
    display.newTTFLabel({text = "等待对面确认出战英雄", fontSize = 50}):addTo(bg):setPosition(cc.p(display.cx, display.cy))

end

function Ready:initTouch( )
    self._parent.touchTable = {
        {say = "等待对方确认出战英雄", func = function (  )
            print("等待")
        end}
    }
    self._parent.index = appdf.getIndex(1)
    self._parent.state = 402
    tts.say("等待对方确认出战英雄")
end

return Ready
