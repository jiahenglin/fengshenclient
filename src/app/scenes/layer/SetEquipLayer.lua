--[[
    装备穿戴
]]
local SetEquipLayer =
    class(
    "SetEquipLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function SetEquipLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self._H = 5 -- 横排数量
    self._V = 4 -- 竖排数量
    self.data = data
    self:show()
    self:initTouch()
end

function SetEquipLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/RankListLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("rankList")
end

function SetEquipLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.state = 12
    runScene.index = appdf.getIndex(0)
    for i, v in ipairs(self.data) do
        local stone = ""
        local stonedata = mGameData.data["stone"]
        local atk = 0
        local hp = 0
        local speed = 0
        local def = 0
        local mul = {1, 1.2, 1.4, 1.6, 1.8, 2, 3, 3.2, 3.4, 3.6, 3.8, 4, 5, 6, 7, 8, 11, 13, 16, 18, 23, 31, 41, 51, 61}
        local name = mGameData.data["equip"][v[2][1]].name
        local level = v[2][10] or 0
        local rat = 1
        local attribute = "伤害" .. (v[2][4] * rat + atk) .. "防御" .. (v[2][5] * rat + def) .. "生命值" .. (v[2][6] * rat + hp) .. "法力值" .. v[2][7] * rat .. "速度" .. (v[2][8] * rat + speed)
        local name = mGameData.data["equip"][v[2][1]].name
        local level = v[2][10] or 0
        local t = {
            say = name .. "强化等级" .. level .. attribute,
            func = function()
                mGameWorld:getNetMgr():send({REQUST.TAKEONEQUIP, runScene.currentHeroId, v[1]})
            end
        }
        table.insert(runScene.touchTable, t)
    end
    tts.say("可装备列表,右滑查看装备")
end

--刷新
function SetEquipLayer:refresh(data)
    self.data = data
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    for i, v in ipairs(self.data) do
        local name = mGameData.data["equip"][v[2][1]].name
        local t = {
            say = name .. "数量" .. v[2][2] .. "伤害" .. v[2][4] .. "防御" .. v[2][5] .. "生命值" .. v[2][6] .. "法力值" .. v[2][7] .. "速度" .. v[2][8],
            func = function()
                mGameWorld:getNetMgr():send({REQUST.TAKEONEQUIP, runScene.currentHeroId, v[1]})
            end
        }
        table.insert(runScene.touchTable, t)
    end
    if #self.data == 0 then
        runScene.touchTable = {
            {
                say = "无可用装备",
                func = function()
                    print("无装备")
                end
            }
        }
    end
    self:show()
end

function SetEquipLayer:show()
    self.pageList:removeAllChildren()
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 806 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                -- print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function SetEquipLayer:createItem(data)
    --1 物品 -- 2装备 -- 碎片
    local item = cc.CSLoader:createNode("common/Equip.csb")
    local img = item:getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    local num = item:getChildByName("bg"):getChildByName("num")
    img:setTexture("equip/" .. math.ceil(data[2][1] / 4) .. ".png")
    num:setString(data[2][2])

    return item
end

return SetEquipLayer
