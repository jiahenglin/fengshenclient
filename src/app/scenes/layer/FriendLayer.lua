--[[
    好友申请列表
]]
local FriendLayer =
    class(
    "FriendLayer",
    function()
        return display.newLayer()
    end
)

function FriendLayer:ctor(parent, data, type)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self._type = type -- 1 删除好友   2 好友对战
    self._H = 1
    self._V = 5
    self:initCsb()
    self:initTouch()
    self:show()
end

function FriendLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildListLayer.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageView")
end

function FriendLayer:initTouch()
    local str = "好友列表，双击可邀请PK"
    if self._type == 1 then
        str = "好友列表"
    end
    tts.say(str)
    self._parent.state = 503
    self._parent.index = appdf.getIndex(0)

    self:initTouchTable()
end

function FriendLayer:initTouchTable()
    self._parent.touchTable = {}
    local sex = {"男", "女"}
    for i, v in ipairs(self.data) do
        local t = {
            say = "玩家id：" .. v[1] .. ",昵称：" .. v[2] .. ",性别：" .. sex[v[3]],
            func = function()
                if self._type == 1 then
                    -- mGameWorld:getNetMgr():send({REQUST.DELFRIEND, v[1]})
                elseif self._type == 2 then
                    mGameWorld:getNetMgr():send({REQUST.APPLYFRIENDPK, v[1]})
                end
            end
        }
        table.insert(self._parent.touchTable, t)
    end
    if #self._parent.touchTable == 0 then
        self._parent.touchTable[1] = {
            say = "列表为空",
            func = function()
                print("好友列表为空")
            end
        }
    end
end

function FriendLayer:show()
    self.pageList:removeAllPages()
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(805, 666))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 805 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function FriendLayer:createItem(data)
    local sex = {"男", "女"}
    local item = cc.CSLoader:createNode("guild/ApplyItem.csb")
    local name = item:getChildByName("name")
    local id = item:getChildByName("idbg"):getChildByName("id")
    local gender = item:getChildByName("gender")
    local state = item:getChildByName("btn"):getChildByName("tag")
    local text = item:getChildByName("text")
    local rank = item:getChildByName("rank")
    text:setVisible(false)
    rank:setVisible(false)
    state:setString("好友PK")
    name:setString(data[2])
    id:setString(data[1])
    gender:setString(sex[data[3]])
    return item
end

function FriendLayer:refresh(data)
    self.data = data
    self:initTouchTable()
    self:show()
end

return FriendLayer
