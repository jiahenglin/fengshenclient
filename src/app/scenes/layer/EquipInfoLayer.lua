--[[
    EquipInfoLayer
]]
local EquipInfoLayer =
    class(
    "EquipInfoLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function EquipInfoLayer:ctor(parent, data)
    self._parent = parent
    self.data = data
    self:addTo(self._parent, 20)
    self:initCsb()
    self:initTouch()
    self:show()
end

function EquipInfoLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("equip/EquipInfoLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.level = csbNode:getChildByName("level")
    self.attribute = csbNode:getChildByName("attribute")
end

function EquipInfoLayer:initTouch()
    self._parent.state = 1013
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("装备详细信息以及打造菜单")
end

function EquipInfoLayer:initTouchTable()
    local stone = ""
    local stonedata = mGameData.data["stone"]
    local atk = 0
    local hp = 0
    local speed = 0
    local def = 0
    if self.data[2][9] then
        local sd = self.data[2][9]
        if #sd == 0 then
            stone = "装备未打孔"
        end
        for i, v in pairs(sd) do
            if v == 0 then
                stone = stone .. i .. "号孔位无宝石镶嵌"
            else
                stone = stone .. i .. "号孔位镶嵌" .. stonedata[v].name
            end
        end
    else
        stone = "装备未打孔"
    end

    local mul = {1, 1.2, 1.4, 1.6, 1.8, 2, 3, 3.2, 3.4, 3.6, 3.8, 4, 5, 6, 7, 8, 11, 13, 16, 18, 23, 31, 41, 51, 61}
    local name = mGameData.data["equip"][self.data[2][1]].name
    local quality = mGameData.data["equip"][self.data[2][1]].level
    local num = {50, 100, 200, 300}
    local level = self.data[2][10] or 0
    local rat = 1
    local attribute =
        "伤害" ..
        (self.data[2][4] * rat + atk) .. "防御" .. (self.data[2][5] * rat + def) .. "生命值" .. (self.data[2][6] * rat + hp) .. "法力值" .. self.data[2][7] * rat .. "速度" .. (self.data[2][8] * rat + speed)
    self.level:setString(level)
    self.attribute:setString(attribute)
    EQUIPID = self.data[1]
    self._parent.touchTable = {
        {
            say = name .. "强化等级" .. level
        },
        {
            say = attribute
        },
        {
            say = stone
        },
        {
            say = "装备打孔，首个孔消耗五千金币，第二个需要一万金币，第三个需要一万五千金币，装备孔数上限由低到高分别为1个，2个，2个，3个",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.PUNCHEQUIP, self.data[1]})
            end
        },
        {
            say = "装备分解，分解后装备消失，获得一定数量的强化石",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.SPLITEQUIP, self.data[1]})
            end
        },
        {
            say = "强化装备，当前装备强化需要" .. num[quality] .. "个强化石,强化等级越高，成功概率越低，失败后有概率降级",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.FORGEEQUIP, self.data[1]})
            end
        }
    }
    if self.data[2][9] then
        for i, v in ipairs(self.data[2][9]) do
            local t = {
                say = i .. "号孔位宝石镶嵌,镶嵌将会覆盖该装备该空位原有宝石，原有宝石消失",
                func = function()
                    STONEPOS = i
                    MAKESTONE = false
                    mGameWorld:getNetMgr():send({REQUST.GETSTONELIST})
                end
            }
            table.insert(self._parent.touchTable, t)
        end
    end
end

function EquipInfoLayer:show()
    local item = self:createItem(self.data)
    item:addTo(self)
    item:setPosition(cc.p(200, display.height - 200))
end

function EquipInfoLayer:refresh(data)
    self.data = data
    self:show()
    self:initTouchTable()
    self:performWithDelay(
        function()
            tts.say(self._parent.touchTable[1].say)
        end,
        1
    )
end

function EquipInfoLayer:createItem(data)
    --1 物品 -- 2装备 -- 碎片
    local item = cc.CSLoader:createNode("common/Equip.csb")
    local img = item:getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    local num = item:getChildByName("bg"):getChildByName("num")
    img:setTexture("equip/" .. math.ceil(data[2][1] / 4) .. ".png")
    num:setString(data[2][2])

    return item
end

return EquipInfoLayer
