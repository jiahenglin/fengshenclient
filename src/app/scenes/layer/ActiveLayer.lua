--[[
    活动页面
]]
local ActiveLayer =
    class(
    "ActiveLayer",
    function()
        return display.newLayer()
    end
)

local mGameData = require("app.template.gamedata")
function ActiveLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self:initActive()
    self._H = 2
    self._V = 3
    self.data = data
    self:show()
    self:initTouch()
end

function ActiveLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("active/ActiveLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageView")
end
function ActiveLayer:show()
    -- 展示
    self.pageList:removeAllPages()
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1024, 642))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 1024 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 642
                print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function ActiveLayer:createItem(itemData)
    local item = cc.CSLoader:createNode("active/item.csb")
    local text1 = item:getChildByName("text1")
    local text2 = item:getChildByName("text2")
    local state = item:getChildByName("state")
    local a = mGameData.data["activity"][itemData[1]]
    text1:setString(a.name)
    local str = self:getStr(a)
    text2:setString(str)
    state:setVisible(false)
    if itemData[2] == 1 then
        state:setVisible(true)
    end

    return item
end
function ActiveLayer:getStr(a)
    local str = "奖励："
    if a.gold > 0 then
        str = str .. "金币" .. a.gold
    end
    if a.gem > 0 then
        str = str .. "金条" .. a.gem
    end
    if a.exp > 0 then
        str = str .. "经验" .. a.exp
    end
    if a.skillpoint > 0 then
        str = str .. "技能点" .. a.skillpoint
    end
    if a.itemid ~= "" and a.itemnum ~= "" then
        local data = appdf.gsub({a.itemid, a.itemnum}, "|")
        str = str .. appdf.sayGoods(data)
    end
    if a.equipid ~= "" and a.equipnum ~= "" then
        local data = appdf.gsub({a.equipid, a.equipnum}, "|")
        str = str .. appdf.sayEquip(data, "false")
    end
    if a.chipid ~= "" and a.chipnum ~= "" then
        local data = appdf.gsub({a.chipid, a.chipnum}, "|")
        str = str .. appdf.sayHeroChip(data)
    end
    if a.heroid ~= "" then
        local data = {}
        string.gsub(
            a.heroid,
            "[^|]+",
            function(w)
                table.insert(data, w)
            end
        )
        str = str .. appdf.sayHero(data)
    end
    return str
end

function ActiveLayer:initTouch()
    tts.say("活动菜单，左右滑动查看内容")
    self:refreshTouch()
end
function ActiveLayer:refreshTouch()
    self._parent.touchTable = {}
    self._parent.index = appdf.getIndex(0)
    self._parent.state = 501
    local state = {
        "未完成",
        "待领取",
        "已领取"
    }
    for i, v in ipairs(self.ACTIVE) do
        local t = {
            say = v,
            func = function()
                print("活动")
            end
        }
        table.insert(self._parent.touchTable, t)
    end
    for i, v in ipairs(self.data) do
        local a = mGameData.data["activity"][v[1]]
        local str = a.name
        local t = {
            say = str .. self:getStr(a) .. state[v[2] + 1],
            func = function()
                if v[2] == 0 then
                    tts.say("不满足条件")
                elseif v[2] == 1 then
                    mGameWorld:getNetMgr():send({REQUST.FETCHACTIVITAWARD, v[1]})
                end
            end,
            touch = function()
                self.pageList:scrollToPage(math.ceil(i / 6) - 1)
            end
        }
        table.insert(self._parent.touchTable, t)
    end

    -- if #self._parent.touchTable == 0 then
    --     self._parent.touchTable = {[1] = {say = "无相应活动", func = function (  )
    --         print("活动未空")
    --     end}}
    -- end
end

function ActiveLayer:initActive()
    self.ACTIVE = {
        "充值活动，即时起至九月八日，单笔充值一百金条附赠强化石四百个，五百金条附赠4种一级宝石各6个，一千金条附赠史诗头部装备黑砂冠5件，五千金条附赠阵法升级材料各五百个，一万金条附赠英雄崇黑虎，五万金条附赠英雄女娲，赵公明",
        "封神榜冲榜活动一，活动时间：二零一九年八月二十日至八月三十日二十四点。活动内容：各位诸侯自由进行封神榜挑战冲击封神榜单冠军位，三十日二十四点结算刷新时的封神榜冠军将获得奖励。奖励说明：冠军奖励500元现金，第二名至第十名奖励金条1000，第十一至五十名奖励金条600，第五十一至一百名奖励金条300，第一百零一至两百名奖励金条100活动结束次日官方通过微信或支付宝发放。",
        "封神榜冲榜活动二，活动时间：二零一九年八月二十日至八月三十日二十四点。活动内容：公会成员积极参加封神榜挑战，公会成员在封神榜内的战绩总和决定公会在封神榜内的排名，根据十五日二十四点结算刷新时公会排名发放奖励。奖励说明：封神榜前十公会奖励一级公会物资礼包：含3000金条。封神榜十一到二十公会奖励二级公会物资礼包：含1500金条。封神榜二十一到五十公会奖励三级公会物资礼包：含金条800",
        "试炼活动，活动时间：二零一九年八月一日至八月三十一日二十四点。活动内容：各位诸侯每天可以选择一位试炼战场boss进行挑战，一旦选择则当日内不得更换，每日二十四点系统对各位诸侯当日内对该boss造成的累计伤害值进行排名进行奖励。奖励说明：前十名获得当日挑战boss碎片8片，第10至100名获得当日挑战boss碎片6片   第101至500名获得当日挑战boss碎片4片  第501至1000名获得当日挑战boss碎片2片，第1001名及之后获得当日挑战boss碎片1片。"
    }
end

function ActiveLayer:refresh(data)
    self.data = data
    self:refreshTouch()
    self:show()
end

return ActiveLayer
