--[[
    阵法选择
]]
local TroopSetLayer =
    class(
    "TroopSetLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function TroopSetLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    dump(self.data)
    mGameWorld:getSoundMgr():stopEff()
    self:initCsb()
    self:show()
    if GlobalUserItem.guid_pont == 100 then
        self:initTouch()
    else
        self:initGuidTouch()
    end
end

function TroopSetLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("fight/SetHero.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    local down = csbNode:getChildByName("heroUp")
    down:setVisible(false)
    self.troopBox = csbNode:getChildByName("heroBox")
end

function TroopSetLayer:show()
    local a = mGameData.data["formation_simple"]
    local height = 428
    if #self.data >= 11 then
        height = math.ceil(#self.data / 4) * 214
        self.troopBox:setInnerContainerSize(cc.size(1024, height))
    end
    for i, v in ipairs(self.data) do
        local item = cc.CSLoader:createNode("fight/troop.csb")
        local name = item:getChildByName("name")

        name:setString(a[v[1]].name)
        item:addTo(self.troopBox)
        local h = height - 120 / 2 - (math.ceil(i / 4) - 1) * 120
        local w = (((i - (math.ceil(i / 4) - 1) * 4) - 1) * 2 + 1) / 8 * 1024
        item:setPosition(cc.p(w, h))
    end
    self.nochose = cc.CSLoader:createNode("fight/troop.csb")
    local name = self.nochose:getChildByName("name")
    name:setString("不选择")
    self.nochose:addTo(self.troopBox)
    self.nochose:setPosition(cc.p(((((#self.data + 1) - (math.ceil((#self.data + 1) / 4) - 1) * 4) - 1) * 2 + 1) / 8 * 1024, height - 120 / 2 - (math.ceil((#self.data + 1) / 4) - 1) * 120))
end

function TroopSetLayer:initTouch()
    self._parent.state = 1001
    local num = #self.data + 1
    local str = "未选择"
    self._parent.touchTable = {}
    local t = mGameData.data["formation_simple"]
    local u = {"未装备", "装备中"}
    for i, v in ipairs(self.data) do
        if v[4] == 1 then
            num = i
            str = t[v[1]].name
        end
        local str = t[v[1]].name .. "," .. u[v[4] + 1]
        local call = function()
            mGameWorld:getNetMgr():send({REQUST.SELECTFORMATION, v[1]})
            FORMATIONLV = v[2]
        end
        table.insert(self._parent.touchTable, {say = str, func = call})
    end

    self._parent.index = appdf.getIndex(num)

    table.insert(
        self._parent.touchTable,
        {
            say = "不选择",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.SELECTFORMATION, 0})
            end
        }
    )
    local sayStr = "选择阵法，当前选择为" .. str
    if self._parent.bpvp then
        sayStr = "对手," .. ENEMYINFO .. sayStr .. ",90秒时限"
    end
    tts.say(sayStr)
end

function TroopSetLayer:initGuidTouch()
    self._parent.state = 1001
    tts.say("当前为选择阵法界面，双击选择筑基阵配置上阵英雄")
    self._parent.touchTable = {
        {
            say = "当前为选择阵法界面，双击选择筑基阵配置上阵英雄",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.SELECTFORMATION, 1})
            end
        }
    }
end

return TroopSetLayer
