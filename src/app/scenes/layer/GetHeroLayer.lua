--[[
    召唤获得英雄奖励信息
]]
local GetHeroLayer =
    class(
    "GetHeroLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function GetHeroLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self.data = data
    self:show()
    if GlobalUserItem.guid_pont == 100 then
        self:initTouch()
    else
        self:initGuildTouch()
    end
end
function GetHeroLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("summon/GetHeroLayer.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.bt1 = csbNode:getChildByName("bt1") -- 再来一次按钮
    self.bt2 = csbNode:getChildByName("bt2") -- 返回上一页按钮
    self.number = csbNode:getChildByName("number")
    self.text = csbNode:getChildByName("text") -- 文字
end

function GetHeroLayer:show()
    local data = self.data
    local head = appdf.createHeroHead(data[2])
    display.newTTFLabel({text = data[3], x = 130, y = 30}):addTo(head)
    local name = mGameData.data["hero"][data[2]].name
    local str = ""
    local num = 0
    local numstr = ""
    if data[1] == 1 then
        str = "成功获得" .. name .. "碎片" .. data[3] .. "块"
        num = GlobalUserItem.gold
        numstr = "当前金币数" .. num
    else
        str = "成功召唤" .. name .. "卡牌"
        num = GlobalUserItem.gem
        numstr = "当前金条数" .. num
    end
    self.number:setString(num)
    head:addTo(self._csbNode)
    self.text:setString(str)
    self.numstr = numstr
end
function GetHeroLayer:initTouch()
    tts.say(self.text:getString())
    self._parent.touchTable = {}
    self._parent.state = 7
    self._parent.index = appdf.getIndex(0)
    self._parent.touchTable = {
        [1] = {
            say = self.numstr,
            func = function()
                print("金币")
            end
        },
        [2] = {
            say = "再来一次",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.CALLHERO, self.data[1]})
            end
        },
        [3] = {
            say = "返回上一级",
            func = function()
                self._parent:upCallback()
            end
        }
    }
end

function GetHeroLayer:initGuildTouch()
    self._parent.state = 7
    self._parent.touchTable = {}
    if GlobalUserItem.guid_pont == 6 then
        tts.say(self.text:getString() .. ",上滑返回上一页吧")
        self._parent.touchTable = {
            [1] = {
                say = "返回上一页",
                func = function()
                    self._parent:upCallback()
                end
            }
        }
    else
        tts.say(self.text:getString() .. ",下滑返回主菜单")
        self._parent.touchTable = {
            [1] = {
                say = "返回主菜单",
                func = function()
                    self._parent:downCallback()
                end
            }
        }
    end
end

function GetHeroLayer:refresh(data)
    self.data = data
    self:show()
    self:initTouch()
end

return GetHeroLayer
