--[[
    试炼榜
]]
local GuildStoreLayer =
    class(
    "GuildStoreLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function GuildStoreLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self._H = 4 -- 横排数量
    self._V = 2 -- 竖排数量
    self.data = data
    self:show()
    self:initTouch()
end

function GuildStoreLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("shop/ApLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageList")
    self.gem = csbNode:getChildByName("box2"):getChildByName("text_left")
    self.ap = csbNode:getChildByName("box1"):getChildByName("text_left")
    self.img = csbNode:getChildByName("box1"):getChildByName("img")
    self.img:setTexture("shop/ar.png")
    self.gold = csbNode:getChildByName("box3"):getChildByName("text_left")
end

function GuildStoreLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.index = appdf.getIndex(0)
    runScene.state = 908
    self:initTouchTable()
    tts.say("公会商店")
end

function GuildStoreLayer:initTouchTable()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.touchTable[1] = {
        say = "当前金条数" .. GlobalUserItem.gem,
        func = function()
            print("当前金条数")
        end
    }
    runScene.touchTable[2] = {
        say = "当前金币数" .. GlobalUserItem.gold,
        func = function()
            print("当前金币数")
        end
    }
    local money = {"金条", "金币"}
    for i, v in ipairs(self.data) do
        local new = mGameData.data["family_store"][v[1]]
        local info = new["name"] .. "," .. new["des"]
        local have = 0
        if new["moneytype"] == 1 then
            have = GlobalUserItem.gem
        else
            have = GlobalUserItem.gold
        end
        local t = {
            say = info .. "消耗" .. money[new["moneytype"]] .. new["price"] .. ",公会剩余购买次数" .. v[2],
            func = function()
                if v[2] > 0 then
                    if new["price"] > have then
                        local left = new["price"] - have
                        tts.say("当前还差" .. left .. money[new["moneytype"]] .. "才可购买，快去兑换更多" .. money[new["moneytype"]] .. "吧")
                    else
                        mGameWorld:getNetMgr():send({REQUST.BUYFAMILYITEM, v[1]})
                    end
                else
                    tts.say("无法购买，次数不足")
                end
            end
        }
        table.insert(runScene.touchTable, t)
    end
end

function GuildStoreLayer:show()
    self.gem:setString(GlobalUserItem.gem)
    self.gold:setString(GlobalUserItem.gold)
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            if self.data[k] then
                print("family_store")
                local itemData = mGameData.data["family_store"][self.data[k][1]]
                local item = appdf.createShopItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 1070 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 488
                print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function GuildStoreLayer:refresh(data)
    self.data = data
    self:initTouchTable()
end

return GuildStoreLayer
