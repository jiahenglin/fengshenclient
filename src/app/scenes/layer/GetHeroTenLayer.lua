--[[
    召唤获得英雄奖励信息
]]
local GetHeroTenLayer =
    class(
    "GetHeroTenLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function GetHeroTenLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self.data = data
    self:show()
    self:initTouch()
end
function GetHeroTenLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("summon/GetHeroLayer.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.bt1 = csbNode:getChildByName("bt1") -- 再来一次按钮
    self.bt2 = csbNode:getChildByName("bt2") -- 返回上一页按钮
    self.number = csbNode:getChildByName("number")
    self.text = csbNode:getChildByName("text") -- 文字
    self.bt1:setVisible(false)
    self.bt2:setVisible(false)
    self.text:setVisible(false)
    self._bg = csbNode:getChildByName("bg")
end

function GetHeroTenLayer:show()
    local data = self.data
    local num = ""
    local numstr = ""
    if data[1] == 1 then
        num = GlobalUserItem.gold
        numstr = "当前金币数" .. num
    else
        num = GlobalUserItem.gem
        numstr = "当前金条数" .. num
    end
    self.number:setString(num)
    self.numstr = numstr
    for i, v in ipairs(data[2]) do
        local head = appdf.createHeroHead(v[1])
        head:addTo(self._bg)
        display.newTTFLabel({text = v[2], x = 20, y = 30}):addTo(head)
        local h = math.ceil(i / 5)
        local w = i - (h - 1) * 5
        local x = display.width * ((w + w - 1) / 10)
        local y = 0.1 * display.height + (5 - 2 * h + 1) / 5 * display.height * 0.6
        head:setPosition(x, y)
    end
end
function GetHeroTenLayer:initTouch()
    tts.say("召唤成功，右滑看出详情")
    self._parent.touchTable = {}
    self._parent.state = 7
    self._parent.index = appdf.getIndex(0)
    
    self._parent.touchTable = {
        [1] = {
            say = self.numstr,
            func = function()
                print("金币")
            end
        }
    }
    local a = mGameData.data["hero"]
    for i, v in ipairs(self.data[2]) do
        local str = "英雄"
        if self.data[1] == 1 then
            str = "英雄碎片"
        end
        local t = {
            say = a[v[1]].name .. str .. ",数量" .. v[2],
            func = function()
                print(a[v[1]].name)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function GetHeroTenLayer:refresh(data)
    self.data = data
    self:show()
    self:initTouch()
end

return GetHeroTenLayer
