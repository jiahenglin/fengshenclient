--[[
    背包
]]
local BagLayer =
    class(
    "BagLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")

function BagLayer:ctor(parent, data)
    -- dump(data)

    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self.data = data
    self:show()
    self:initTouch()
end

function BagLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("bag/BagLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.bagView = csbNode:getChildByName("bagView")
end

function BagLayer:show()
    self.bagView:removeAllPages()
    local number = #self.data
    for i = 1, math.ceil(number / 20) do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1200, 550))
        for j = 1, 20 do
            local itemData = self.data[(i - 1) * 20 + j]
            if itemData then
                local item = self:createItem(itemData[2][3], itemData[2][1], itemData[2][2])
                local h = math.ceil(j / 5)
                local w = j - (h - 1) * 5
                local x = 806 * ((w + w - 1) / 10)
                local y = (8 - 2 * h + 1) / 8 * 666
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end

        self.bagView:addPage(layout)
    end
end

function BagLayer:createItem(type, id, number)
    --1 物品 -- 2装备 -- 碎片
    local item = cc.CSLoader:createNode("common/Equip.csb")
    local img = item:getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    local num = item:getChildByName("bg"):getChildByName("num")
    local path
    local scale = 1
    if type == 1 then
        path = "item/"
    elseif type == 2 then
        path = "equip/"
        id = math.ceil(id / 4)
    elseif type == 3 then
        path = "hero/hero_"
        scale = 0.5
    elseif type == 4 then
        path = "stone/"
        id = math.ceil(id / 5)
    end
    img:setTexture(path .. id .. ".png"):setScale(scale)
    num:setString(number)
    return item
end

function BagLayer:initTouch()
    self._parent.index = appdf.getIndex(0)
    self._parent.state = 201
    self:initTouchTable()
    tts.say("当前为背包，左右滑动查看背包详情")
end

function BagLayer:initTouchTable()
    self._parent.touchTable = {}
    self._parent.touchTable[1] = {
        say = "一键出售所有一级装备",
        func = function()
            mGameWorld:getNetMgr():send({REQUST.SELLALLEQUIP})
        end
    }
    for i, v in ipairs(self.data) do
        local index = math.ceil(i / 20)
        local str = self:getSay(v[2])
        local t = {
            say = str,
            func = function()
                if v[2][3] == 2 then
                    mGameWorld:getNetMgr():send({REQUST.SELLEQUIP, v[1]})
                end
            end,
            touch = function()
                print("index:" .. index)
                self.bagView:scrollToPage(index - 1)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function BagLayer:getSay(data)
    local str
    local name
    local attribute = ""
    if data[3] == 1 then
        name = mGameData.data["item"][data[1]]["name"] .. "," .. mGameData.data["item"][data[1]]["des"]
    elseif data[3] == 2 then
        name = mGameData.data["equip"][data[1]]["name"] .. "，出售价：" .. mGameData.data["equip"][data[1]]["sell_price"]
    elseif data[3] == 3 then
        name = mGameData.data["hero"][data[1]]["name"] .. "碎片"
    elseif data[3] == 4 then
        name = mGameData.data["stone"][data[1]]["name"]
        local pname = mGameData.data["stone"][data[1]].pname
        local pvalue = mGameData.data["stone"][data[1]].pvalue
        if pname == "atk" then
            attribute = "伤害增加" .. pvalue
        elseif pname == "hp" then
            attribute = "血量增加" .. pvalue
        elseif pname == "speed" then
            attribute = "速度增加" .. pvalue
        elseif pname == "def" then
            attribute = "防御增加" .. pvalue
        end
    end
    str = name .. attribute .. ",数量" .. data[2]
    if #data > 3 then
        str = str .. ",增加攻击力" .. data[4] .. ",增加防御力" .. data[5] .. ",增加血量" .. data[6] .. ",增加法力" .. data[7] .. ",增加速读" .. data[8]
    end
    return str
end

function BagLayer:refresh(data)
    self.data = data
    self:show()
    self:initTouchTable()
end

return BagLayer
