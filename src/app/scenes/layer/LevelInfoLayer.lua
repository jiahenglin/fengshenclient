--[[
    关卡信息
]]
local LevelInfoLayer =
    class(
    "LevelInfoLayer",
    function()
        return display.newLayer()
    end
)

local mGameData = require("app.template.gamedata")
local SetHeroLayer = require("app.scenes.layer.SetHeroLayer")
local ApLayer = require("app.scenes.layer.ApLayer")
function LevelInfoLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.numTable = {"一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二", "十三", "十四", "十五"}
    self.data = data -- 当前关卡数
    self:getInfo()
    self:initCsb()
    self:initTouch()
end

function LevelInfoLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("fight/LevelInfo.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.text_ap = csbNode:getChildByName("ap")
    self.text_title = csbNode:getChildByName("title")
    self.text_power = csbNode:getChildByName("power")
    self.user_ap = csbNode:getChildByName("box1"):getChildByName("text_left") -- 当前体力值
    self.text_gold = csbNode:getChildByName("box2"):getChildByName("text_left") -- 当前金币
    local box = csbNode:getChildByName("box2")
    box:setVisible(false)
    self.user_ap:setString(GlobalUserItem.ap)
    self.text_gold:setString(GlobalUserItem.gold)
    local chapter = math.ceil(self.data / 6)
    local number = self.data - (chapter - 1) * 6
    self.text_title:setString("第" .. self.numTable[chapter] .. "章 第" .. self.numTable[number] .. "关")
    self.text_power:setString(self.powerNumer)
    self.text_ap:setString("5")
    for i, v in ipairs(self.info) do
        local item = self:createItem(v["tid"])
        item:addTo(csbNode)
        item:setPosition(-256 + (i - 1) * 148, -80)
    end
end

function LevelInfoLayer:createItem(id)
    local item = cc.CSLoader:createNode("common/Head.csb")
    local img = item:getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    img:setTexture("monster/" .. id .. ".png")
    return item
end

function LevelInfoLayer:initTouch()
    tts.say("当前是关卡信息，左右滑动查看信息")
    self.user_ap:setString(GlobalUserItem.ap)
    self.text_gold:setString(GlobalUserItem.gold)
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {
        [1] = {
            say = "当前体力值" .. GlobalUserItem.ap .. "双击进入兑换体力",
            func = function()
                runScene.currentLayer[#runScene.currentLayer + 1] = true
                runScene.apLayer = ApLayer.new(runScene)
                runScene.currentLayer[#runScene.currentLayer] = runScene.apLayer
            end
        },
        [2] = {
            say = self.text_title:getString(),
            func = function()
                print("关卡数")
            end
        },
        [3] = {
            say = "消耗体力为 5 敌方阵容为" .. self.nameStr .. "  敌方战力为" .. self.powerNumer .. self:getReward() .. ",双击选择出战英雄卡牌",
            func = function()
                if GlobalUserItem.ap < 5 then
                    tts.say("体力不足")
                else
                    mGameWorld:getNetMgr():send({REQUST.GETALLBATTLEFORMATION})
                end
            end
        }
    }
    runScene.index = appdf.getIndex(0)
    runScene.state = 102
    if GlobalUserItem.guid_pont == 7 then
        runScene.touchTable = {
            {
                say = "当前体力值" .. GlobalUserItem.ap .. self.text_title:getString() .. "消耗体力为 5 敌方阵容为" .. self.nameStr .. "  敌方战力为" .. self.powerNumer .. self:getReward() .. ",双击选择出战英雄卡牌",
                func = function()
                    if GlobalUserItem.ap < 5 then
                        tts.say("体力不足")
                    else
                        mGameWorld:getNetMgr():send({REQUST.GETALLBATTLEFORMATION})
                    end
                end
            }
        }
        runScene.index = 1
        tts.say("关卡信息"..runScene.touchTable[1].say)
    end
    if GlobalUserItem.crusade_point < self.data then
        mGameWorld:getSoundMgr():stopEff()
        local zhang = math.ceil(self.data / 6)
        local jie = self.data % 6
        if jie == 0 then
            jie = 6
        end
        mGameWorld:getSoundMgr():playEff(zhang .. "_jie_" .. jie)
    else
        table.insert(
            runScene.touchTable,
            {
                say = "扫荡",
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.SWEEPCELL, self.data})
                    FIGHTTYPE = 1
                    runScene.fight = self.data
                end
            }
        )
    end
end

function LevelInfoLayer:getInfo()
    self.info = {}
    for i, v in ipairs(mGameData.data["monster"]) do
        if v.blockid == self.data then
            table.insert(self.info, v)
        end
    end
    local power = 0
    local name = ""
    for i, v in ipairs(self.info) do
        power = power + (v.hp + v.def * 2) * 0.3 + v.atk * 0.7
        name = name .. v.name .. ","
    end
    self.nameStr = name
    self.powerNumer = power
end

function LevelInfoLayer:getReward()
    local a = mGameData.data["block_loot"][self.data]
    local str = ",掉落："
    if a.gold > 0 then
        str = str .. "金币" .. a.gold
    end
    if a.exp > 0 then
        str = str .. ",经验" .. a.exp
    end
    if a.skillpoint > 0 then
        str = str .. ",技能点" .. a.skillpoint
    end
    if a.itemid ~= -1 then
        local data = appdf.gsub({a.itemid, a.itemnum}, "|")
        str = str .. "," .. appdf.sayGoods(data)
    end
    if a.equipid ~= -1 then
        local data = appdf.gsub({a.equipid, a.equipnum}, "|")
        str = str .. "," .. appdf.sayEquip(data, "false")
    end
    if a.chipid ~= -1 then
        local data = appdf.gsub({a.chipid, a.chipnum}, "|")
        str = str .. "," .. appdf.sayHeroChip(data)
    end
    return str
end

return LevelInfoLayer
