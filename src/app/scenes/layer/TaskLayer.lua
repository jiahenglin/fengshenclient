--[[
    任务
]]
local TaskLayer =
    class(
    "TaskLayer",
    function()
        return display.newLayer()
    end
)

function TaskLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self:initTouch()
end

function TaskLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("task/TaskLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
end
function TaskLayer:initTouch()
    self._parent.state = 31
    self._parent.index = appdf.getIndex(0)
    self._parent.touchTable = {
        {
            say = "每日任务",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.GETDAILYTASK})
            end
        },
        {
            say = "剧情任务",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.GETCITYTASK})
            end
        }
    }
end
return TaskLayer
