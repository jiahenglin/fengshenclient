--[[
    公会
]]
local GuildLayer =
    class(
    "GuildLayer",
    function()
        return display.newLayer()
    end
)
local MultiPlatform = require("app.external.MultiPlatform")
local GuildMemLayer = require("app.scenes.layer.GuildMemLayer")
local GuildMemManageLayer = require("app.scenes.layer.GuildMemManageLayer")
local ChangeGuildName = require("app.ui.ChangeGuildName")
function GuildLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self:initCsb()
    self:initTouch()
    self:show()
end

function GuildLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.notice = csbNode:getChildByName("notice")
end
function GuildLayer:initTouch( )
    tts.say("公会信息")
    self._parent.touchTable = {}
    self._parent.state = 904
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
end

function GuildLayer:initTouchTable()
    local manage = "退出公会"
    local send = function()
        mGameWorld:getNetMgr():send({REQUST.QUITFAMILY})
    end
    if self.data[2] == GlobalUserItem.id then
        manage = "申请列表"
        send = function()
            mGameWorld:getNetMgr():send({REQUST.GETFAMILYAPPLY})
        end
    end
    self._parent.touchTable = {
        [1] = {
            say = self.data[3],
            func = function()
                print("公会名称")
                if self.data[2] == GlobalUserItem.id then
                    self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                    self._parent.changeGuildName = ChangeGuildName.new(self._parent, self.data[3])
                    self._parent.currentLayer[#self._parent.currentLayer] = self._parent.changeGuildName
                end
            end
        },
        [2] = {
            say = "公告:" .. self.data[6],
            func = function()
                print("公会公告")
                if self.data[2] == GlobalUserItem.id then
                    MultiPlatform:getClipboard()
                    self.notice:setString(ClipboardText)
                    mGameWorld:getNetMgr():send({REQUST.SETFAMILYNOTICE, ClipboardText})
                    tts.say("公告设置：" .. ClipboardText)
                end
            end
        },
        [3] = {
            say = "会长:" .. self.data[4],
            func = function()
                print("会长")
            end
        },
        [4] = {
            say = "公会ID:" .. self.data[1],
            func = function()
                print("公会ID")
            end
        },
        [5] = {
            say = "公会商店",
            func = function()
                print("公会商店")
                mGameWorld:getNetMgr():send({REQUST.GETFAMILYSTORE})
            end
        },
        [6] = {
            say = "公会仓库",
            func = function()
                print("公会仓库")
                mGameWorld:getNetMgr():send({REQUST.GETFAMILYWAREHOUSE})
            end
        },
        [7] = {
            say = "公会成员",
            func = function()
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self._parent.guildMemLayer = GuildMemLayer.new(self._parent, self.data[8])
                self._parent.currentLayer[#self._parent.currentLayer] = self._parent.guildMemLayer
            end
        },
        [8] = {
            say = manage,
            func = send
        }
    }

    if self.data[2] == GlobalUserItem.id then
        table.insert(
            self._parent.touchTable,
            {
                say = "成员管理",
                func = function()
                    print("成员管理")
                    self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                    self._parent.guildMemManageLayer = GuildMemManageLayer.new(self._parent, self.data[8])
                    self._parent.currentLayer[#self._parent.currentLayer] = self._parent.guildMemManageLayer
                end
            }
        )

        table.insert(
            self._parent.touchTable,
            {
                say = "会长金条"..self.data[9],
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.FETCHLEADERGEM, self.data[9]})
                end
            }
        )
    end
end

function GuildLayer:show()
    local name = self._csbNode:getChildByName("name")
    local notice = self._csbNode:getChildByName("notice")
    local leader = self._csbNode:getChildByName("president")
    local id = self._csbNode:getChildByName("id")
    local qq = self._csbNode:getChildByName("qq")
    local isleader = self._csbNode:getChildByName("manage"):getChildByName("text")
    name:setString(self.data[3])
    notice:setString(self.data[6])
    leader:setString(self.data[4])
    id:setString(self.data[1])
    qq:setString("888888")
    if self.data[2] == GlobalUserItem.id then
        isleader:setString("申请列表")
    end
end

function GuildLayer:refresh(data)
    self.data = data
    self:initTouchTable()
    self:show()
end
return GuildLayer
