--[[
    试炼战场
]]
local TrialLayer =
    class(
    "TrialLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
local ShilianLayer = require("app.scenes.layer.ShilianLayer")
local ShilianRankLayer = require("app.scenes.layer.ShilianRankLayer")
function TrialLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self:initCsb()
    self:show()
    self:initTouch()
end

function TrialLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("trial/TrialLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.shiliancnt = csbNode:getChildByName("box1"):getChildByName("text_left")
end

function TrialLayer:initTouch()
    self._parent.state = 10
    self._parent.index = appdf.getIndex(0)
    tts.say("当前在试炼战场")
    self:initTouchTable()
end

function TrialLayer:initTouchTable()
    self.shiliancnt:setString(GlobalUserItem.shiliancnt)
    local runScene = cc.Director:getInstance():getRunningScene()
    self._parent.touchTable = {
        [1] = {
            say = "试炼值:" .. GlobalUserItem.shiliancnt .. "双击可兑换更多试炼值",
            func = function()
                runScene.currentLayer[#runScene.currentLayer + 1] = true
                runScene.shilianLayer = ShilianLayer.new(runScene)
                runScene.currentLayer[#runScene.currentLayer] = runScene.shilianLayer
            end
        }
    }
    if GlobalUserItem.shilianindex == 0 then
        for i, v in ipairs(mGameData.data["shilian"]) do
            local t = {
                say = v.name,
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.FIGHTSHILIAN, v.id})
                    GlobalUserItem.shilianindex = v.id
                end,
                touch = function()
                    mGameWorld:getSoundMgr():stopEff()
                    mGameWorld:getSoundMgr():playEff("hero_say_"..v.itemid)
                end
            }
            table.insert(self._parent.touchTable, t)
        end
    else
        self._parent.touchTable[2] = {
            say = "排行榜",
            func = function()
                local url = "http://114.55.170.29/fengshen/index.php?m=getshilianrank&uid=" .. GlobalUserItem.id .. "&sid=1&slid=" .. GlobalUserItem.shilianindex
                appdf.onHttpJsionTable(
                    url,
                    "GET",
                    function(jsondata)
                        -- dump(jsondata)
                        runScene.currentLayer[#runScene.currentLayer + 1] = true
                        runScene.shilianRankLayer = ShilianRankLayer.new(self, jsondata)
                        runScene.currentLayer[#runScene.currentLayer] = runScene.shilianRankLayer
                    end
                )
            end
        }
        table.insert(
            self._parent.touchTable,
            {
                say = mGameData.data["shilian"][GlobalUserItem.shilianindex]["name"],
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.FIGHTSHILIAN, GlobalUserItem.shilianindex})
                end
            }
        )
    end
end

function TrialLayer:show()
    if GlobalUserItem.shilianindex == 0 then
        for i, v in ipairs(mGameData.data["shilian"]) do
            local btn = display.newSprite("hero/hero_" .. v.itemid .. ".png")
            btn:addTo(self):setScale(0.4)
            btn:setPosition(i / 6 * display.width, display.cy - 100)
        end
    else
        local btn = display.newSprite("hero/hero_" .. mGameData.data["shilian"][GlobalUserItem.shilianindex].itemid .. ".png")
        btn:addTo(self):setScale(1)
        btn:setPosition(display.cx, display.cy - 100)
    end
end

return TrialLayer
