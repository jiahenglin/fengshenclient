--[[
    HappyLayer
]]
local HappyLayer =
    class(
    "HappyLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function HappyLayer:ctor(parent, data)
    self._parent = parent
    self.data = data
    self.isIn = false -- 是否参与
    if data[4].joinTs then
        self.isIn = true
    end
    self:addTo(self._parent, 20)
    self:initCsb()
    self:initTouch()
    self:show()
end

function HappyLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("active/HappyLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.bar = csbNode:getChildByName("cat"):getChildByName("bar")
    self.num = csbNode:getChildByName("cat"):getChildByName("now")
end

function HappyLayer:initTouch()
    self._parent.state = 100001
    self._parent.index = appdf.getIndex(0)
    tts.say("全民攒福气，轻松得三少爷。")
    self:initTouchTable()
end

function HappyLayer:initTouchTable()
    local award = mGameData.data["global_award"]
    self._parent.touchTable = {
        {say = "活动详情,活动期间每充值一元，为全服累计收集一福气值，活动期间参与活动贡献福气值大于一的玩家将获得不同阶段的奖励，更有主创英雄三少爷，新英雄纣王等你来拿"},
        {say = "当前全服累计福气值" .. self.data[1]}
    }
    for i, v in ipairs(award) do
        local str = "全服福气值达到"
        str = str .. v.con
        local gift = appdf.gsub({v.itemtype, v.itemid, v.itemnum}, "|")
        local n = appdf.sayAll(gift)
        local str = str .. "奖励" .. n
        local state = "福气值未达到要求"
        local isget = false
        if self.data[4] and self.data[4]["data"] then
            for k, t in pairs(self.data[4]["data"]) do
                if i == t.id then
                    isget = true
                end
            end
        end
        if self.isIn then
            if self.data[1] >= v.con then
                if isget then
                    state = "已领取"
                else
                    state = "可领取"
                end
            else
                state = "福气值未达到要求"
            end
        else
            if self.data[1] >= v.con then
                state = "参与活动即可领取"
            else
                state = "福气值未达到要求"
            end
        end
        str = str .. "," .. state
        local t = {
            say = str,
            func = function()
                if self.isIn then
                    if self.data[1] >= v.con then
                        if isget then
                            tts.say("已领取该奖励")
                        else
                            mGameWorld:getNetMgr():send({REQUST.FETCHGLOBALAWARD, v.id})
                        end
                    else
                        tts.say("还未达到要求福气值")
                    end
                else
                    tts.say("未参与活动")
                end
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function HappyLayer:show()
    self.bar:setPercent((self.data[1] / 600000) * 100)
    self.num:setString(self.data[1])
    local data = mGameData.data["global_award"]
    for i, v in ipairs(data) do
        local gift = appdf.gsub({v.itemtype, v.itemid, v.itemnum}, "|")
        local str2 = appdf.sayAll(gift)
        local str3 = "不可领取"
        local isget = false
        if self.data[4] and self.data[4]["data"] then
            for k, t in pairs(self.data[4]["data"]) do
                if i == t.id then
                    isget = true
                end
            end
        end
        if self.isIn then
            if self.data[1] >= v.con then
                if isget then
                    str3 = "已领取"
                else
                    str3 = "可领取"
                end
            else
                str3 = "福气值未达到要求"
            end
        else
            if self.data[1] >= v.con then
                str3 = "参与活动即可领取"
            else
                str3 = "福气值未达到要求"
            end
        end
        local str1 = v.con
        local item = self:createItem(str1, str2, str3)
        item:addTo(self)
        item:setPosition(cc.p(display.cx + math.pow(-1, i) * (display.cx - 180), i / 6 * display.height))
    end
end
function HappyLayer:createItem(data1, data2, data3)
    local item = cc.CSLoader:createNode("active/award.csb")
    local bg = item:getChildByName("bg")
    local num = bg:getChildByName("num")
    local award = bg:getChildByName("award")
    local state = bg:getChildByName("state")
    num:setString(data1)
    award:setString(data2)
    state:setString(data3)
    return item
end

function HappyLayer:refresh(data)
    self:show()
    self:initTouchTable()
end

return HappyLayer
