--[[
    好友申请列表
]]
local FriendApplyLayer =
    class(
    "FriendApplyLayer",
    function()
        return display.newLayer()
    end
)

function FriendApplyLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self._H = 1
    self._V = 5
    self:initCsb()
    self:initTouch()
    self:show()
end

function FriendApplyLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildListLayer.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageView")
end

function FriendApplyLayer:initTouch()
    tts.say("好友申请列表，左屏双击同意，右屏双击拒绝")
    
    self._parent.state = 502
    self._parent.index = appdf.getIndex(0)

    self:initTouchTable()
end

function FriendApplyLayer:initTouchTable()
    self._parent.touchTable = {}
    local sex = {"男", "女"}
    for i, v in ipairs(self.data) do
        local t = {
            say = "玩家id：" .. v["id"] .. ",昵称：" .. v["nick"] .. ",性别：" .. sex[v["gender"]],
            leftTouch = function()
                mGameWorld:getNetMgr():send({REQUST.ACCEPTAPPLYFRIEND, v["id"]})
            end,
            rightTouch = function()
                mGameWorld:getNetMgr():send({REQUST.REFUSEAPPLYFRIEND, v["id"]})
            end
        }
        table.insert(self._parent.touchTable, t)
    end
    if #self._parent.touchTable == 0 then
        self._parent.touchTable[1] = {say = "列表为空", func = function (  )
            print("好友列表为空")
        end}
    end
end

function FriendApplyLayer:show()
    self.pageList:removeAllPages()
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(805, 666))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 805 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function FriendApplyLayer:createItem(data)
    local sex = {"男", "女"}
    local item = cc.CSLoader:createNode("guild/ApplyItem.csb")
    local name = item:getChildByName("name")
    local id = item:getChildByName("idbg"):getChildByName("id")
    local gender = item:getChildByName("gender")
    local state = item:getChildByName("btn"):getChildByName("tag")
    local text = item:getChildByName("text")
    local rank = item:getChildByName("rank")
    text:setVisible(false)
    rank:setVisible(false)
    state:setString("同意")
    name:setString(data["nick"])
    id:setString(data["id"])
    gender:setString(sex[data["gender"]])
    return item
end

function FriendApplyLayer:refresh(data)
    self.data = data
    self:initTouchTable()
    self:show()
end

return FriendApplyLayer
