--[[
    GuildWareHouse
]]
local GuildWareHouse =
    class(
    "GuildWareHouse",
    function()
        return display.newLayer()
    end
)
local GuildWareShare = require("app.scenes.layer.GuildWareShare")
function GuildWareHouse:ctor(parent, data)
    self._parent = parent
    self:addTo(self._parent, 20)
    self.data = data
    self.memData = self._parent.guildLayer.data[8] -- 成员数据
    self:initCsb()
    self:initTouch()
    self:show()
end

function GuildWareHouse:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildWareHouse.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.gem = csbNode:getChildByName("gem")
end

function GuildWareHouse:initTouch()
    self._parent.state = 909
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("公会仓库")
end

function GuildWareHouse:initTouchTable()
    self._parent.touchTable = {
        [1] = {
            say = "公会金：" .. self.data,
            func = function()
                print("公会金：")
            end
        }
    }
    local num = {
        50,
        100,
        300,
        500,
        1000
    }
    if self._parent.guildLayer.data[2] == GlobalUserItem.id then
        for i = 1, 5 do
            local t = {
                say = "分配" .. num[i] .. "金条",
                func = function()
                    self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                    self._parent.guildWareShare = GuildWareShare.new(self._parent, self.memData, num[i])
                    self._parent.currentLayer[#self._parent.currentLayer] = self._parent.guildWareShare
                end
            }
            table.insert(self._parent.touchTable, t)
        end
    end
end

function GuildWareHouse:show()
    self.gem:setString(self.data)
end

function GuildWareHouse:refresh(data)
    self:show()
    self:initTouchTable()
end

return GuildWareHouse
