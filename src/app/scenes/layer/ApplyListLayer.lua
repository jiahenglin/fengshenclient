--[[
    申请列表
]]
local ApplyListLayer =
    class(
    "ApplyListLayer",
    function()
        return display.newLayer()
    end
)

function ApplyListLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self._H = 1
    self._V = 5
    self:initCsb()
    self:initTouch()
    self:show()
end

function ApplyListLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildListLayer.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageView")
end

function ApplyListLayer:initTouch()
    tts.say("申请列表，左屏双击同意，右屏双击拒绝")

    self._parent.state = 905
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
end

function ApplyListLayer:initTouchTable()
    self._parent.touchTable = {}
    local t = {"男", "女"}
    for i, v in ipairs(self.data) do
        local t = {
            say = "玩家ID:" .. v[1] .. ",昵称:" .. v[2] .. ",性别:" .. t[v[3]] .. "，封神榜排名：" .. v[4],
            leftTouch = function()
                mGameWorld:getNetMgr():send({REQUST.ACCEPTAPPLYFAMILY, v[1]})
            end,
            rightTouch = function()
                mGameWorld:getNetMgr():send({REQUST.REFUSEAPPLYFAMILY, v[1]})
            end
        }
        table.insert(self._parent.touchTable, t)
    end
    if #self._parent.touchTable == 0 then
        self._parent.touchTable[1] = {
            say = "列表为空",
            func = function()
                print("好友列表为空")
            end
        }
    end
end

function ApplyListLayer:show()
    self.pageList:removeAllPages()
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(750, 550))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 805 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function ApplyListLayer:createItem(data)
    local item = cc.CSLoader:createNode("guild/ApplyItem.csb")
    local name = item:getChildByName("name")
    local gender = item:getChildByName("gender")
    local id = item:getChildByName("idbg"):getChildByName("id")
    local rank = item:getChildByName("rank")
    name:setString(data[2])
    local t = {"男", "女"}
    gender:setString(t[data[3]])
    id:setString(data[1])
    rank:setString(data[4])
    return item
end

function ApplyListLayer:refresh(data)
    self.data = data
    self:show()
    self:initTouchTable()
end

return ApplyListLayer
