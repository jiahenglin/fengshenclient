--[[
    集市
]]
local ShopLayer =
    class(
    "ShopLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
local GemLayer = require("app.scenes.layer.GemLayer")
function ShopLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self.typeList = {0, 1, 2, 3, 9} -- 集市页面的 itemtype
    self:initCsb()
    self._H = 4 --
    self._V = 2 -- 竖排数量
    self:getShopData()
    self:show()
    self:initTouch()
end

function ShopLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("shop/ShopLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageList")
    self.gem = csbNode:getChildByName("box"):getChildByName("text_left")
    self.gold = csbNode:getChildByName("box2"):getChildByName("text_left")
end

function ShopLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.index = appdf.getIndex(0)
    runScene.state = 301
    self:refresh()
    local money = {"金条", "金币"}
    for i, v in ipairs(self.data) do
        local index = math.ceil(i / 8)
        local info = v["name"] .. "," .. v["des"]
        local have = 0
        if v["moneytype"] == 1 then
            have = GlobalUserItem.gem
        else
            have = GlobalUserItem.gold
        end
        local t = {
            say = info .. "消耗" .. money[v["moneytype"]] .. v["price"],
            func = function()
                if v["price"] > have then
                    local left = v["price"] - have
                    tts.say("当前还差" .. left .. money[v["moneytype"]] .. "才可购买，快去兑换更多" .. money[v["moneytype"]] .. "吧")
                else
                    mGameWorld:getNetMgr():send({REQUST.BUYITEM, v["id"]})
                end
            end,
            touch = function()
                print("index:" .. index)
                self.pageList:scrollToPage(index - 1)
            end
        }
        table.insert(runScene.touchTable, t)
    end
    -- tts.say("您来到了集市,左右滑动查看商品吧")
    mGameWorld:getSoundMgr():playEff("xiaoer")
end

function ShopLayer:show()
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = appdf.createShopItem(itemData)
                local h = math.ceil(j / 4)
                local w = j - (h - 1) * 4
                local x = 1070 * ((w + w - 1) / 8)
                local y = (4 - 2 * h + 1) / 4 * 488
                -- print("x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function ShopLayer:getShopData()
    local bIn = function(value)
        for i, v in ipairs(self.typeList) do
            if v == value then
                return true
            end
        end
        return false
    end
    local store = mGameData.data["store"]
    local data = {}
    for i, v in pairs(store) do
        if bIn(v["itemtype"]) then
            table.insert(data, v)
        end
    end
    self.data = data
    local function sortA(a, b)
        local r
        local a1 = a.order
        local b1 = b.order
        r = a1 < b1
        return r
    end
    table.sort(self.data, sortA)
end

function ShopLayer:refresh()
    self.gem:setString(GlobalUserItem.gem)
    self.gold:setString(GlobalUserItem.gold)
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable[1] = {
        say = "当前金条数" .. GlobalUserItem.gem .. "双击兑换更多金条",
        func = function()
            runScene.currentLayer[#runScene.currentLayer + 1] = true
            runScene.gemLayer = GemLayer.new(runScene)
            runScene.currentLayer[#runScene.currentLayer] = runScene.gemLayer
        end
    }
    runScene.touchTable[2] = {
        say = "当前金币数" .. GlobalUserItem.gold,
        func = function()
            print("当前金币数" .. GlobalUserItem.gold)
        end
    }
end

return ShopLayer
