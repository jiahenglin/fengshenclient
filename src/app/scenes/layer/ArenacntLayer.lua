--[[
    挑战值兑换
]]
local ArenacntLayer =
    class(
    "ArenacntLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function ArenacntLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self.typeList = {5} -- 集市页面的 itemtype
    self:initCsb()
    self._H = 4 -- 横排数量
    self._V = 2 -- 竖排数量
    self:getShopData()
    self:show()
    self:initTouch()
end

function ArenacntLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("shop/ApLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageList")
    self.gem = csbNode:getChildByName("box2"):getChildByName("text_left")
    self.ap = csbNode:getChildByName("box1"):getChildByName("text_left")
    self.img = csbNode:getChildByName("box1"):getChildByName("img")
    self.img:setTexture("shop/ar.png")
    self.gold = csbNode:getChildByName("box3"):getChildByName("text_left")
end

function ArenacntLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.index = appdf.getIndex(0)
    runScene.state = 303
    self:refresh()
    local money = {"金条", "金币"}
    for i, v in ipairs(self.data) do
        local info = v["name"] .. "," .. v["des"]
        local have = 0
        if v["moneytype"] == 1 then
            have = GlobalUserItem.gem
        else
            have = GlobalUserItem.gold
        end
        local t = {
            say = info .. "消耗" .. money[v["moneytype"]] .. v["price"],
            func = function()
                if v["price"] > have then
                    local left = v["price"] - have
                    tts.say("当前还差" .. left .. money[v["moneytype"]] .. "才可购买，快去兑换更多" .. money[v["moneytype"]] .. "吧")
                else
                    mGameWorld:getNetMgr():send({REQUST.BUYITEM, v["id"]})
                end
            end
        }
        table.insert(runScene.touchTable, t)
    end
    tts.say("当前可以兑换挑战值,左右滑动查看商品吧")
end

function ArenacntLayer:show()
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = appdf.createShopItem(itemData)
                local h = math.ceil(j / 4)
                local w = j - (h - 1) * 4
                local x = 1070 * ((w + w - 1) / 8)
                local y = (4 - 2 * h + 1) / 4 * 488
                print("x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function ArenacntLayer:getShopData()
    local bIn = function(value)
        for i, v in ipairs(self.typeList) do
            if v == value then
                return true
            end
        end
        return false
    end
    local store = mGameData.data["store"]
    local data = {}
    for i, v in pairs(store) do
        if bIn(v["itemtype"]) then
            table.insert(data, v)
        end
    end
    self.data = data
end

function ArenacntLayer:refresh()
    self.gem:setString(GlobalUserItem.gem)
    self.ap:setString(GlobalUserItem.arenacnt)
    self.gold:setString(GlobalUserItem.gold)
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable[1] = {
        say = "当前挑战值" .. GlobalUserItem.arenacnt,
        func = function()
            print("当前挑战值")
        end
    }
    runScene.touchTable[2] = {
        say = "当前金条数" .. GlobalUserItem.gem,
        func = function()
            print("当前金条数")
        end
    }
    runScene.touchTable[3] = {
        say = "当前金币数" .. GlobalUserItem.gold,
        func = function()
            print("当前金币数")
        end
    }
end

return ArenacntLayer
