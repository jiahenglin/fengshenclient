--[[
    试炼榜
]]
local ShilianRankLayer =
    class(
    "ShilianRankLayer",
    function()
        return display.newLayer()
    end
)
function ShilianRankLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self._H = 1 -- 横排数量
    self._V = 6 -- 竖排数量
    self.data = data
    self:show()
    self:initTouch()
end

function ShilianRankLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/RankListLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.pageList = csbNode:getChildByName("rankList")
end

function ShilianRankLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.index = appdf.getIndex(0)
    runScene.state = 9
    runScene.touchTable[1] = {
        say = "我的排名:" .. (self.data[1][1] + 1) .. ",累计总伤害:" .. self.data[1][3],
        func = function()
            print("我的排名")
        end
    }
    for i, v in ipairs(self.data[2]) do
        local t = {
            say = "第" .. (v[1] + 1) .. "名," .. v[2] .. ",累计总伤害:" .. v[3],
            func = function()
                print("查看战绩i")
            end
        }
        table.insert(runScene.touchTable, t)
    end
    tts.say("试炼榜，右滑看看榜单")
end

function ShilianRankLayer:show()
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[2][k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 806 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                -- print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function ShilianRankLayer:createItem(data)
    local item = cc.CSLoader:createNode("Rank/RankItem.csb")
    local num = item:getChildByName("rank_num")
    local name = item:getChildByName("rank_name")
    local atk = item:getChildByName("rank_atk")
    num:setString(data[1] + 1)
    name:setString(data[2])
    atk:setString(data[3])

    return item
end

return ShilianRankLayer
