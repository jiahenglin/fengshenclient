local HeroLayer =
    class(
    "HeroLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
local TPYE = {
    [0] = "碎片",
    [1] = "英雄"
}
function HeroLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self.All = {}
    self:initData()
end

-- 英雄界面的 触摸控制表
function HeroLayer:initTouch()
    tts.say("当前在英雄菜单,左右滑动选中英雄，双击查看英雄详情")
    self._parent.touchTable = {}
    self._parent.index = appdf.getIndex(0)
    self._parent.state = 4
    local quality = {"初级", "中级", "高级", "稀有"}
    local type = {"攻击型", "速度型", "血厚型"}
    for i, v in ipairs(self.up) do
        table.insert(
            self._parent.touchTable,
            {
                say = v.name .. ",已获得," .. quality[v.quality] .. "," .. type[v.stype],
                jump = function()
                    self.scroll1:jumpToPercentVertical((math.ceil(i / 2) - 1) / (#self.up / 2) * 100)
                end,
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.GETHEROINFO, v.heroid})
                    -- HEROID = v.heroid
                    if GlobalUserItem.guid_pont == 4 then
                        GlobalUserItem.guid_pont = 5
                    end
                end
            }
        )
    end
    table.insert(
        self._parent.touchTable,
        {
            say = "以下英雄未获得",
            func = function()
                print("以下英雄未获得")
            end
        }
    )
    for i, v in ipairs(self.down) do
        local a = mGameData.data["chip"][v.id]
        local bcompose = "还差" .. (a.num - v.number) .. "英雄碎片"
        if v.number >= a.num then
            bcompose = "可合成，双击合成英雄卡牌"
        end

        table.insert(
            self._parent.touchTable,
            {
                say = v.name .. ",未获得" .. quality[v.quality] .. "," .. type[v.stype] .. ",拥有碎片" .. v.number .. "," .. bcompose,
                jump = function()
                    self.scroll2:jumpToPercentVertical((math.ceil(i / 2) - 1) / (#self.down / 2) * 100)
                end,
                func = function()
                    if v.number >= a.num then
                        mGameWorld:getNetMgr():send({REQUST.COMPOSEHERO, v.id})
                    else
                        tts.say("无法合成还差" .. (a.num - v.number) .. "英雄碎片")
                    end
                end
            }
        )
    end
end

function HeroLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("herobag/HeroBag.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width/a.width, display.height/a.height)
    self.scroll1 = csbNode:getChildByName("scroll1")
    self.scroll2 = csbNode:getChildByName("scroll2")
end

function HeroLayer:initGuidTouch()
    tts.say("姬昌已经加入英雄的队列，双击查看英雄详情吧")
    self._parent.touchTable = {}
    self._parent.index = appdf.getIndex(1)
    self._parent.state = 4
    local heroid
    for i, v in ipairs(self.up) do
        if v.id == 6 then
            heroid = v.heroid
        end
    end
    self._parent.touchTable[1] = {
        say = "双击查看姬昌英雄详情",
        func = function()
            mGameWorld:getNetMgr():send({REQUST.GETHEROINFO, heroid})
            GlobalUserItem.guid_pont = 5
        end
    }
end

function HeroLayer:showAll(data)
    self:changeData(data)
    if GlobalUserItem.guid_pont ~= 100 then
        self:initGuidTouch()
    else
        self:initTouch()
    end
    local line = math.ceil(#self.up / 2)
    local line2 = math.ceil(#self.down / 2)
    local height = 168 * line + line * 10 + 1
    local height2 = 168 * line2 + line2 * 10 + 1
    if height < 320 then
        height = 320
    end
    if height2 < 250 then
        height2 = 250
    end
    self.scroll1:setInnerContainerSize(cc.size(800, height))
    self.scroll2:setInnerContainerSize(cc.size(800, height2))

    for i, v in ipairs(self.up) do
        local item = self:createItem(v.id, v.name, v.quality, v.stype)
        self.scroll1:addChild(item)
        local w = i % 2
        if w == 0 then
            w = 2
        end
        local x = 370 / 2 + 20 + (w - 1) * 380
        local y = height - 20 - 168 / 2 - (math.ceil(i / 2) - 1) * 178
        item:setPosition(x, y)
        item:setAnchorPoint(0.5, 0.5)
    end
    for i, v in ipairs(self.down) do
        local item = self:createItem(v.id, v.name, v.quality, v.stype)
        self.scroll2:addChild(item)
        local w = i % 2
        if w == 0 then
            w = 2
        end
        local x = 370 / 2 + 20 + (w - 1) * 380
        local y = height2 - 20 - 168 / 2 - (math.ceil(i / 2) - 1) * 178
        item:setPosition(x, y)
        item:setAnchorPoint(0.5, 0.5)
        cc.DelayTime:create(0.2)
    end
end

function HeroLayer:initData()
    for i, v in ipairs(mGameData.data["hero"]) do
        table.insert(self.All, {["id"] = v.id, ["name"] = v.name, ["quality"] = v.quality, ["type"] = 0, ["number"] = 0, ["lv"] = 0, ["stype"] = v.stype})
    end
end

function HeroLayer:changeData(data)
    for i, v in ipairs(data) do
        for k, t in ipairs(self.All) do
            if v[1] == t.id then
                t.type = v[2]
                t.number = v[4]
                t.heroid = v[3]
                t.lv = v[5]
            end
        end
    end

    self.up = {}
    self.down = {}
    local function sortA(a, b)
        local r
        local a1 = tonumber(a.lv)
        local b1 = tonumber(b.lv)
        local a2 = tonumber(a.quality)
        local b2 = tonumber(b.quality)
        local a3 = tonumber(a.number)
        local b3 = tonumber(b.number)
        if a1 == b1 then
            if a2 == b2 then
                r = a3 > b3
            else
                r = a2 > b2
            end
        else
            r = a1 > b1
        end
        return r
    end
    for i, v in ipairs(self.All) do
        if v.type == 1 then
            table.insert(self.up, v)
        else
            table.insert(self.down, v)
        end
    end
    table.sort(self.up, sortA)
    table.sort(self.down, sortA)
end

function HeroLayer:createItem(id, name, quality, stype)
    local q = {"初级", "中级", "高级", "稀有"}
    local t = {"攻击型", "速度型", "血厚型"}
    local item = cc.CSLoader:createNode("herobag/HeroItem.csb")
    local bg = item:getChildByName("bg")
    local text_name = bg:getChildByName("text_name")
    text_name:setString(name)
    local text_quality = bg:getChildByName("quality")
    local text_stype = bg:getChildByName("text_stype")
    text_quality:setString(q[quality])
    text_stype:setString(t[stype])
    local head = appdf.createHeroHead(id)
    head:addTo(bg)
    head:align(display.CENTER, 85, 85)
    return item
end

return HeroLayer
