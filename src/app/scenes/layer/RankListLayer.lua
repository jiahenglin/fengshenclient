--[[
    挑战值兑换
]]
local RankListLayer =
    class(
    "RankListLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function RankListLayer:ctor(parent, data, type)
    self._parent = parent
    self._type = type
    -- dump(data)
    self:addTo(parent, 20)
    self:initCsb()
    self._H = 1 -- 横排数量
    self._V = 6 -- 竖排数量
    self.data = data[2]
    self.mydata = data[1]
    self:show()
    self:initTouch()
end

function RankListLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/RankListLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.pageList = csbNode:getChildByName("rankList")
end

function RankListLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.touchTable = {}
    runScene.index = appdf.getIndex(0)
    runScene.state = 403

    local str = "封神个人榜，右滑查看榜单信息"
    local my = ""
    local k = "战绩"
    if self._type == 1 then
        str = "封神个人榜，右滑查看榜单信息"
        if self.mydata[1] then
            my = "我的排名：" .. (self.mydata[1] + 1) .. ",战绩" .. self.mydata[3]
        else
            my = "我的排名未上榜"
        end
    elseif self._type == 2 then
        str = "封神公会榜，右滑查看榜单信息"
        if self.mydata[1] then
            my = "我的公会排名：" .. (self.mydata[1] + 1) .. ",战绩" .. self.mydata[3]
        else
            my = "我的公会排名未上榜，或无加入公会"
        end
    elseif self._type == 3 then
        k = "战斗力"
        str = "战斗力排行榜"
        if self.mydata[1] then
            my = "我的排名" .. (self.mydata[1] + 1) .. ",战斗力" .. self.mydata[3]
        else
            my = "我的排名未上榜"
        end
    end
    runScene.touchTable[1] = {
        say = my,
        func = function()
            print("我的排名")
        end
    }
    for i, v in ipairs(self.data) do
        local t = {
            say = "第" .. (v[1] + 1) .. "名," .. v[2] .. k .. v[3],
            func = function()
                print("查看战绩i")
            end,
            touch = function()
                self.pageList:scrollToPage(math.ceil(i / 6) - 1)
            end
        }
        table.insert(runScene.touchTable, t)
    end
    tts.say(str)
end

function RankListLayer:show()
    -- 展示
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1070, 488))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 806 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                -- print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function RankListLayer:createItem(data)
    local item = cc.CSLoader:createNode("Rank/RankItem.csb")
    local num = item:getChildByName("rank_num")
    local name = item:getChildByName("rank_name")
    local atk = item:getChildByName("rank_atk")
    num:setString(data[1] + 1)
    name:setString(data[2])
    atk:setString(data[3])

    return item
end

return RankListLayer
