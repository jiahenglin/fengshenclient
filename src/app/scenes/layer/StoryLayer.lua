--[[
    StoryLayer
]]
local StoryLayer =
    class(
    "StoryLayer",
    function()
        return display.newLayer()
    end
)

local mGameData = require("app.template.gamedata")
function StoryLayer:ctor(parent)
    self._parent = parent
    self:addTo(self._parent, 20)
    self:initCsb()
    self:initTouch()
    self:show()
end

function StoryLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("story/StoryLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width/a.width, display.height/a.height)
    self.text = self._bg:getChildByName("text")
    self.des = {
        "姬昌正在西苑狩猎，听到雷震子说“雷鬼岭出现异状，而且岭下一座村子的野民竟然全都无故消失”，遂决定前往雷鬼岭，看看究竟是何人敢在西岐为非作歹。",
        "在与魔里寿等人交战中，姬昌败退，雷震子忽然消失，一切的线索指向那被混沌能量撕碎的埋没在历史尘埃中的上古战场。",
        "雷震子消失，伯邑考也被抓，刚进入雷鬼岭，队伍就少了两个人，这对姬昌而言，这不是个好消息。周围突然升起迷雾，但好在他们逃进了一个地洞，这里漆黑一片，敌人无法追踪，现在的情况变得对他们有利。",
        "来到了客栈，这间客栈被熟悉的人称为食人客栈，因为弱者在这里无法生存。看着客栈中的客人，姬昌发现，这里潜伏着很多殷商旅人，客栈门口的牌子上写着：有些事情一旦开始，就无法停止。这家客栈到底有着什么样的秘密？",
        "跟踪殷郊来到了夜色镇，因为森林密布，这是一座常年被黑夜笼罩的城镇，整个城市常年亮着幽暗的灯火，传闻这里隐藏着一个神秘的宗教。",
        "随着越来越多的暗月教徒涌来，姬昌等人招架不住，决定逃离这里。却在路上遇到了一伙盗贼。不，这不是盗贼，那是胡升，他又来了。",
        "胡升虽已被打败，但他突然引起的腐朽之力竟没有消逝，反而进入癫狂状态开始吞噬一切生物。难道这个力量并不是源自荆棘山谷？",
        "解除了顺风耳的误会，我们检查了千里眼的伤情，看来还是需要找到这股力量的源头才可以消灭它，我们得抓紧时间了，不能让这股力量在西歧扩大。",
        "在古迹的广场上，殷洪闭眼坐阵法中央，腐朽之力从四面八方汇集过来，环绕在他身上。原来是殷洪激怒了封魔古迹的亡灵，那些腐朽之力便是这些亡灵的力量，一旦被缠绕在身，就会被亡灵控制，被控制的殷洪开始向我们进攻，我们得离开这里。如何才可以让亡灵不敢靠近……",
        "借住哪吒的力量将殷洪救出，听说是殷郊在封魔古迹中拿了一个宝箱，随后整个古迹的亡灵们被唤醒。殷郊在这里拿的到底是什么呢？只知道那个宝箱上画的符文和祭司那的符文很像。",
        "姬昌回想着水潭的音乐，突然想起了月亮女祭司的诅咒，那是关于一个女人的诅咒。联想到一系列的事情，可以看出那个宝箱中一定是关于那个女人。不过又是谁，能让殷洪和邓婵玉来到这里",
        "又失去了方向，这一次孙良也与队伍断了联系，但距离真相越来越近，在离开密道的那一刹那，罗宣的一句“我要将那狐狸碎尸万段”让姬昌陷入沉思，那个狐狸是谁？会与有那个被诅咒的女人有联系吗？",
        "最后一颗棋子落下，终于赢了棋盘。在闻仲的帮助下我们救出了雷震子。雷震子竟从宝塔中带出了关于那个宝箱的信息，里面装着妲己的一根断尾，妲己想要拿回自己的断尾，所以邓婵玉和胡升才会来到这里。",
        "经过再三的劝说和威逼利诱，胡升最终说出了殷郊的去向，他就在浮屠宫宫顶的望天台。截住宝箱，阻止妲己拿到断尾，即便殷郊是殷商太子，也不必害怕，姬昌沉思道，自己身为西歧之主，二人地位不相上下，只要方法得当，一切皆有可能。这里有一个姜子牙的锦囊，关键时刻可以用来脱身。",
        "差点误伤了前来救我们的土行孙，借助他的力量逃离雷鬼岭后，回到西歧都城。姬昌将事情的经过讲给西歧最大的巫师听，而查看了箱子的巫师却说了另一番话，这番话让姬昌越来越迷惑。"
    }
end

function StoryLayer:initTouch()
    self._parent.state = 40
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("故事背景")
end

function StoryLayer:initTouchTable()
    self._parent.touchTable = {}
    local storyData = mGameData.data["block"]
    for i = 1, 15 do
        local t = {
            say = "第" .. i .. "章",
            func = function()
                mGameWorld:getSoundMgr():stopEff()
                mGameWorld:getSoundMgr():playEff("zhang_" .. i)
                self.text:setString(self.des[i])
            end
        }
        table.insert(self._parent.touchTable, t)
        local index = 0
        for k, p in ipairs(storyData) do
            local zhang = math.ceil(p.id / 6)
            local jie = p.id % 6
            if jie == 0 then
                jie = 6
            end
            if index == 6 then
                break
            end
            if zhang == i then
                local n = {
                    say = p.name,
                    func = function()
                        mGameWorld:getSoundMgr():stopEff()
                        mGameWorld:getSoundMgr():playEff(zhang .. "_jie_" .. jie)
                        self.text:setString(p.name.."\n"..p.des)
                    end
                }
                table.insert(self._parent.touchTable, n)
                index = index + 1
            end
        end
    end
end

function StoryLayer:show()
end

function StoryLayer:refresh(data)
    self:show()
    self:initTouchTable()
end

return StoryLayer
