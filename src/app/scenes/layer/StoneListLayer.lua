--[[
    背包
]]
local StoneListLayer =
    class(
    "StoneListLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")

function StoneListLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self:initCsb()
    self.data = data
    self:show()
    self:initTouch()
end

function StoneListLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("bag/BagLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.bagView = csbNode:getChildByName("bagView")
end

function StoneListLayer:show()
    self.bagView:removeAllPages()
    local number = #self.data
    for i = 1, math.ceil(number / 20) do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1200, 550))
        for j = 1, 20 do
            local itemData = self.data[(i - 1) * 20 + j]
            if itemData then
                local item = self:createItem(itemData[2][3], itemData[2][1], itemData[2][2])
                local h = math.ceil(j / 5)
                local w = j - (h - 1) * 5
                local x = 806 * ((w + w - 1) / 10)
                local y = (8 - 2 * h + 1) / 8 * 666
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end

        self.bagView:addPage(layout)
    end
end

function StoneListLayer:createItem(type, id, number)
    --1 物品 -- 2装备 -- 碎片
    local item = cc.CSLoader:createNode("common/Equip.csb")
    local img = item:getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    local num = item:getChildByName("bg"):getChildByName("num")
    local path
    local scale = 1
    if type == 1 then
        path = "item/"
    elseif type == 2 then
        path = "equip/"
        id = math.ceil(id / 4)
    elseif type == 3 then
        path = "hero/hero_"
        scale = 0.5
    elseif type == 4 then
        path = "stone/"
        id = math.ceil(id / 5)
    end
    img:setTexture(path .. id .. ".png"):setScale(scale)
    num:setString(number)
    return item
end

function StoneListLayer:initTouch()
    self._parent.state = 1014
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("当前为宝石列表,5个相同级别的宝石可以合成一个更高一级的宝石。")
end

function StoneListLayer:initTouchTable()
    self._parent.touchTable = {}
    if MAKESTONE then
        self._parent.touchTable[1] = {
            say = "一键合成，当前背包内符合条件的所有宝石合成更高一级的宝石",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.COMPOSEALLSTONE})
            end
        }
    end
    for i, v in ipairs(self.data) do
        local index = math.ceil(i / 20)
        local str = self:getSay(v[2])
        local t = {
            say = str,
            func = function()
                if MAKESTONE then
                    mGameWorld:getNetMgr():send({REQUST.COMPOSESTONE, v[2][1]})
                else
                    mGameWorld:getNetMgr():send({REQUST.MOSAICSTONETOEQUIP, EQUIPID, v[2][1], STONEPOS})
                end
            end,
            touch = function()
                print("index:" .. index)
                self.bagView:scrollToPage(index - 1)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function StoneListLayer:getSay(data)
    local str
    local name
    name = mGameData.data["stone"][data[1]]["name"]

    local pname = mGameData.data["stone"][data[1]].pname
    local pvalue = mGameData.data["stone"][data[1]].pvalue
    if pname == "atk" then
        str = name .. "伤害增加" .. pvalue
    elseif pname == "hp" then
        str = name .. "血量增加" .. pvalue
    elseif pname == "speed" then
        str = name .. "速度增加" .. pvalue
    elseif pname == "def" then
        str = name .. "防御增加" .. pvalue
    end
    str = str .. ",数量" .. data[2]
    return str
end

function StoneListLayer:refresh(data)
    self.data = data
    self:show()
    self:initTouchTable()
end

return StoneListLayer
