--[[
    公会列表
]]
local GuildListLayer =
    class(
    "GuildListLayer",
    function()
        return display.newLayer()
    end
)

function GuildListLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self._H = 1
    self._V = 5
    self:initCsb()
    self:initTouch()
    self:show()
end

function GuildListLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("guild/GuildListLayer.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.pageList = csbNode:getChildByName("pageView")
end

function GuildListLayer:initTouch()
    tts.say("公会列表，双击申请加入公会")
    self._parent.touchTable = {}
    self._parent.state = 903
    self._parent.index = appdf.getIndex(0)
    for i, v in ipairs(self.data) do
        local t = {
            say = v[2] .. "," .. v[4] .. "人，" .. v[5],
            func = function()
                mGameWorld:getNetMgr():send({REQUST.APPLYFAMILY, v[1]})
            end,
            touch = function (  )
                self.pageList:scrollToPage(math.ceil(i / 6) - 1)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function GuildListLayer:show()
    self.pageList:removeAllPages()
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(805, 666))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = 805 * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * 666
                print("h:" .. h .. "w:" .. w .. "x:" .. x .. "y:" .. y)
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function GuildListLayer:createItem(data)
    local item = cc.CSLoader:createNode("guild/GuildItem.csb")
    local name = item:getChildByName("name")
    local num = item:getChildByName("num")
    local notice = item:getChildByName("notice")
    local state = item:getChildByName("btn"):getChildByName("tag")
    name:setString(data[2])
    num:setString("(" .. data[4] .. "人)")
    notice:setString(data[5])
    return item
end

return GuildListLayer
