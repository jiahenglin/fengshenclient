--[[
    战役战斗
]]
local CheckPointLayer =
    class(
    "CheckPointLayer",
    function()
        return display.newLayer()
    end
)
local LevelInfoLayer = require("app.scenes.layer.LevelInfoLayer")

local mGameData = require("app.template.gamedata")

local NUM = {"一", "二", "三", "四", "五", "六", "七", "八", "九", "十", "十一", "十二", "十三", "十四", "十五"}
local DATA_T = {}
function CheckPointLayer:ctor(parent)
    self._parent = parent
    self:addTo(parent, 20)
    DATA_T = {}
    self:initCsb()
    self:createPage()
    if GlobalUserItem.guid_pont ~= 100 then
        self:initGuidTouch()
    else
        self:initTouch()
    end
end

function CheckPointLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("fight/ChoseLevel.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.page = csbNode:getChildByName("pageView")
    self.title = csbNode:getChildByName("title")
    self.currentPage = math.floor(GlobalUserItem.crusade_point / 6)
end

function CheckPointLayer:createPage()
    local n = math.ceil(#mGameData.data["block_loot"] / 6)
    local big = {}
    for i = 1, n do
        big[i] = {}
        for j = 1, 6 do
            local value = (i - 1) * 6 + j
            if value <= #mGameData.data["block_loot"] then
                table.insert(big[i], value)
            end
        end
    end
    for i, v in ipairs(big) do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(1200, 550))
        for k, t in ipairs(v) do
            local current = (i - 1) * 6 + k
            local h = math.ceil(k / 3)
            local w = k - (h - 1) * 3
            local x = 1200 * (w / 4)
            local y = (3 - h) / 3 * 600
            local csb = cc.CSLoader:createNode("fight/Level.csb")
            local ckb = csb:getChildByName("ckb")
            local text = ckb:getChildByName("number")
            local bpass = ckb:getChildByName("Bpass")
            text:setString("第" .. NUM[k] .. "关")
            bpass:setString("")
            if current <= GlobalUserItem.crusade_point then
                bpass:setString("已通过")
            end
            layout:addChild(csb)
            csb:setPosition(cc.p(x, y))
            table.insert(DATA_T, (i - 1) * 6 + k, csb)
        end
        self.page:addPage(layout)
    end
    if GlobalUserItem.crusade_point == #DATA_T then
        self.currentPage = self.currentPage - 1
    end
    self.page:scrollToPage(self.currentPage)
    self.page:retain()
    -- DATA_T[GlobalUserItem.crusade_point + 1]:getChildByName("ckb"):setSelected(true)
    -- self._parent.index = GlobalUserItem.crusade_point + 1
end

function CheckPointLayer:initTouch()
    tts.say("当前为战役战斗选择关卡，左右滑动切换关卡，双击确认")
    self._parent.touchTable = {}
    -- self._parent.index = GlobalUserItem.crusade_point + 1
    local pass = GlobalUserItem.crusade_point + 1
    if GlobalUserItem.crusade_point == #DATA_T then
        pass = GlobalUserItem.crusade_point
    end
    print(pass .. GlobalUserItem.crusade_point)
    self._parent.index = appdf.getIndex(pass)
    print("self.index" .. self._parent.index)
    DATA_T[self._parent.index]:getChildByName("ckb"):setSelected(true)
    self.title:setString("第" .. NUM[math.ceil(self._parent.index / 6)] .. "章")
    if self._parent.index % 6 == 1 then
        mGameWorld:getSoundMgr():playEff("zhang_" .. math.ceil(self._parent.index / 6))
    end
    self._parent.state = 101
    for i, v in ipairs(DATA_T) do
        local index = math.ceil(i / 6)
        self._parent.touchTable[i] = {}
        local a = ""
        if i <= GlobalUserItem.crusade_point then
            a = "已通过"
        end
        v:getChildByName("ckb"):getChildByName("Bpass"):setString(a)
        self._parent.touchTable[i].say = "第" .. NUM[index] .. "章" .. v:getChildByName("ckb"):getChildByName("number"):getString() .. a
        self._parent.touchTable[i].func = function()
            print(i .. "~~~~~~~~~~~GlobalUserItem.crusade_point~~~~~~~~~~" .. GlobalUserItem.crusade_point)
            if i <= GlobalUserItem.crusade_point + 1 then
                self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                self._parent.levelInfoLayer = LevelInfoLayer.new(self._parent, i)
                self._parent.currentLayer[#self._parent.currentLayer] = self._parent.levelInfoLayer
                self._parent.fight = i
            else
                tts.say("无法挑战当前关卡，请确认上一关卡是否通过")
            end
        end
        self._parent.touchTable[i].touch = function()
            print("index:" .. index)
            self.page:scrollToPage(index - 1)
            self.title:setString("第" .. NUM[index] .. "章")
            for k, t in ipairs(DATA_T) do
                t:getChildByName("ckb"):setSelected(false)
            end
            v:getChildByName("ckb"):setSelected(true)
            mGameWorld:getSoundMgr():stopEff()
            if i % 6 == 1 and GlobalUserItem.crusade_point + 1 == i then
                mGameWorld:getSoundMgr():playEff("zhang_" .. math.ceil(index))
            end
        end
    end
end

function CheckPointLayer:initGuidTouch()
    if GlobalUserItem.guid_pont == 7 then
        tts.say("双击选择第一章第一关")
        self._parent.touchTable = {
            {
                say = "第一章，第一关",
                func = function()
                    self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                    self._parent.levelInfoLayer = LevelInfoLayer.new(self._parent, 1)
                    self._parent.currentLayer[#self._parent.currentLayer] = self._parent.levelInfoLayer
                    self._parent.fight = 1
                end
            }
        }
    end
end

return CheckPointLayer
