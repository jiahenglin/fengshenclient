--[[
    FrontLayer
]]
local FrontLayer =
    class(
    "FrontLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
local FrontInfoLayer = require("app.scenes.layer.FrontInfoLayer")
function FrontLayer:ctor(parent, data1, data2)
    self._parent = parent
    self:addTo(self._parent, 20)
    self._H = 3
    self._V = 3
    self:initCsb()
    self:getData(data1, data2)
    self:initTouch()
    self:show()
end

-- 2修习中， 1装备中， 0已获得 已修习
function FrontLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("front/FrontLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.pageList = self._bg:getChildByName("pageList")
end

function FrontLayer:initTouch()
    self._parent.state = 21
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
    tts.say("当前在阵法菜单")
end
-- {id,lv,exp,state,xxtime}  -- {itemid,num,type}
function FrontLayer:getData(data1, data2)
    local a = mGameData.data["formation_simple"]
    self.data = appdf.clone(a, true)
    for i, v in ipairs(self.data) do
        v["lv"] = 0
        v["exp"] = 0
        v["number"] = 0
        v["state"] = -1
        v["study"] = 0
        v["starttime"] = 0
        v["ostime"] = 0
    end
    -- dump(data2)
    for i, v in ipairs(self.data) do
        for k, t in ipairs(data1) do
            if v["fid"] == t[1] then
                v["lv"] = t[2]
                v["exp"] = t[3]
                v["state"] = t[4]
                v["study"] = t[5] or 0
                v["starttime"] = t[6]
                v["ostime"] = t[7]
            end
        end
        for m, n in ipairs(data2) do
            if v["itemid"] == n[1] then
                v["number"] = n[2]
            end
        end
    end
    local function sortA(a, b)
        local r
        local a1 = tonumber(a.lv)
        local b1 = tonumber(b.lv)
        local a2 = tonumber(a.state)
        local b2 = tonumber(b.state)
        local a3 = tonumber(a.number)
        local b3 = tonumber(b.number)
        if a2 == 2 then
            a2 = 0
        end
        if b2 == 2 then
            b2 = 0
        end
        if a2 == b2 then
            if a1 == b1 then
                r = a3 > b3
            else
                r = a1 > b1
            end
        else
            r = a2 > b2
        end
        return r
    end
    table.sort(self.data, sortA)
    -- dump(self.data)
end

function FrontLayer:initTouchTable()
    self._parent.touchTable = {}
    local runScene = cc.Director:getInstance():getRunningScene()
    for i, v in ipairs(self.data) do
        local index = math.ceil(i / (self._V * self._H))
        local state = ""
        local lv = ",等级:" .. v["lv"]
        if v["state"] == 2 then
            state = lv .. ",修习中"
        elseif v["state"] == 1 then
            state = lv .. "装备中"
        elseif v["state"] == 0 then
            if v["lv"] == 0 then
                state = "已获得，未修习"
            else
                state = lv .. "已修习"
            end
        elseif v["state"] == -1 then
            state = "未获得,拥有残卷数量：" .. v["number"]
        end
        local t = {
            say = v["name"] .. state,
            func = function()
                FORMATIONCHOSE = v["name"]
                runScene.currentLayer[#runScene.currentLayer + 1] = true
                runScene.frontInfoLayer = FrontInfoLayer.new(runScene, v)
                runScene.currentLayer[#runScene.currentLayer] = runScene.frontInfoLayer
            end,
            touch = function()
                self.pageList:scrollToPage(index - 1)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
    if not tolua.isnull(runScene.frontInfoLayer) then
        local nd
        for i, v in ipairs(self.data) do
            if v["name"] == FORMATIONCHOSE then
                nd = v
                -- dump(nd)
                break
            end
        end
        runScene.frontInfoLayer:refresh(nd)
    end
end

function FrontLayer:show()
    self.pageList:removeAllPages()
    local pageW = self.pageList:getContentSize().width
    local pageH = self.pageList:getContentSize().height
    local number = #self.data
    local avg = self._H * self._V
    local pageNum = math.ceil(number / avg)
    print(number .. avg .. pageNum)
    for i = 1, pageNum do
        local layout = ccui.Layout:create()
        layout:setContentSize(cc.size(750, 550))
        for j = 1, avg do
            local k = (i - 1) * avg + j
            local itemData = self.data[k]
            if itemData then
                local item = self:createItem(itemData)
                local h = math.ceil(j / self._H)
                local w = j - (h - 1) * self._H
                local x = pageW * ((w + w - 1) / (self._H * 2))
                local y = ((self._V * 2) - 2 * h + 1) / (self._V * 2) * pageH
                item:addTo(layout):align(display.CENTER, x, y)
            end
        end
        self.pageList:addPage(layout)
    end
end

function FrontLayer:createItem(data)
    local item = cc.CSLoader:createNode("front/FrontItem.csb")
    local bg = item:getChildByName("bg")
    local sprite = bg:getChildByName("sprite")
    local name = bg:getChildByName("name")
    local num = bg:getChildByName("num")
    local lv = bg:getChildByName("lv")
    local get = bg:getChildByName("get")
    local state = ""
    local lv = "等级:" .. data["lv"]
    if data["state"] == 2 then
        state = lv .. ",修习中"
    elseif data["state"] == 1 then
        state = lv .. "装备中"
    elseif data["state"] == 0 then
        if data["lv"] == 0 then
            state = "已获得，未修习"
        else
            state = lv .. "已修习"
        end
    elseif data["state"] == -1 then
        state = "未获得,拥有残卷数量：" .. data["number"]
    end
    sprite:setTexture("formation/" .. data["fid"] .. ".png")
    name:setString(data["name"])
    num:setString(data["number"])
    get:setString(state)
    return item
end

function FrontLayer:refresh(data1, data2)
    self:getData(data1, data2)
    self:show()
    self:initTouchTable()
end

return FrontLayer
