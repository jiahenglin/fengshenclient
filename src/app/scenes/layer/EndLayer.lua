local EndLayer =
    class(
    "EndLayer",
    function()
        return display.newLayer()
    end
)

local mGameData = require("app.template.gamedata")
local MultiPlatform = require("app.external.MultiPlatform")

function EndLayer:ctor(parent, data, winteam)
    print("winteam" .. winteam)
    -- dump(data)
    self._parent = parent
    self._parent.state = 2 -- 结算时
    self:addTo(parent, 20):setPosition(0, 0)
    self.gold = data[1] or 0 -- 金币
    self.exp = data[2] or 0 --经验
    self.equip = data[3] or {} -- 装备
    self.card = data[4] or {} -- 碎片
    self.skill = data[5] or 0 -- 技能点
    self.hero = data[6] or 0 -- 英雄
    self.blue = data[7] or {} -- 道具
    local t = {
        "胜 利",
        "失 败"
    }
    self.ttilestr = t[winteam]
    local csbNode = cc.CSLoader:createNode("fight/EndLayer.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)

    self._csbNode = csbNode
    self.t_gold = csbNode:getChildByName("text_gold")
    self.t_exp = csbNode:getChildByName("text_exp")
    self.t_skill = csbNode:getChildByName("text_skill")
    self.t_blue = csbNode:getChildByName("text_blue")
    self.title = csbNode:getChildByName("title")
    self.Bwin = winteam
    self:show()
    self:initTouch()
end

function EndLayer:show()
    -- self.t_blue:setString(self.blue[2] or 0)
    self.t_gold:setString(self.gold)
    self.t_exp:setString(self.exp)
    self.t_skill:setString(self.skill)
    if self.Bwin == 1 then
        mGameWorld:getSoundMgr():playEff("win")
        self._parent.battleInfo = self._parent.battleInfo .. "胜利"
    else
        mGameWorld:getSoundMgr():playEff("lose")
        self._parent.battleInfo = self._parent.battleInfo .. "失败"
    end
    self.title:setString(self.ttilestr)
    if GlobalUserItem.guid_pont == 100 then
        tts.say(self.ttilestr)
    elseif GlobalUserItem.guid_pont == 7 then
        GlobalUserItem.guid_pont = 8
    else
        tts.say("胜利 获得金币" .. self.gold .. "获得技能点" .. self.skill .. "获得物资" .. mGameData.data["hero"][6]["name"] .. "英雄卡牌,快去查看拥有的英雄卡牌并且提升技能和装备吧，下滑进入主菜单，去选择英雄。")
    end
end

function EndLayer:initTouch()
    self._parent.state = 2
    self._parent.index = appdf.getIndex(0)
    self._parent.touchTable = {}
    local runScene = cc.Director:getInstance():getRunningScene()
    local str = "获得金币" .. self.gold .. "经验" .. self.exp
    if #self.equip > 0 then
        for i, v in ipairs(self.equip) do
            if #v > 0 then
                str = str .. "装备" .. mGameData.data["equip"][v[1]]["name"] .. "数量" .. v[2]
            end
        end
    end
    if #self.card > 0 then
        for i, v in ipairs(self.card) do
            if #v > 0 then
                str = str .. "英雄碎片" .. mGameData.data["hero"][v[1]]["name"] .. "碎片数量" .. v[2]
            end
        end
    end

    if self.skill then
        str = str .. "技能点" .. self.skill
    end
    if self.hero > 0 then
        str = str .. mGameData.data["hero"][self.hero]["name"] .. "英雄卡牌"
    end
    if #self.blue > 0 then
        for i, v in ipairs(self.blue) do
            if #v > 0 then
                str = str .. "道具" .. mGameData.data["item"][v[1]]["name"] .. "数量" .. v[2]
                if v[1] == 1 then
                    self.t_blue:setString(self.blue[2])
                end
            end
        end
    end
    if GlobalUserItem.guid_pont == 100 then
        self._parent.touchTable[1] = {
            say = str,
            func = function()
                print("战利品")
            end
        }
        local pve = {
            say = "再来一次",
            func = function()
                self._parent:upCallback()
                runScene.touchTable[runScene.fight].func()
            end
        }
        local next = {
            say = "下一关",
            func = function()
                self._parent:upCallback()
                runScene.touchTable[runScene.fight + 1].func()
            end
        }
        local pvp = {
            say = "继续匹配",
            func = function()
                self._parent:upCallback()
                mGameWorld:getNetMgr():send({REQUST.STARTRANDOMMATCH})
            end
        }
        local pvw = {
            say = "再来一次",
            func = function()
                self._parent:upCallback()
                mGameWorld:getNetMgr():send({REQUST.FIGHTSHILIAN, GlobalUserItem.shilianindex})
            end
        }
        local back = {
            say = "返回",
            func = function()
                self._parent:upCallback()
            end
        }
        local copy = {
            say = "复制详细战况",
            func = function()
                MultiPlatform:copyToClipboard(self._parent.battleInfo)
                print(self._parent.battleInfo .. TEXT_END)
                tts.say("复制成功")
            end
        }
        if self.Bwin == 1 then
            tts.say("胜利")
            if FIGHTTYPE == 1 then -- pve
                if self._parent.fight == GlobalUserItem.crusade_point + 1 then
                    GlobalUserItem.crusade_point = GlobalUserItem.crusade_point + 1
                end
                table.insert(self._parent.touchTable, next)
                table.insert(self._parent.touchTable, pve)
            elseif FIGHTTYPE == 3 then -- pvp
                table.insert(self._parent.touchTable, pvp)
            elseif FIGHTTYPE == 5 then -- pvw
                table.insert(self._parent.touchTable, pvw)
            end
        else
            tts.say("失败")
            if FIGHTTYPE == 1 then
                table.insert(self._parent.touchTable, pve)
            elseif FIGHTTYPE == 3 then
                table.insert(self._parent.touchTable, pvp)
            elseif FIGHTTYPE == 5 then
                table.insert(self._parent.touchTable, pvw)
            end
        end
        table.insert(self._parent.touchTable, copy)
        table.insert(self._parent.touchTable, back)
    end
end

return EndLayer
