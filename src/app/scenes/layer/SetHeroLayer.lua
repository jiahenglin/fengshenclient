--[[
    英雄上阵
]]
local SetHeroLayer =
    class(
    "SetHeroLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
local Ready = require("app.scenes.layer.Ready")

function SetHeroLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data
    self.uphero = {}
    self.buff = {
        "前位",
        "坤位",
        "巽位",
        "坎位",
        "离位"
    }
    self:dealData()
    self:initCsb()
    self.bp = false
    self:initTouch()
    self:showHero()
    self:showUp()
end
function SetHeroLayer:dealData()
    local a = mGameData.data["formation"]
    local data = {}

    if FORMATIONID ~= 0 then
        for i, v in ipairs(a) do
            if v["fid"] == FORMATIONID and v["level"] == FORMATIONLV then
                data = v
            end
        end

        for i = 1, 5 do
            local n = data["prop" .. i]
            local m = data["value" .. i]
            self.buff[i] = self.buff[i] .. "增益效果为" .. appdf.buffToString(n, m)
        end
    end
end

function SetHeroLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("fight/SetHero.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self.heroBox = csbNode:getChildByName("heroBox") -- 拥有英雄操作区
    self.heroUp = csbNode:getChildByName("heroUp") -- 上阵英雄显示区
end

function SetHeroLayer:initTouch()
    local herodata = mGameData.data["hero"]
    local troopdata = mGameData.data["formation"]
    self._parent.state = 1002
    self._parent.index = appdf.getIndex(0)
    self._parent.touchTable = {}

    local guidsay = ""
    for k = 1, 5 do
        local herochose = "未选择上阵英雄"
        self.uphero[k] = 0
        for i, v in ipairs(self.data) do
            if v[4] == k then
                herochose = herodata[v[2]].name .. "上阵中"
                self.uphero[k] = v[2]
                break
            end
        end
        self._parent.touchTable[k] = {
            say = self.buff[k] .. herochose,
            func = function()
                self:toChoseHero(k)
            end
        }
        guidsay = guidsay .. self._parent.touchTable[k].say
    end

    if FORMATIONSETTING then
        tts.say("当前可配置上阵英雄")
    else
        tts.say("选择位置,长按三秒进入战斗")
        self._parent.touchTable[6] = {
            say = "开始战斗",
            func = function()
                if self._parent.bpvp then
                    print("~~~~~~~~~~~~~READY~~~~~~~~~~~~~~~")
                    mGameWorld:getNetMgr():send({REQUST.READY}) -- 发送准备
                    self._parent.currentLayer[#self._parent.currentLayer + 1] = true
                    self._parent.Ready = Ready.new(self._parent)
                    self._parent.currentLayer[#self._parent.currentLayer] = self._parent.Ready
                else
                    mGameWorld:getNetMgr():send({REQUST.ATKCITYCELL, self._parent.fight}) -- 发送开始
                end
            end
        }
    end
    if GlobalUserItem.guid_pont == 7 then
        tts.say("长按三秒开始战斗" .. guidsay)
        self._parent.touchTable = {
            {
                say = "开始战斗",
                func = function()
                    mGameWorld:getNetMgr():send({REQUST.ATKCITYCELL, self._parent.fight})
                end
            }
        }
        self._parent.index = 0
    end
end

function SetHeroLayer:toChoseHero(pos)
    self._parent.state = 1003
    self._parent.index = 0
    self._parent.touchTable = {}
    self._parent.touchTable[1] = {
        say = "下阵该位置英雄",
        func = function()
            mGameWorld:getNetMgr():send({REQUST.SETFORMATIONPOS, 0, pos})
        end,
        hero = function()
            print("下阵")
        end
    }
    for i, v in ipairs(self.data) do
        local name = mGameData.data["hero"][v[2]].name
        local a = {
            say = name,
            func = function()
                print("~~~~~~~~~~~~~~~~~~pos~~~~~~~~~~~~~~~~" .. pos)
                mGameWorld:getNetMgr():send({REQUST.SETFORMATIONPOS, v[1], pos})
            end,
            hero = function()
                mGameWorld:getSoundMgr():stopEff()
                mGameWorld:getSoundMgr():playEff("hero_say_" .. v[2])
            end,
            touch = function()
                self.heroBox:jumpToPercentVertical((math.ceil(i / 5) - 1) / (#self.data / 5) * 100)
            end
        }
        table.insert(self._parent.touchTable, a)
    end

    tts.say("选择该位置上阵英雄")
end

function SetHeroLayer:refresh(heroid, pos, downhero)
    for i, v in ipairs(self.data) do
        if v[1] == heroid then
            v[3] = 1
            v[4] = pos
        end
        if v[1] == downhero and downhero ~= heroid then
            v[3] = 0
            v[4] = 0
        end
    end
    self:initTouch()
    self:showUp()
end

function SetHeroLayer:showUp()
    for i, v in ipairs(self.uphero) do
        local head = appdf.createHeroHead(v)
        self.heroUp:addChild(head)
        head:setPosition(((i - 1) * 2 + 1) / 10 * 1024, 234 / 2)
    end
end

function SetHeroLayer:showHero()
    local height = 428
    if #self.data > 10 then
        height = math.ceil(#self.data / 5) * 214
        self.heroBox:setInnerContainerSize(cc.size(1024, height))
    end
    for i, v in ipairs(self.data) do
        local h = height - 214 / 2 - (math.ceil(i / 5) - 1) * 214
        local w = (i - (math.ceil(i / 5) - 1) * 5) / 6 * 1024
        local head = appdf.createHeroHead(v[2])
        self.heroBox:addChild(head)
        head:setPosition(w, h)
        -- print("x:" .. w .. "y:" .. h)
    end
end

return SetHeroLayer
