--[[
    FrontInfoLayer
]]
local FrontInfoLayer =
    class(
    "FrontInfoLayer",
    function()
        return display.newLayer()
    end
)
local mGameData = require("app.template.gamedata")
function FrontInfoLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(self._parent, 20)
    self.data = data
    self.needExp = 999999
    self.needStudy = 999999
    self:getInfo()
    print("exp" .. self.needExp .. "\nstudy" .. self.needStudy)
    self:initCsb()
    self:initTouch()
    self:show()
    -- dump(self.data)
end

function FrontInfoLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("front/FrontInfoLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    self.sprite = self._bg:getChildByName("book")
    self.name = self._bg:getChildByName("name")
    self.lv = self._bg:getChildByName("lv")
    self.exp = self._bg:getChildByName("exp")
    self.state = self._bg:getChildByName("state")
end

function FrontInfoLayer:initTouch()
    self._parent.state = 22
    self._parent.index = appdf.getIndex(0)
    self:initTouchTable()
end

function FrontInfoLayer:initTouchTable()
    self._parent.touchTable = {}
    local data = self.data
    local state = ""
    if data["state"] == 2 then
        state = " 修习中"
    elseif data["state"] == 1 then
        state = "装备中"
    elseif data["state"] == 0 then
        if data["lv"] == 0 then
            state = "已获得，未修习"
        else
            state = "已修习"
        end
    elseif data["state"] == -1 then
        state = "未获得,拥有残卷数量：" .. data["number"]
    end
    self.sprite:setTexture("formation/" .. data["fid"] .. ".png")
    self.name:setString(data["name"])
    self.lv:setString(data["lv"])
    self.state:setString(state)
    self.exp:setString(data["exp"])
    local timed = data["study"]
    if data["starttime"] == 0 then
        timed = timed
    else
        timed = timed + data["ostime"] - data["starttime"]
    end
    timed = math.floor(timed / 60)
    if timed >= self.needStudy then
        timed = self.needStudy
    end
    local itemdata = mGameData.data["item"]
    local needstr = ""
    if self.needItem ~= 0 then
        needstr = itemdata[self.needItem].name .. "材料" .. self.needItemNum .. "个"
    end

    self._parent.touchTable = {
        {
            say = data["name"] .. state,
            func = function()
                print("name")
            end
        },
        {
            say = " 当前等级:" .. data["lv"] .. "，升级所需熟练度：" .. self.needExp .. "," .. needstr .. ",当前熟练度:" .. data["exp"],
            func = function()
                print("exp")
            end
        },
        {
            say = "已修习时长" .. timed .. "分, 升级所需时长：" .. self.needStudy .. "分",
            func = function()
                print("study")
            end
        }
    }
    local s = {
        say = "修习阵法",
        func = function()
            mGameWorld:getNetMgr():send({REQUST.STARTSTUDYFORMATION, data["fid"]})
        end
    }
    local set = {
        say = "装备阵法",
        func = function()
            mGameWorld:getNetMgr():send({REQUST.SELECTFORMATION, data["fid"]})
            FORMATIONID = data["fid"]
            FORMATIONSETTING = true
        end
    }
    local up = {
        say = "极速修习, 消耗" .. self.needGem .. "金条直接完成修习",
        func = function()
            mGameWorld:getNetMgr():send({REQUST.LEVELUPFORMATION, data["fid"]})
        end
    }
    if data["state"] == 2 then
        -- table.insert(
        --     self._parent.touchTable,
        --     {
        --         say = "停止修习",
        --         func = function()
        --             if data["state"] == 2 then
        --                 mGameWorld:getNetMgr():send({REQUST.STOPSTUDYFORMATION, data["fid"]})
        --             else
        --                 tts.say("无法停止，未修习中")
        --             end
        --         end
        --     }
        -- )
        -- if data["exp"] >= self.needExp then
        --     table.insert(self._parent.touchTable, up)
        -- end
    elseif data["state"] == 1 then
        if data["fid"] ~= 1 then
            if data["exp"] >= self.needExp then
                table.insert(self._parent.touchTable, up)
                table.insert(self._parent.touchTable, s)
            end
        end
        table.insert(
            self._parent.touchTable,
            {
                say = "部署阵位英雄",
                func = function()
                    FORMATIONID = data["fid"]
                    FORMATIONLV = data["lv"]
                    FORMATIONSETTING = true
                    mGameWorld:getNetMgr():send({REQUST.GETFORMATION})
                end
            }
        )
    elseif data["state"] == 0 then
        -- if data["lv"] == 0 then
        -- else
        -- end
        table.insert(self._parent.touchTable, set)
        if data["fid"] ~= 1 then
            if data["exp"] >= self.needExp then
                table.insert(self._parent.touchTable, up)
                table.insert(self._parent.touchTable, s)
            end
        end
    elseif data["state"] == -1 then
        table.insert(
            self._parent.touchTable,
            {
                say = "合成阵法",
                func = function()
                    if data["number"] < data["itemnum"] then
                        tts.say("残卷不足")
                    else
                        mGameWorld:getNetMgr():send({REQUST.COMPOSEFORMATION, data["fid"]})
                    end
                end
            }
        )
    end
    for i, v in ipairs(self.buff) do
        self._bg:getChildByName("buff" .. i):setString(self.buff[i])
        local t = {
            say = self.buff[i],
            func = function()
                print("阵位信息" .. i)
            end
        }
        table.insert(self._parent.touchTable, t)
    end
end

function FrontInfoLayer:getInfo()
    -- dump(self.data)
    local data = {}
    for i, v in ipairs(mGameData.data["formation"]) do
        if v["fid"] == self.data["fid"] then
            table.insert(data, v)
        end
    end
    -- dump(data)
    self.buff = {
        "前位",
        "坤位",
        "巽位",
        "坎位",
        "离位"
    }
    local add = ""
    if data["fid"] == 6 then
        add = "孙良上阵则属性增益大大提升"
    elseif data["fid"] == 7 then
        add = "姜子牙上阵则属性增益大大提升"
    end
    if self.data["lv"] ~= 0 then
        for i, v in ipairs(data) do
            if v["level"] == self.data["lv"] + 1 then
                self.needExp = v["exp"]
                self.needStudy = v["study"]
                self.needGem = v["cost"]
                self.needItem = v["itemid"]
                self.needItemNum = v["itemnum"]
            end
            if v["level"] == self.data["lv"] then
                for i = 1, 5 do
                    local n = v["prop" .. i]
                    local m = v["value" .. i]
                    self.buff[i] = self.buff[i] .. "增益效果为" .. appdf.buffToString(n, m) .. add
                end
            end
        end
    end
    if self.data["lv"] == 0 then
        for i, v in ipairs(data) do
            if v["level"] == 1 then
                for i = 1, 5 do
                    local n = v["prop" .. i]
                    local m = v["value" .. i]
                    self.buff[i] = self.buff[i] .. "增益效果为" .. appdf.buffToString(n, m)
                end
                self.needStudy = v["study"]
                self.needExp = v["exp"]
                self.needGem = v["cost"]
                self.needItem = v["itemid"]
                self.needItemNum = v["itemnum"]
            end
        end
    end
    -- print("needexp" .. self.needExp)
    -- print("needStudy" .. self.needStudy)
end

function FrontInfoLayer:show()
end

function FrontInfoLayer:refresh(data)
    self.data = data
    self:getInfo()
    self:show()
    self:initTouchTable()
end

return FrontInfoLayer
