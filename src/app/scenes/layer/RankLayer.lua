--[[
    封神榜
]]
local RankLayer =
    class(
    "RankLayer",
    function()
        return display.newLayer()
    end
)

local ArenacntLayer = require("app.scenes.layer.ArenacntLayer")
local RankListLayer = require("app.scenes.layer.RankListLayer")

function RankLayer:ctor(parent, data)
    self._parent = parent
    self:addTo(parent, 20)
    self.data = data[1]
    self:initCsb()
    self:initTouch()
end

function RankLayer:initCsb()
    local csbNode = cc.CSLoader:createNode("Rank/RankLayer.csb")
    csbNode:addTo(self):align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.rank = csbNode:getChildByName("box1"):getChildByName("text_ranking")

    self.arenacnt = csbNode:getChildByName("box2"):getChildByName("text_left")
    self.arenacnt:setString(GlobalUserItem.arenacnt)
    self.rank:setString("未上榜")
    if self.data[1] then
        self.rank:setString((self.data[1] + 1) .. ",战绩:" .. self.data[3])
    end
end

function RankLayer:initTouch()
    local runScene = cc.Director:getInstance():getRunningScene()
    runScene.state = 401
    runScene.index = appdf.getIndex(0)
    runScene.touchTable = {
        [1] = {
            say = "个人排名" .. self.rank:getString(),
            func = function()
                print("个人排名")
            end
        },
        [2] = {
            say = "当前挑战值" .. GlobalUserItem.arenacnt .. "双击兑换挑战值",
            func = function()
                print("兑换挑战值")
                runScene.currentLayer[#runScene.currentLayer + 1] = true
                runScene.arenacntLayer = ArenacntLayer.new(runScene)
                runScene.currentLayer[#runScene.currentLayer] = runScene.arenacntLayer
            end
        },
        [3] = {
            say = "好友训练场",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.GETONLINEFRIENDS})
            end
        },
        [4] = {
            say = "开始匹配,消耗1挑战值",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.STARTRANDOMMATCH})
            end
        },
        [5] = {
            say = "封神个人榜单",
            func = function()
                local url = "http://114.55.170.29/fengshen/index.php?m=getsinglerank&uid=" .. GlobalUserItem.id .. "&sid=1"
                appdf.onHttpJsionTable(
                    url,
                    "GET",
                    function(jsondata)
                        runScene.currentLayer[#runScene.currentLayer + 1] = true
                        runScene.rankListLayer = RankListLayer.new(runScene, jsondata, 1)
                        runScene.currentLayer[#runScene.currentLayer] = runScene.rankListLayer
                    end
                )
            end
        },
        [6] = {
            say = "封神公会榜单",
            func = function()
                local url = "http://114.55.170.29/fengshen/index.php?m=getfamilyrank&sid=1&fid=" .. GlobalUserItem.familyid
                appdf.onHttpJsionTable(
                    url,
                    "GET",
                    function(jsondata)
                        runScene.currentLayer[#runScene.currentLayer + 1] = true
                        runScene.rankListLayer = RankListLayer.new(runScene, jsondata, 2)
                        runScene.currentLayer[#runScene.currentLayer] = runScene.rankListLayer
                    end
                )
            end
        }
    }
    tts.say("当前是封神榜菜单内，左右滑动切换功能")
end

return RankLayer
