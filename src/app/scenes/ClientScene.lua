local ClientScene =
    class(
    "ClientScene",
    function()
        return display.newScene("ClientScene")
    end
)

local MultiPlatform = require("app.external.MultiPlatform")
local mGameData = require("app.template.gamedata")
local scheduler = require("framework.scheduler")
local RES_LAYER = "app.scenes.layer."
local FightLayer = require(RES_LAYER .. "FightLayer")
local HeroLayer = require(RES_LAYER .. "HeroLayer") --英雄
local SummonLayer = require(RES_LAYER .. "SummonLayer") --灵台
local CheckPointLayer = require(RES_LAYER .. "CheckPointLayer") -- 战役战斗
local BagLayer = require(RES_LAYER .. "BagLayer") -- 背包
local ShopLayer = require(RES_LAYER .. "ShopLayer") -- 集市
local RankLayer = require(RES_LAYER .. "RankLayer") -- 封神榜
local WaittingLayer = require(RES_LAYER .. "WaittingLayer") -- 匹配等待
local HomeLayer = require(RES_LAYER .. "HomeLayer") --家园
local EndLayer = require(RES_LAYER .. "EndLayer") -- 结算
local SetHeroLayer = require(RES_LAYER .. "SetHeroLayer") -- 设置上阵英雄
local Ready = require(RES_LAYER .. "Ready")
local ActiveLayer = require(RES_LAYER .. "ActiveLayer")
local SetEquipLayer = require(RES_LAYER .. "SetEquipLayer")
local HeroInfoLayer = require(RES_LAYER .. "HeroInfoLayer")
local SettingLayer = require(RES_LAYER .. "SettingLayer")
local TroopSetLayer = require(RES_LAYER .. "TroopSetLayer")
local GetHeroLayer = require("app.scenes.layer.GetHeroLayer")
local GuildMenu = require(RES_LAYER .. "GuildMenu")
local GuildListLayer = require(RES_LAYER .. "GuildListLayer")
local GuildLayer = require(RES_LAYER .. "GuildLayer")
local ApplyListLayer = require(RES_LAYER .. "ApplyListLayer")
local Pass = require("app.ui.Pass")
local FriendApplyLayer = require(RES_LAYER .. "FriendApplyLayer")
local FriendLayer = require(RES_LAYER .. "FriendLayer")
local PKMessage = require("app.ui.PKMessage")
local WaitForFriend = require("app.ui.WaitForFriend")
local TrialLayer = require(RES_LAYER .. "TrialLayer")
local FrontLayer = require(RES_LAYER .. "FrontLayer")
local DayGetLayer = require(RES_LAYER .. "DayGetLayer")
local GetHeroTenLayer = require(RES_LAYER .. "GetHeroTenLayer")
local GuildStoreLayer = require(RES_LAYER .. "GuildStoreLayer")
local GuildWareHouse = require(RES_LAYER .. "GuildWareHouse")
local TaskLayer = require(RES_LAYER .. "TaskLayer")
local TaskForDaily = require(RES_LAYER .. "TaskForDaily")
local TaskForCity = require(RES_LAYER .. "TaskForCity")
local StoryLayer = require(RES_LAYER .. "StoryLayer")
local HappyLayer = require(RES_LAYER .. "HappyLayer")
local EquipLayer = require(RES_LAYER .. "EquipLayer")
local MakeEquipLayer = require(RES_LAYER .. "MakeEquipLayer")
local StoneListLayer = require(RES_LAYER .. "StoneListLayer")
local RankListLayer = require(RES_LAYER .. "RankListLayer")
function ClientScene:ctor()
    self:init()
end
function ClientScene:init()
    self._csbNode = cc.CSLoader:createNode("hall/HallScene.csb")
    local csbNode = self._csbNode
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self:addEventListener()
    self.index = 1
    self.state = 3

    self.battleInfo = ""  -- 战报
    self.currentLayer = {}
    self.events = {}
    if device.platform ~= "windows" then
        local b = display.newSprite("common/shader.png"):addTo(self, 1000):align(display.CENTER, display.cx, display.cy):setOpacity(OPACITY)
        local a = b:getContentSize()
        b:setScale(display.width / a.width, display.height / a.height)
    end
    INDEXTABLE = {}
    table.insert(INDEXTABLE, 0)
end

--添加触摸监听
function ClientScene:addEventListener()
    local x, y, a, b
    local click = false
    local shcheduleID
    local shcheduleCopy
    local time = false
    local function onTouchBegan(touch, event)
        local pos = touch:getLocation()
        x = pos.x
        y = pos.y
        if self.state == 1002 or self.state == 1003 then
            shcheduleID =
                scheduler.performWithDelayGlobal(
                function()
                    if FORMATIONSETTING == false then
                        if self.bpvp then
                            print("~~~~~~~~~~~~~READY~~~~~~~~~~~~~~~")
                            mGameWorld:getNetMgr():send({REQUST.READY}) -- 发送准备
                            self.currentLayer[#self.currentLayer + 1] = true
                            self.Ready = Ready.new(self)
                            self.currentLayer[#self.currentLayer] = self.Ready
                        else
                            mGameWorld:getNetMgr():send({REQUST.ATKCITYCELL, self.fight}) -- 发送开始
                        end
                    end
                end,
                2
            )
        else
            if self.index ~= 0 then
                shcheduleCopy =
                    scheduler.performWithDelayGlobal(
                    function()
                        time = true
                        tts.say("上下滑动可复制内容")
                    end,
                    2
                )
            end
        end

        if click then
            click = false
            mGameWorld:getSoundMgr():playEff("click_double")
            print("双击" .. self.state .. self.index)
            -- 战斗中
            if self.state == 1 then
                if self.touchTable[self.index] then
                    self.fightlayer:useSkill(self.touchTable[self.index])
                    table.removebyvalue(self.touchTable, self.touchTable[self.index])
                end
            elseif self.state == 905 or self.state == 502 then
                if x < display.cx then
                    self.touchTable[self.index].leftTouch()
                else
                    self.touchTable[self.index].rightTouch()
                end
            else -- 菜单界面  英雄菜单界面  战役选择
                if self.touchTable[self.index] and self.touchTable[self.index].func then
                    self.touchTable[self.index].func()
                end
            end
        else
            self:runAction(
                cc.Sequence:create(
                    cc.DelayTime:create(0.2),
                    cc.CallFunc:create(
                        function()
                            if click then
                                click = false
                            end
                        end
                    )
                )
            )
            click = true
        end
        return true
    end

    local function onTouchEnded(touch, event)
        local pos = touch:getLocation()
        a = pos.x
        b = pos.y
        if a - x < -TOUCH_LIMIT and math.abs(a - x) > math.abs(b - y) then
            self:leftCallback()
            click = false
        elseif a - x > math.abs(b - y) and a - x > TOUCH_LIMIT then
            self:rightCallback()
            click = false
        elseif b - y > math.abs(a - x) and b - y > TOUCH_LIMIT then
            click = false
            if time then
                ClipboardText = self.touchTable[self.index].say or ""
                MultiPlatform:copyToClipboard(ClipboardText .. TEXT_END)
                tts.say("复制成功")
                time = false
            else
                self:upCallback()
            end
        elseif b - y < -TOUCH_LIMIT and math.abs(b - y) > math.abs(a - x) then
            click = false
            if time then
                ClipboardText = ClipboardText .. self.touchTable[self.index].say
                MultiPlatform:copyToClipboard(ClipboardText .. TEXT_END)
                tts.say("追加复制成功")
                time = false
            else
                self:downCallback()
            end
            print("下滑" .. self.state)
        end
        if shcheduleID then
            print("cancell shcheduleFight")
            scheduler.unscheduleGlobal(shcheduleID)
            shcheduleID = nil
        end
        if shcheduleCopy then
            print("cancell shcheduleCopy")
            scheduler.unscheduleGlobal(shcheduleCopy)
            shcheduleCopy = nil
            time = false
        end
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    self.listener = listener
    listener:registerScriptHandler(onTouchBegan, cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(onTouchEnded, cc.Handler.EVENT_TOUCH_ENDED)
    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, self)
end

-- 右滑 操作
function ClientScene:rightCallback()
    mGameWorld:getSoundMgr():playEff("touch_right")
    -- body
    if self.state == 1 then -- 战斗中
        self.index = self.index + 1
        if self.index > #self.touchTable then
            self.index = 1
        end
        if self.touchTable[self.index] then
            tts.say(self.touchTable[self.index].name)
        end
    elseif self.state == 602 or self.state == 603 or self.state == 604 then
        if self.touchTable[self.index] then
            self.touchTable[self.index].right()
        end
    else -- 目前支持英雄灵台 战役
        self.index = self.index + 1
        if self.index > #self.touchTable then
            self.index = 1
        end
        INDEXTABLE[#self.currentLayer + 1] = self.index
        if self.touchTable[self.index] then
            tts.say(self.touchTable[self.index].say)
            if self.touchTable[self.index].jump then
                self.touchTable[self.index].jump()
            end
        end
    end

    -- if self.state == 101 or self.state == 201 or  self.state == 301 or self.state == 1003 then -- 战役战斗
    if self.touchTable[self.index] then
        if self.touchTable[self.index].touch then
            self.touchTable[self.index].touch()
        end
    end
    -- end

    if self.state == 1003 then
        mGameWorld:getSoundMgr():stopEff()
        self:stopAllActions()
        self.effAction =
            cc.Sequence:create(
            cc.DelayTime:create(1),
            cc.CallFunc:create(
                function()
                    if self.touchTable[self.index].hero then
                        self.touchTable[self.index].hero()
                    end
                end
            )
        )
        self:runAction(self.effAction)
    end
    -- dump(INDEXTABLE)
end

--左滑操作
function ClientScene:leftCallback()
    mGameWorld:getSoundMgr():playEff("touch_left")
    -- body
    if self.state == 1 then -- 战斗中
        self.index = self.index - 1
        if self.index <= 0 then
            self.index = #self.touchTable
        end
        if self.touchTable[self.index] then
            tts.say(self.touchTable[self.index].name)
        end
    elseif self.state == 602 or self.state == 603 or self.state == 604 then
        if self.touchTable[self.index] then
            self.touchTable[self.index].left()
        end
    else -- 目前支持英雄灵台 战役
        self.index = self.index - 1
        if self.index <= 0 then
            self.index = #self.touchTable
        end
        INDEXTABLE[#self.currentLayer + 1] = self.index
        if self.touchTable[self.index] then
            tts.say(self.touchTable[self.index].say)
            if self.touchTable[self.index].jump then
                self.touchTable[self.index].jump()
            end
        end
    end
    -- if self.state == 101 or self.state == 201 or self.state == 301 or self.state == 1003 then -- 战役战斗
    if self.touchTable[self.index] then
        if self.touchTable[self.index].touch then
            self.touchTable[self.index].touch()
        end
    end
    -- end

    if self.state == 1003 then
        mGameWorld:getSoundMgr():stopEff()
        self:stopAllActions()
        self.effAction =
            cc.Sequence:create(
            cc.DelayTime:create(1),
            cc.CallFunc:create(
                function()
                    if self.touchTable[self.index] then
                        if self.touchTable[self.index].hero then
                            self.touchTable[self.index].hero()
                        end
                    end
                end
            )
        )
        self:runAction(self.effAction)
    end
    -- dump(INDEXTABLE)
end

--上滑操作
function ClientScene:upCallback()
    print("state" .. self.state)
    mGameWorld:getSoundMgr():stopEff()
    mGameWorld:getSoundMgr():playEff("touch_up")
    if self.state == 404 then -- 在匹配的时候相当于也发送了取消匹配操作
        mGameWorld:getNetMgr():send({REQUST.EXISTMATCH})
        print("取消匹配")
    end
    if self.state == 504 then -- 等待好友pk界面
        mGameWorld:getNetMgr():send({REQUST.CANCELFRIENTPK})
        print("发送取消PK")
    end
    if self.state == 505 then -- 好友pK界面
        mGameWorld:getNetMgr():send({REQUST.REFUSEFRIENDPK, self.pkMessage.data[1]})
    end
    local t = {
        "全体播报",
        "仅敌方",
        "仅我方"
    }

    if self.state == 1 then -- 战斗中切换播报内容
        PLAY_MODE = PLAY_MODE + 1
        if PLAY_MODE == 4 then
            PLAY_MODE = 1
        end
        cc.UserDefault:getInstance():setIntegerForKey("play_mode", PLAY_MODE)
        print("切换模式：" .. PLAY_MODE)
        tts.say("播报模式" .. t[PLAY_MODE])
    end

    if self.state ~= 1 and self.state ~= 2 and self.state ~= 3 and self.state ~= 402 and self.state ~= 1003 then
        if self.state == 1001 and self.bpvp then
            return
        end
        if self.state == 1101 then -- pass界面
            return
        end
        if self.state == 4 and (GlobalUserItem.guid_pont == 4 or GlobalUserItem.guid_pont == 5) then -- 新手引导相关
            return
        end
        if self.state == 5 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 7 and GlobalUserItem.guid_pont == 7 then -- 新手引导相关
            return
        end
        if self.state == 101 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 102 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 1001 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 1002 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end

        if self.currentLayer[#self.currentLayer] then
            self.currentLayer[#self.currentLayer]:removeFromParent()
            table.remove(self.currentLayer, #self.currentLayer)

            if self.currentLayer[#self.currentLayer] then
                if self.state == 12 then -- 为了刷新 装备被穿戴
                    print("~~~~~~~~~~~~装备页面上滑~~~~~~~~~~~~")
                    mGameWorld:getNetMgr():send({REQUST.GETHEROINFO, HEROID})
                else
                    HEROID = 0
                    self.currentLayer[#self.currentLayer]:initTouch()
                end
            else
                self:initTouch()
            end
        end
    elseif self.state == 1003 then
        self.state = 1002
        self.setHeroLayer:initTouch()
    end
    print(GlobalUserItem.guid_pont)
    if GlobalUserItem.guid_pont == 100 and self.state == 2 then
        mGameWorld:getSoundMgr():playBGM("client_bg3")
        for i = #self.currentLayer, 1, -1 do
            local v = self.currentLayer[i]
            if not tolua.isnull(self.checkPointLayer) then
                if v ~= self.checkPointLayer then
                    table.removebyvalue(self.currentLayer, v)
                    v:removeFromParent()
                end
            end

            if not tolua.isnull(self.rankLayer) then
                if v ~= self.rankLayer then
                    table.removebyvalue(self.currentLayer, v)
                    v:removeFromParent()
                end
            end

            if not tolua.isnull(self.trialLayer) then
                if v ~= self.trialLayer then
                    table.removebyvalue(self.currentLayer, v)
                    v:removeFromParent()
                end
            end
        end
        if not tolua.isnull(self.checkPointLayer) then
            self.checkPointLayer:initTouch()
            print("checkPointLayer")
        end
        if not tolua.isnull(self.rankLayer) then
            self.rankLayer:initTouch()
            print("rankLayer")
        end
        if not tolua.isnull(self.trialLayer) then
            self.trialLayer:initTouch()
            print("trialLayer")
        end
    end
    local ttt = INDEXTABLE

    for k, v in pairs(ttt) do
        if k > (#self.currentLayer + 1) then
            -- print("-----------------------" .. k)
            -- table.remove(INDEXTABLE, k)
            INDEXTABLE[k] = nil
        end
    end
end
--下滑操作  -- 返回菜单
function ClientScene:downCallback()
    mGameWorld:getSoundMgr():stopEff()
    HEROID = 0
    mGameWorld:getSoundMgr():playEff("touch_down")
    print("state:" .. self.state)
    if self.state == 404 then -- 在匹配的时候相当于也发送了取消匹配操作
        mGameWorld:getNetMgr():send({REQUST.EXISTMATCH})
    end
    if self.state == 504 then -- 等待好友pk界面
        mGameWorld:getNetMgr():send({REQUST.CANCELFRIENTPK})
    end
    if self.state == 505 then -- 被好友pk界面
        mGameWorld:getNetMgr():send({REQUST.REFUSEFRIENDPK, self.pkMessage.data[1]})
    end
    if self.state ~= 1 and self.state ~= 3 and self.state ~= 402 then
        if (self.state == 1002 or self.state == 1003 or self.state == 1001) and self.bpvp then
            return
        end
        if self.state == 1101 then -- pass pk
            return
        end
        if self.state == 4 and GlobalUserItem.guid_pont == 4 then -- 新手引导相关
            return
        end
        if self.state == 5 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 7 and GlobalUserItem.guid_pont == 6 then -- 新手引导相关
            return
        end
        if self.state == 101 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 102 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 1001 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 1002 and GlobalUserItem.guid_pont ~= 100 then -- 新手引导相关
            return
        end
        if self.state == 2 then
            mGameWorld:getSoundMgr():playBGM("client_bg3")
        end
        for i = #self.currentLayer, 1, -1 do
            local v = self.currentLayer[i]
            if v then
                table.removebyvalue(self.currentLayer, v)
                v:removeFromParent()
            end
        end
        self.currentLayer = {}
        local a = INDEXTABLE[1]
        INDEXTABLE = {}
        INDEXTABLE[1] = a
        -- if GlobalUserItem.guid_pont == 100 then
        self:initTouch()
    -- end
    end
    -- dump(INDEXTABLE)
    print("下滑后的图层数量" .. #self.currentLayer)
end

-- 引导4 英雄 菜单引导的数据
function ClientScene:dataForguidFour()
    self.state = 3
    self.touchTable = {}
    self.index = 1
    self.touchTable[1] = {say = "成功救下姬昌，快双击进入英雄功能查看姬昌英雄详情吧", func = handler(self, self.menuHero)}
end
--引导5 灵台菜单的引导数据
function ClientScene:dataForguidFive()
    print("英雄界面下滑")
    self.state = 3
    self.touchTable = {}
    self.index = 1
    self.touchTable[1] = {say = "为了天下百姓，我们需要更多的英雄加入，双击进入灵台搜罗天下人才吧", func = handler(self, self.menuSummon)}
end

-- 战役战斗引导
function ClientScene:dataForguidSeven()
    self.state = 3
    self.touchTable = {
        {
            say = "您的队伍已获得新的英雄，一起踏上封神战役战斗的征程吧！双击进入战役战斗。",
            func = handler(self, self.menuPve)
        }
    }
    self.index = 1
end

-- 引导 英雄菜单
function ClientScene:menuHero()
    print("英雄界面打开")
    self.currentLayer[#self.currentLayer + 1] = true
    self.heroLayer = HeroLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.heroLayer
    mGameWorld:getNetMgr():send({REQUST.GETALLHEROS})
end

function ClientScene:onGetAllHeros(data)
    print("获取所有英雄")
    dump(data)
    self.heroLayer:showAll(data)
end

-- 首战
function ClientScene:guidAtk()
    mGameWorld:getNetMgr():send({REQUST.ATKGUIDCELL})
end
-- 战斗开始响应
function ClientScene:onField(data)
    mGameWorld:getSoundMgr():stopEff()
    if not tolua.isnull(self.fightlayer) then
        table.removebyvalue(self.currentLayer, self.endLayer)
        table.removebyvalue(self.currentLayer, self.fightlayer)
        self.fightlayer:removeFromParent()
    end

    self.currentLayer[#self.currentLayer + 1] = true
    self.fightlayer = FightLayer.new(self, data):setPosition(0, 0):addTo(self, 20)
    self.state = 1 -- 战斗中
    self.currentLayer[#self.currentLayer] = self.fightlayer
    self.index = appdf.getIndex(1)
    -- self.index = 1
end

function ClientScene:onAttack(data, data2)
    -- print("=======回合消息===========")
    -- dump(data)
    -- local apos = data[1][1]
    -- print("攻击者pos" .. apos)
    -- local dpos = data[1][2][1][1][1]
    -- print("被攻击者pos：" .. dpos)
    -- local hurt = data[1][2][1][1][2]
    -- print("伤害" .. hurt)
    -- local bUse = data[1][2][1][1][3]
    -- if bUse ~= 0 then
    --     print("~~~~~~~~~技能数据~~~~~~~~~~~")
    --     dump(data[1][2])
    --     print("~~~~~~~~~技能数据~~~~~~~~~~~end")
    -- end
    self.fightlayer:onAttack(data)
end
function ClientScene:onCanUseSkill(heroid, skillid)
    print("==========是否使用大招消息===========")
    self.fightlayer:onCanUseSkill(heroid, skillid)
end
function ClientScene:onShowBattleAward(data, winteam)
    print("===========战斗结算消息=============")
    self.currentLayer[#self.currentLayer + 1] = true
    self.endLayer = EndLayer.new(self, data, winteam)
    self.currentLayer[#self.currentLayer] = self.endLayer
end
-- 操作
function ClientScene:initTouch()
    self.state = 3
    self.index = INDEXTABLE[#self.currentLayer + 1]
    self.bpvp = false -- 是不是封神对战
    FORMATIONSETTING = false
    self.touchTable = {
        {say = "全民集福气活动", func = handler(self, self.menuHappy)},
        {say = "战役战斗", func = handler(self, self.menuPve)},
        {say = "试炼战场", func = handler(self, self.menuPvw)},
        {say = "封神榜", func = handler(self, self.menuPvp)},
        {say = "灵台", func = handler(self, self.menuSummon)},
        {say = "集市", func = handler(self, self.menuShop)},
        {say = "英雄", func = handler(self, self.menuHero)},
        {say = "阵法", func = handler(self, self.menuFront)},
        {say = "背包", func = handler(self, self.menuBag)},
        {say = "万工坊", func = handler(self, self.menuEquip)},
        {say = "公会", func = handler(self, self.menuGuild)},
        {say = "家园", func = handler(self, self.menuHome)},
        {say = "任务", func = handler(self, self.menuTask)},
        {say = "活动", func = handler(self, self.menuActive)},
        {say = "消息", func = handler(self, self.menuMessage)},
        {say = "故事背景", func = handler(self, self.menuStory)},
        {say = "系统设置", func = handler(self, self.menuSetting)},
        {say = "排行榜", func = handler(self, self.menuRank)},
        {say = "官方QQ群236408341，双击复制", func = handler(self, self.menuJoinQQ)}
    }
    if GlobalUserItem.guid_pont == 3 then
        GlobalUserItem.guid_pont = 4
        self:dataForguidFour()
        tts.say("成功救下姬昌，快双击进入英雄功能查看姬昌英雄详情吧")
    elseif GlobalUserItem.guid_pont == 4 then
        self:dataForguidFour()
        tts.say("成功救下姬昌，快双击进入英雄功能查看姬昌英雄详情吧")
    elseif GlobalUserItem.guid_pont == 5 then
        self:dataForguidFive()
        tts.say("为了天下百姓，我们需要更多的英雄加入，双击进入灵台搜罗天下人才吧")
    elseif GlobalUserItem.guid_pont == 7 then
        self:dataForguidSeven()
        tts.say("您的队伍已获得新的英雄，一起踏上封神战役战斗的征程吧！双击进入战役战斗。")
    elseif GlobalUserItem.guid_pont == 8 then
        tts.say("参加战役战斗可以获得英雄碎片、及英雄升级材料是提升英雄实力的重要途经。此外，您可以在菜单界面中的试炼战场，通过参加试炼获得稀有英雄，在封神榜中与其它玩家进行在线对战，争夺封神榜排名奖励。您还可以万工坊中打造出顶级神装，还有更多其他功能等您去探索")
        GlobalUserItem.guid_pont = 100
    elseif GlobalUserItem.guid_pont == 100 then
        tts.say("当前为菜单页面，请左右滑动切换功能")
        if not ACTIVIED then
            ACTIVIED = true
            mGameWorld:getNetMgr():send({REQUST.GETACTIVITYS})
        else
            mGameWorld:getNetMgr():send({REQUST.GETLOGINAWARDINFO})
        end
    end
end

function ClientScene:onGetHeroInfo(data)
    -- print("=========获取英雄信息===========" .. HEROID)
    -- dump(data)
    if HEROID == 0 then
        self.currentLayer[#self.currentLayer + 1] = true
        self.heroInfoLayer = HeroInfoLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.heroInfoLayer
    else
        self.heroInfoLayer:refresh(data)
    end
end

function ClientScene:menuJoinQQ()
    MultiPlatform:copyToClipboard("236408341")
    tts.say("复制成功")
end

function ClientScene:menuRank()
    local url = "http://114.55.170.29/fengshen/index.php?m=getzhanlirank&uid=" .. GlobalUserItem.id .. "&sid=1"
    appdf.onHttpJsionTable(
        url,
        "GET",
        function(jsondata)
            self.currentLayer[#self.currentLayer + 1] = true
            self.rankListLayer = RankListLayer.new(self, jsondata, 3)
            self.currentLayer[#self.currentLayer] = self.rankListLayer
        end
    )
end

function ClientScene:menuSetting()
    self.currentLayer[#self.currentLayer + 1] = true
    self.settingLayer = SettingLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.settingLayer
end

function ClientScene:menuHappy()
    mGameWorld:getNetMgr():send({REQUST.GETGLOBALACTIVITY})
end

function ClientScene:menuPve()
    self.currentLayer[#self.currentLayer + 1] = 1
    self.checkPointLayer = CheckPointLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.checkPointLayer
end
function ClientScene:menuStory()
    self.currentLayer[#self.currentLayer + 1] = true
    self.storyLayer = StoryLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.storyLayer
end

function ClientScene:menuPvw()
    mGameWorld:getNetMgr():send({REQUST.GETSHILIANINFO})
end

function ClientScene:menuFront()
    mGameWorld:getNetMgr():send({REQUST.GETALLFORMATION})
end

function ClientScene:menuEquip()
    self.currentLayer[#self.currentLayer + 1] = true
    self.equipLayer = EquipLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.equipLayer
end

function ClientScene:menuMessage()
    tts.say("未开放")
end

function ClientScene:menuPvp()
    -- mGameWorld:getNetMgr():send({REQUST.GETARENARANK})
    local url = "http://114.55.170.29/fengshen/index.php?m=getsinglerank&uid=" .. GlobalUserItem.id .. "&sid=1"
    appdf.onHttpJsionTable(
        url,
        "GET",
        function(jsondata)
            self.currentLayer[#self.currentLayer + 1] = true
            self.rankLayer = RankLayer.new(self, jsondata)
            self.currentLayer[#self.currentLayer] = self.rankLayer
        end
    )
end

-- 灵台
function ClientScene:menuSummon()
    self.currentLayer[#self.currentLayer + 1] = true
    self.summonLayer = SummonLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.summonLayer
end
-- 集市
function ClientScene:menuShop()
    self.currentLayer[#self.currentLayer + 1] = true
    self.shopLayer = ShopLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.shopLayer
end

-- function ClientScene:menuHero( )

-- end

function ClientScene:menuBag()
    mGameWorld:getNetMgr():send({REQUST.GETBAGINFO})
end

function ClientScene:menuGuild()
    print("~~~~~~~~~~~~~GlobalUserItem.familyid~~~~~~~~~~~~~~~" .. GlobalUserItem.familyid)
    if GlobalUserItem.familyid == -1 then
        self.currentLayer[#self.currentLayer + 1] = true
        self.guildMenu = GuildMenu.new(self)
        self.currentLayer[#self.currentLayer] = self.guildMenu
    else
        mGameWorld:getNetMgr():send({REQUST.GETFAMILYINFO})
    end
end

function ClientScene:menuHome()
    mGameWorld:getNetMgr():send({REQUST.GETUSERINFO})
end

function ClientScene:onGetUserInfo(data)
    -- dump(data)
    GlobalUserItem.skill_point = data[1]
    GlobalUserItem.power = data[2]
    self.currentLayer[#self.currentLayer + 1] = true
    self.homeLayer = HomeLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.homeLayer
end

function ClientScene:menuTask()
    self.currentLayer[#self.currentLayer + 1] = true
    self.taskLayer = TaskLayer.new(self)
    self.currentLayer[#self.currentLayer] = self.taskLayer
end

function ClientScene:menuActive()
    mGameWorld:getNetMgr():send({REQUST.GETACTIVITYS})
end

function ClientScene:onGetActivitys(data)
    -- print("~~~~~~~~~~~~~~onGetActivitys~~~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.activeLayer) then
        self.activeLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.activeLayer = ActiveLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.activeLayer
    end
end

function ClientScene:onGetBagInfo(data)
    -- dump(data)
    if not tolua.isnull(self.bagLayer) then
        self.bagLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.bagLayer = BagLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.bagLayer
    end
end

--英雄合成回调
function ClientScene:onComposeHero()
    tts.say("合成成功")
end
-- 加技能回调
function ClientScene:onAddHeroSkillPoint()
    tts.say("技能经验加1")
    mGameWorld:getNetMgr():send({REQUST.GETHEROINFO, HEROID})
end
--升级回调
function ClientScene:onUpHeroLevel()
    tts.say("升级成功")
    mGameWorld:getSoundMgr():playEff("uplv")
    mGameWorld:getNetMgr():send({REQUST.GETHEROINFO, HEROID})
end
--升星回调
function ClientScene:onUpHeroStar()
    tts.say("升星成功")
    mGameWorld:getSoundMgr():playEff("upstar")
    mGameWorld:getNetMgr():send({REQUST.GETHEROINFO, HEROID})
end

--获取装备列表回调
function ClientScene:onGetEquipList(data)
    if MAKEEQUIP then
        if not tolua.isnull(self.makeEquipLayer) then
            self.makeEquipLayer:refresh(data)
            if self.state == 1013 then
                for i, v in ipairs(data) do
                    if v[1] == EQUIPID then
                        self.equipInfoLayer:refresh(v)
                    end
                end
            end
        else
            self.currentLayer[#self.currentLayer + 1] = true
            self.makeEquipLayer = MakeEquipLayer.new(self, data)
            self.currentLayer[#self.currentLayer] = self.makeEquipLayer
        end
    else
        if self.state == 12 then
            self.setEquipLayer:refresh(data)
        else
            if #data == 0 then
                tts.say("无可用装备")
            else
                self.currentLayer[#self.currentLayer + 1] = true
                self.setEquipLayer = SetEquipLayer.new(self, data)
                self.currentLayer[#self.currentLayer] = self.setEquipLayer
            end
        end
    end
end
-- 错误回调
function ClientScene:onRes(data)
    tts.say(data .. "")
end
-- 召唤英雄
function ClientScene:onCallHero(data)
    -- print("~~~~~~~~~~~onCallHero~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.getHeroLayer) then
        self.getHeroLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.getHeroLayer = GetHeroLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.getHeroLayer
    end
end

function ClientScene:onCallHeroTen(data)
    -- print("~~~~~~~~~~~~onCallHeroTen~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.getHeroTenLayer) then
        self.getHeroTenLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.getHeroTenLayer = GetHeroTenLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.getHeroTenLayer
    end
end

--设置英雄上阵回调
function ClientScene:onSetFormation(data)
    self.setHeroLayer:onSetFormation(data)
end
-- 获取英雄上阵回调
function ClientScene:onGetFormation(data)
    -- dump(data)
    self.currentLayer[#self.currentLayer + 1] = true
    self.setHeroLayer = SetHeroLayer.new(self, data)
    self.currentLayer[#self.currentLayer] = self.setHeroLayer
end

function ClientScene:onChangeGold(data)
    GlobalUserItem.gold = GlobalUserItem.gold + data
    if self.state == 301 then
        self.shopLayer:refresh()
    end
end

function ClientScene:onChangeGem(data)
    GlobalUserItem.gem = GlobalUserItem.gem + data
    if self.state == 301 then
        self.shopLayer:refresh()
    end
end

function ClientScene:onChangeAp(data)
    GlobalUserItem.ap = GlobalUserItem.ap + data
    if data > 0 then
        mGameWorld:getSoundMgr():playEff("ap")
    end
    if self.state == 302 then
        self.apLayer:refresh()
    end
end

function ClientScene:onChangeArenaCnt(data)
    GlobalUserItem.arenacnt = GlobalUserItem.arenacnt + data
    if self.state == 303 then
        self.arenacntLayer:refresh()
    end
end

function ClientScene:onChangeShilianCnt(data)
    GlobalUserItem.shiliancnt = GlobalUserItem.shiliancnt + data
    if self.state == 8 then
        self.shilianLayer:refresh()
    end
end

function ClientScene:onStartMatch(data)
    if data == 1 then
        self.currentLayer[#self.currentLayer + 1] = true
        self.waittingLayer = WaittingLayer.new(self)
        self.currentLayer[#self.currentLayer] = self.waittingLayer
    end
end

function ClientScene:onMatched(power, name)
    -- print("~~~~~~~~~~~~~~~~~~~onMatched~~~~~~~~~~~~~~~~~~~")
    -- print(power, name)
    if name == "rob" then
        name = appdf.randomname()
    end
    ENEMYINFO = "," .. name .. ",战力：" .. power
    self.bpvp = true
    mGameWorld:getNetMgr():send({REQUST.GETALLBATTLEFORMATION})
end

function ClientScene:onSelectFormation(data)
    print("~~~~~~~~~~~~~~~~~~~~~onSelectFormation~~~~~~~~~~~~~~~~~~~~")
    dump(data)
    FORMATIONID = data
    if FORMATIONSETTING == false then
        mGameWorld:getNetMgr():send({REQUST.GETFORMATION})
    else
        mGameWorld:getNetMgr():send({REQUST.GETALLFORMATION})
    end
end

function ClientScene:onGetActivityAward(data)
    -- print("~~~~~~~~~~~~~~~~onGetActivityAward~~~~~~~~~~~~~~~")
    -- dump(data)
    -- {
    --     活动id，
    --         {
    --         金条，
    --         金币，
    --         经验，
    --         装备（列表），
    --         碎片（列表），
    --         技能点，
    --         英雄（列表），
    --         物品（列表）
    --         }
    --     }
    local activeData = mGameData.data["activity"][data[1]]
    local a = data[2]
    local str = ""
    if a[1] ~= 0 then
        str = str .. "金条" .. a[1]
    end
    if a[2] ~= 0 then
        str = str .. "金币" .. a[2]
    end
    if a[3] ~= 0 then
        str = str .. "经验" .. a[3]
    end
    if #a[4] > 0 then
        str = str .. appdf.sayEquip(a[4], "false")
    end
    if #a[5] > 0 then
        str = str .. appdf.sayHeroChip(a[5])
    end
    if a[6] ~= 0 then
        str = str .. "技能点" .. a[6]
    end
    if #a[7] > 0 then
        str = str .. appdf.sayHero(a[7])
    end
    if #a[8] > 0 then
        str = str .. appdf.sayGoods(a[8])
    end
    if data[1] == -1 then
        self:performWithDelay(
            function()
                tts.say("胜利，获得金币66，命之源数量20，装备普通黑砂冠。恭喜完成新手引导，获得新手礼包" .. str .. "下滑返回大厅")
            end,
            0
        )
    else
        tts.say("成功领取" .. activeData.name .. "奖励,获得" .. str)
        self:menuActive()
    end
end

function ClientScene:onGetAllFormation(data1, data2)
    if not tolua.isnull(self.frontLayer) then
        self.frontLayer:refresh(data1, data2)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.frontLayer = FrontLayer.new(self, data1, data2)
        self.currentLayer[#self.currentLayer] = self.frontLayer
    end
end

function ClientScene:onGetAllBattleFormation(data)
    dump(data)
    self.currentLayer[#self.currentLayer + 1] = true
    self.troopSetLayer = TroopSetLayer.new(self, data)
    self.currentLayer[#self.currentLayer] = self.troopSetLayer
end

function ClientScene:onSetFPos(heroid, pos, downhero)
    -- print("~~~~~~~~~~~~~onSetFPos~~~~~~~~~~~~")
    self.setHeroLayer:refresh(heroid, pos, downhero)
end

function ClientScene:onBuyItem()
    tts.say("兑换成功")
end

--装备相关
function ClientScene:onTakeOnEquip()
    mGameWorld:getSoundMgr():playEff("equip")
    mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, EQUIP_NUM})
    tts.say("装备成功")
end

function ClientScene:onTakeOffEquip()
    mGameWorld:getNetMgr():send({REQUST.GETHEROINFO, HEROID})
    tts.say("装备已脱下")
    mGameWorld:getSoundMgr():playEff("equipoff")
end

function ClientScene:onSellEquip(data)
    -- print("~~~~~~~~~~onSellEquip~~~~~~~~~~")
    if data == 0 then
        tts.say("无符合条件装备出售")
    else
        tts.say("出售成功，获得金币" .. data)
        mGameWorld:getNetMgr():send({REQUST.GETBAGINFO})
    end
end

function ClientScene:onForgeEquip(data)
    dump(data)
    if data == 1 then
        mGameWorld:getSoundMgr():playEff("equipup")
    else
        mGameWorld:getSoundMgr():playEff("equipdown")
    end
    mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, EQUIP_NUM})
end

function ClientScene:onSplitEquip(data)
    self:upCallback()
    mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, EQUIP_NUM})
end

function ClientScene:onPunchEquip(data)
    tts.say("打孔成功")
    mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, EQUIP_NUM})
end
--装备相关end
--宝石相关
function ClientScene:onGetStoneList(data)
    if not tolua.isnull(self.stoneListLayer) then
        self.stoneListLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.stoneListLayer = StoneListLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.stoneListLayer
    end
end

function ClientScene:onComponseStone(data)
    mGameWorld:getSoundMgr():playEff("getstone")
    tts.say("合成成功")
    mGameWorld:getNetMgr():send({REQUST.GETSTONELIST})
end

function ClientScene:onComposeAllStone(data)
    mGameWorld:getSoundMgr():playEff("getstone")
    tts.say("合成成功")
    mGameWorld:getNetMgr():send({REQUST.GETSTONELIST})
end

function ClientScene:onMosaicStoneToEquip(data)
    self:upCallback()
    mGameWorld:getSoundMgr():playEff("stoneon")
    tts.say("镶嵌成功")
    mGameWorld:getNetMgr():send({REQUST.GETEQUIPLIST, EQUIP_NUM})
end
--宝石相关end
function ClientScene:onCharged()
    tts.say("充值成功")
    self:performWithDelay(
        function()
            self.gemLayer:initTouch()
        end,
        1
    )
end
--阵法相关
function ClientScene:onStartStudyFormation(data)
    -- print("~~~~~~~~~~~~~~~~~~~~onStartStudyFormation~~~~~~~~~~~~~~~~~~~")
    tts.say("修习中，请耐心等待")
    mGameWorld:getNetMgr():send({REQUST.GETALLFORMATION})
end
function ClientScene:onStopStudyFormation(data)
    -- print("~~~~~~~~~~~~~~~~~~~~onStopStudyFormation~~~~~~~~~~~~~~~~~~~")
    -- dump(data)
    mGameWorld:getNetMgr():send({REQUST.GETALLFORMATION})
end
function ClientScene:onLevelUpFormation(data)
    -- print("~~~~~~~~~~~~~~~~~~~~onLevelUpFormation~~~~~~~~~~~~~~~~~~~")
    -- dump(data)
    tts.say("修习成功")
    mGameWorld:getNetMgr():send({REQUST.GETALLFORMATION})
end
function ClientScene:onComposeFormation(data)
    -- print("~~~~~~~~~~~~~~~~~~~~onComposeFormation~~~~~~~~~~~~~~~~~~~")
    -- dump(data)
    tts.say("合成成功")
    mGameWorld:getNetMgr():send({REQUST.GETALLFORMATION})
end
--阵法相关end

-- 公会相关
function ClientScene:onGetFamilyList(data)
    -- print("~~~~~~~~~~~~~~~onGetFamilyList~~~~~~~~~~~~~~")
    -- dump(data)
    self.currentLayer[#self.currentLayer + 1] = true
    self.guildListLayer = GuildListLayer.new(self, data)
    self.currentLayer[#self.currentLayer] = self.guildListLayer
end

function ClientScene:onGetFamilyInfo(data)
    -- print("~~~~~~~~~~~~~~~~~~~onGetFamilyInfo~~~~~~~~~~~~~~~~~~")
    dump(data)
    if not tolua.isnull(self.guildLayer) then
        self.guildLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.guildLayer = GuildLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.guildLayer
    end
end
function ClientScene:onApplyFamily(data)
    -- print("~~~~~~~~~~~~~~~~~~onApplyFamily~~~~~~~~~~~~~~~~~~~~")
    -- dump(data)
    tts.say("发送申请成功")
end

function ClientScene:onGetFamilyApply(data)
    -- print("~~~~~~~~~~~~~~~~~~~~~onGetFamilyApply~~~~~~~~~~~~~~~~~~~~~~~")
    -- dump(data)
    if #data > 0 then
        if not tolua.isnull(self.applyListLayer) then
            self.applyListLayer:refresh(data)
        else
            self.currentLayer[#self.currentLayer + 1] = true
            self.applyListLayer = ApplyListLayer.new(self, data)
            self.currentLayer[#self.currentLayer] = self.applyListLayer
        end
    else
        tts.say("申请列表为空")
        if not tolua.isnull(self.applyListLayer) then
            self:upCallback()
        end
    end
end

function ClientScene:onAcceptApplyFamily(data)
    -- print("~~~~~~onAcceptApplyFamily~~~~~~~")
    -- dump(data)
    if data == GlobalUserItem.id then
        GlobalUserItem.familyid = 0
    else
        mGameWorld:getNetMgr():send({REQUST.GETFAMILYINFO})
        mGameWorld:getNetMgr():send({REQUST.GETFAMILYAPPLY})
        tts.say("通过操作成功")
    end
end
function ClientScene:onRefuseApplyFamily(data)
    -- print("~~~~~~~~~~onRefuseApplyFamily~~~~~~~~~~")
    -- dump(data)
    if data ~= GlobalUserItem.id then
        mGameWorld:getNetMgr():send({REQUST.GETFAMILYAPPLY})
        tts.say("拒绝操作成功")
    end
end

function ClientScene:onQuitFamily(data)
    -- print("~~~~~~~~~~~~~onQuitFamily~~~~~~~~~~~~")
    -- dump(data)
    GlobalUserItem.familyid = -1
    Pass.new(self, "退出公会成功", 1, 2)
end

function ClientScene:onKickMember(data)
    -- print("~~~~~~~~~~~onKickMember~~~~~~~~~~")
    -- dump(data)
    if data == GlobalUserItem.id then
        GlobalUserItem.familyid = -1
        if self.state == 901 or self.state == 902 or self.state == 903 or self.state == 904 or self.state == 905 or self.state == 906 or self.state == 907 then
            Pass.new(self, "您被移除公会", 1, 2)
        end
    else
        mGameWorld:getNetMgr():send({REQUST.GETFAMILYINFO})
        tts.say("移除操作成功")
    end
end

function ClientScene:onGetFamilyStore(data)
    -- print("~~~~~~~~~~~onGetFamilyStore~~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.guildStoreLayer) then
        self.guildStoreLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.guildStoreLayer = GuildStoreLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.guildStoreLayer
    end
end

function ClientScene:onBuyFamilyItem(data)
    -- print("~~~~~~~~~~onBuyFamilyItem~~~~~~~~~")
    -- dump(data)
    tts.say("兑换成功")
    mGameWorld:getNetMgr():send({REQUST.GETFAMILYSTORE})
end

function ClientScene:onGetFamilyWareHouse(data)
    -- print("~~~~~~~~~~onGetFamilyWareHouse~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.guildWareHouse) then
        self.guildWareHouse:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.guildWareHouse = GuildWareHouse.new(self, data)
        self.currentLayer[#self.currentLayer] = self.guildWareHouse
    end
end

function ClientScene:onDispatchFamilyItem(data)
    -- print("~~~~~~~~~~onDispatchFamilyItem~~~~~~~~~")
    -- dump(data)
    tts.say("分配成功")
    -- mGameWorld:getNetMgr():send({REQUST.GETFAMILYWAREHOUSE})
end

function ClientScene:onRenameFamily(data)
    -- print("~~~~~~~~onRenameFamily~~~~~~~~~~")
    -- dump(data)
    mGameWorld:getNetMgr():send({REQUST.GETFAMILYINFO})
end

function ClientScene:onFetchLeaderGem(data)
    -- print("~~~~~~~~~~~~onFetchLeaderGem~~~~~~~~~~~~")
    -- dump(data)
    tts.say("成功领取" .. data .. "金条")
    if data ~= 0 then
        mGameWorld:getNetMgr():send({REQUST.GETFAMILYINFO})
    end
end
--公会相关end--

-- 好友相关

function ClientScene:onGetFriends(data)
    -- print("~~~~~~~~~~~onGetFriends~~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.friendLayer) then
        self.friendLayer:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.friendLayer = FriendLayer.new(self, data, 1)
        self.currentLayer[#self.currentLayer] = self.friendLayer
    end
end

function ClientScene:onGetOnlineFriends(data)
    -- print("~~~~~~~~~~onGetOnlineFriends~~~~~~~~~~")
    -- dump(data)
    self.currentLayer[#self.currentLayer + 1] = true
    self.friendLayer = FriendLayer.new(self, data, 2)
    self.currentLayer[#self.currentLayer] = self.friendLayer
end

function ClientScene:onGetFriendApplys(data)
    -- print("~~~~~~~~~~~~~~~~~onGetFriendApplys~~~~~~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.friendApply) then
        self.friendApply:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.friendApply = FriendApplyLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.friendApply
    end
end

function ClientScene:onApplyFriend(data)
    -- print("~~~~~~~~~~~onApplyFriend~~~~~~~~~~~~")
    -- dump(data)
    if data == GlobalUserItem.id then
        mGameWorld:getSoundMgr():playEff("friend")
    else
        tts.say("发送申请成功")
    end
end

function ClientScene:onAddFriend(data)
    -- print("~~~~~~~~~~~~~~onAddFriend~~~~~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.friendApply) then
        mGameWorld:getNetMgr():send({REQUST.GETFRIENDAPPLYS})
    end
end

function ClientScene:onDelFriend(data)
    -- print("~~~~~~~~~~~~~~~onDelFriend~~~~~~~~~~~~~")
    -- dump(data)
    tts.say("删除成功")
    mGameWorld:getNetMgr():send({REQUST.GETFRIENDS})
end

function ClientScene:onApplyFriendPk(id, name)
    -- print("~~~~~~~~~~~~onApplyFriendPk~~~~~~~~~~~~~~")
    -- dump(id)

    if name then
        self.currentLayer[#self.currentLayer + 1] = true
        self.pkMessage = PKMessage.new(self, {id, name})
        self.currentLayer[#self.currentLayer] = self.pkMessage
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.waitForFriend = WaitForFriend.new(self, {id, name})
        self.currentLayer[#self.currentLayer] = self.waitForFriend
    end
end

function ClientScene:onCancelFrientPk(data)
    -- print("~~~~~~~~~~onCancelFrientPk~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.pkMessage) then
        Pass.new(self, "对方已经取消PK", 1, 1)
    end
end

function ClientScene:onRefuseFriendPk(data)
    -- print("~~~~~~~~~~onRefuseFriendPk~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.waitForFriend) then
        Pass.new(self, "对方已经取消PK", 1, 1)
    end
end
-- 好友相关end

--试炼相关
function ClientScene:onGetShiLianInfo(data)
    -- print("~~~~~~~onGetShiLianInfo~~~~~~~")
    GlobalUserItem.shilianindex = data[1]
    self.currentLayer[#self.currentLayer + 1] = true
    self.trialLayer = TrialLayer.new(self, data)
    self.currentLayer[#self.currentLayer] = self.trialLayer
end
--试炼相关end

-- 每日登入
function ClientScene:onGetLoginAward(data)
    -- print("~~~~~~~~~~~onGetLoginAward~~~~~~~~~~~~~")
    if data[3] == 1 then
        self.currentLayer[#self.currentLayer + 1] = true
        self.dayGetLayer = DayGetLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.dayGetLayer
    end
end
--每日登入end
-- 每日任务
function ClientScene:onGetDailyTask(data)
    -- print("~~~~~~~~~~~onGetDailyTask~~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.taskForDaily) then
        self.taskForDaily:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.taskForDaily = TaskForDaily.new(self, data)
        self.currentLayer[#self.currentLayer] = self.taskForDaily
    end
end

function ClientScene:onGetCityTask(data)
    print("~~~~~~~~~~~~~~~~~onGetCityTask~~~~~~~~~~~~~~~~")
    -- dump(data)
    if not tolua.isnull(self.taskForCity) then
        self.taskForCity:refresh(data)
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.taskForCity = TaskForCity.new(self, data)
        self.currentLayer[#self.currentLayer] = self.taskForCity
    end
end

function ClientScene:onGetGlobalAward(data)
    mGameWorld:getNetMgr():send(REQUST.GETGLOBALACTIVITY)
end

function ClientScene:onFetchDailyTask()
    -- tts.say("领取成功")
    mGameWorld:getNetMgr():send({REQUST.GETDAILYTASK})
end

function ClientScene:onFetchCityTask()
    -- tts.say("领取成功")
    mGameWorld:getNetMgr():send({REQUST.GETCITYTASK})
end
-- 每日任务end

function ClientScene:onGetArenaRank(data)
    -- print("~~~~~~~~~~~~onGetArenaRank~~~~~~~~~~~")
    -- dump(data)
end

function ClientScene:onRenameNick(data)
    -- print("~~~~~~~~~~~~onRenameNick~~~~~~~~~~~~~`")
    -- dump(data)
    tts.say("修改昵称成功,当前昵称" .. data)
    GlobalUserItem.nick = data
end

function ClientScene:onGetGlobalActivity(data)
    if not tolua.isnull(self.happyLayer) then
        self.happyLayer:refresh()
    else
        self.currentLayer[#self.currentLayer + 1] = true
        self.happyLayer = HappyLayer.new(self, data)
        self.currentLayer[#self.currentLayer] = self.happyLayer
    end
end

function ClientScene:onEnter()
    -- print("进入大厅==========")
    print(GlobalUserItem.guid_pont)
    -- 正常进入大厅
    if GlobalUserItem.guid_pont == 3 then
        self:guidAtk()
        print("发送首战消息")
    elseif GlobalUserItem.guid_pont == 4 then
        self:dataForguidFour()
        tts.say("成功救下姬昌，快双击进入英雄功能查看姬昌英雄详情吧")
    elseif GlobalUserItem.guid_pont == 5 or GlobalUserItem.guid_pont == 6 then
        self:dataForguidFive()
        tts.say("为了天下百姓，我们需要更多的英雄加入，双击进入灵台搜罗天下人才吧")
    elseif GlobalUserItem.guid_pont == 7 then
        self:dataForguidSeven()
        tts.say("您的队伍已获得新的英雄，一起踏上封神战役战斗的征程吧！双击进入战役战斗。")
    else
        self:initTouch()
    end
    mGameWorld:getSoundMgr():playBGM("client_bg3")

    mGameWorld:getNetMgr():addRegist(RESPONSE.FIELD, handler(self, self.onField))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ATTACK, handler(self, self.onAttack))
    mGameWorld:getNetMgr():addRegist(RESPONSE.CANUSESKILL, handler(self, self.onCanUseSkill))
    mGameWorld:getNetMgr():addRegist(RESPONSE.SHOWBATTLEAWARD, handler(self, self.onShowBattleAward))
    mGameWorld:getNetMgr():addRegist(RESPONSE.GETALLHEROS, handler(self, self.onGetAllHeros))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETHEROINFO, handler(self, self.onGetHeroInfo))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCOMPOSEHERO, handler(self, self.onComposeHero))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONADDHEROSKILLPOINT, handler(self, self.onAddHeroSkillPoint))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONUPHEROLEVEL, handler(self, self.onUpHeroLevel))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONUPHEROSTAR, handler(self, self.onUpHeroStar))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETEQUIPLIST, handler(self, self.onGetEquipList))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCALLHERO, handler(self, self.onCallHero))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFORMATION, handler(self, self.onGetFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSETFORMATION, handler(self, self.onSetFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETBAGINFO, handler(self, self.onGetBagInfo))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCHANGEGOLD, handler(self, self.onChangeGold))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCHANGEGEM, handler(self, self.onChangeGem))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCHANGEAP, handler(self, self.onChangeAp))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCHANGEARENACNT, handler(self, self.onChangeArenaCnt))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONBUYITEM, handler(self, self.onBuyItem))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSTARTMATCH, handler(self, self.onStartMatch))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONTAKEONEQUIP, handler(self, self.onTakeOnEquip))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONMATCHED, handler(self, self.onMatched))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETACTIVITYAWARD, handler(self, self.onGetActivityAward))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETALLFORMATION, handler(self, self.onGetAllFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETACTIVITYS, handler(self, self.onGetActivitys))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONTAKEOFFEQUIP, handler(self, self.onTakeOffEquip))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSELECTFORMATION, handler(self, self.onSelectFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETUSERINFO, handler(self, self.onGetUserInfo))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSETFPOS, handler(self, self.onSetFPos))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCHARGED, handler(self, self.onCharged))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSELLEQUIP, handler(self, self.onSellEquip))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONRES, handler(self, self.onRes))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFAMILYINFO, handler(self, self.onGetFamilyInfo))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFAMILYLIST, handler(self, self.onGetFamilyList))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFAMILYAPPLY, handler(self, self.onGetFamilyApply))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONACCEPTAPPLYFAMILY, handler(self, self.onAcceptApplyFamily))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONREFUSEAPPLYFAMILY, handler(self, self.onRefuseApplyFamily))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONAPPLYFAMILY, handler(self, self.onApplyFamily))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONQUITFAMILY, handler(self, self.onQuitFamily))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONKICKMEMBER, handler(self, self.onKickMember))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFRIENDS, handler(self, self.onGetFriends))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETONLINEFRIENDS, handler(self, self.onGetOnlineFriends))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFRIENDAPPLYS, handler(self, self.onGetFriendApplys))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONAPPLYFRIEND, handler(self, self.onApplyFriend))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONADDFRIEND, handler(self, self.onAddFriend))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONDELFRIEND, handler(self, self.onDelFriend))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONAPPLYFRIENDPK, handler(self, self.onApplyFriendPk))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCANCELFRIENTPK, handler(self, self.onCancelFrientPk))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONREFUSEFRIENDPK, handler(self, self.onRefuseFriendPk))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCHANGESHILIANCNT, handler(self, self.onChangeShilianCnt))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETSHILIANINFO, handler(self, self.onGetShiLianInfo))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSTARTSTUDYFORMATION, handler(self, self.onStartStudyFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSTOPSTUDYFORMATION, handler(self, self.onStopStudyFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONLEVELUPFORMATION, handler(self, self.onLevelUpFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCOMPOSEFORMATION, handler(self, self.onComposeFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETALLBATTLEFORMATION, handler(self, self.onGetAllBattleFormation))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETLOGINAWARD, handler(self, self.onGetLoginAward))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONRENAMENICK, handler(self, self.onRenameNick))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCALLHEROTEN, handler(self, self.onCallHeroTen))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFAMILYSTORE, handler(self, self.onGetFamilyStore))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONBUYFAMILYITEM, handler(self, self.onBuyFamilyItem))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETFAMILYWAREHOUSE, handler(self, self.onGetFamilyWareHouse))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONDISPATCHFAMILYITEM, handler(self, self.onDispatchFamilyItem))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETDAILYTASK, handler(self, self.onGetDailyTask))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETCITYTASK, handler(self, self.onGetCityTask))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONFETCHDAILYTASK, handler(self, self.onFetchDailyTask))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONFETCHCITYTASK, handler(self, self.onFetchCityTask))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETARENARANK, handler(self, self.onGetArenaRank))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONRENAMEFAMILY, handler(self, self.onRenameFamily))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONFETCHLEADERGEM, handler(self, self.onFetchLeaderGem))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETGLOBALACTIVITY, handler(self, self.onGetGlobalActivity))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETGLOBALAWARD, handler(self, self.onGetGlobalAward))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONFORGEEQUIP, handler(self, self.onForgeEquip))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONSPLITEQUIP, handler(self, self.onSplitEquip))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONPUNCHEQUIP, handler(self, self.onPunchEquip))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONGETSTONELIST, handler(self, self.onGetStoneList))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCOMPONSESTONE, handler(self, self.onComponseStone))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONCOMPOSEALLSTONE, handler(self, self.onComposeAllStone))
    mGameWorld:getNetMgr():addRegist(RESPONSE.ONMOSAICSTONETOEQUIP, handler(self, self.onMosaicStoneToEquip))
    self.events[1] =
        mGameWorld:getEventMgr():addEvent(
        EventConst.EVENT_SERVER_CLOSED,
        function()
            require("app.MyApp").new():run()
        end
    )
end

function ClientScene:onExit()
    mGameWorld:getNetMgr():remRegist(RESPONSE.FIELD, handler(self, self.onField))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ATTACK, handler(self, self.onAttack))
    mGameWorld:getNetMgr():remRegist(RESPONSE.CANUSESKILL, handler(self, self.onCanUseSkill))
    mGameWorld:getNetMgr():remRegist(RESPONSE.SHOWBATTLEAWARD, handler(self, self.onShowBattleAward))
    mGameWorld:getNetMgr():remRegist(RESPONSE.GETALLHEROS, handler(self, self.onGetAllHeros))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETHEROINFO, handler(self, self.onGetHeroInfo))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCOMPOSEHERO, handler(self, self.onComposeHero))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONADDHEROSKILLPOINT, handler(self, self.onAddHeroSkillPoint))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONUPHEROLEVEL, handler(self, self.onUpHeroLevel))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONUPHEROSTAR, handler(self, self.onUpHeroStar))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETEQUIPLIST, handler(self, self.onGetEquipList))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCALLHERO, handler(self, self.onCallHero))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETFORMATION, handler(self, self.onGetFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSETFORMATION, handler(self, self.onSetFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETBAGINFO, handler(self, self.onGetBagInfo))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCHANGEGOLD, handler(self, self.onChangeGold))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCHANGEGEM, handler(self, self.onChangeGem))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCHANGEAP, handler(self, self.onChangeAp))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCHANGEARENACNT, handler(self, self.onChangeArenaCnt))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONBUYITEM, handler(self, self.onBuyItem))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSTARTMATCH, handler(self, self.onStartMatch))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONTAKEONEQUIP, handler(self, self.onTakeOnEquip))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONMATCHED, handler(self, self.onMatched))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETACTIVITYAWARD, handler(self, self.onGetActivityAward))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETALLFORMATION, handler(self, self.onGetAllFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETACTIVITYS, handler(self, self.onGetActivitys))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONTAKEOFFEQUIP, handler(self, self.onTakeOffEquip))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSELECTFORMATION, handler(self, self.onSelectFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETUSERINFO, handler(self, self.onGetUserInfo))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSETFPOS, handler(self, self.onSetFPos))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCHARGED, handler(self, self.onCharged))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSELLEQUIP, handler(self, self.onSellEquip))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONRES, handler(self, self.onRes))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETFAMILYINFO, handler(self, self.onGetFamilyInfo))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETFAMILYLIST, handler(self, self.onGetFamilyList))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONACCEPTAPPLYFAMILY, handler(self, self.onAcceptApplyFamily))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONREFUSEAPPLYFAMILY, handler(self, self.onRefuseApplyFamily))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONAPPLYFAMILY, handler(self, self.onApplyFamily))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONQUITFAMILY, handler(self, self.onQuitFamily))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONKICKMEMBER, handler(self, self.onKickMember))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETFRIENDS, handler(self, self.onGetFriends))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETONLINEFRIENDS, handler(self, self.onGetOnlineFriends))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETFRIENDAPPLYS, handler(self, self.onGetFriendApplys))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONAPPLYFRIEND, handler(self, self.onApplyFriend))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONADDFRIEND, handler(self, self.onAddFriend))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONDELFRIEND, handler(self, self.onDelFriend))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONAPPLYFRIENDPK, handler(self, self.onApplyFriendPk))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCANCELFRIENTPK, handler(self, self.onCancelFrientPk))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONREFUSEFRIENDPK, handler(self, self.onRefuseFriendPk))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCHANGESHILIANCNT, handler(self, self.onChangeShilianCnt))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETSHILIANINFO, handler(self, self.onGetShiLianInfo))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSTARTSTUDYFORMATION, handler(self, self.onStartStudyFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSTOPSTUDYFORMATION, handler(self, self.onStopStudyFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONLEVELUPFORMATION, handler(self, self.onLevelUpFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCOMPOSEFORMATION, handler(self, self.onComposeFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETALLBATTLEFORMATION, handler(self, self.onGetAllBattleFormation))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETLOGINAWARD, handler(self, self.onGetLoginAward))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONRENAMENICK, handler(self, self.onRenameNick))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCALLHEROTEN, handler(self, self.onCallHeroTen))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETFAMILYSTORE, handler(self, self.onGetFamilyStore))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONBUYFAMILYITEM, handler(self, self.onBuyFamilyItem))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETFAMILYWAREHOUSE, handler(self, self.onGetFamilyWareHouse))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONDISPATCHFAMILYITEM, handler(self, self.onDispatchFamilyItem))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETDAILYTASK, handler(self, self.onGetDailyTask))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETCITYTASK, handler(self, self.onGetCityTask))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONFETCHDAILYTASK, handler(self, self.onFetchDailyTask))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONFETCHCITYTASK, handler(self, self.onFetchCityTask))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETARENARANK, handler(self, self.onGetArenaRank))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONRENAMEFAMILY, handler(self, self.onRenameFamily))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONFETCHLEADERGEM, handler(self, self.onFetchLeaderGem))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETGLOBALACTIVITY, handler(self, self.onGetGlobalActivity))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETGLOBALAWARD, handler(self, self.onGetGlobalAward))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONFORGEEQUIP, handler(self, self.onForgeEquip))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONSPLITEQUIP, handler(self, self.onSplitEquip))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONPUNCHEQUIP, handler(self, self.onPunchEquip))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONGETSTONELIST, handler(self, self.onGetStoneList))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCOMPONSESTONE, handler(self, self.onComponseStone))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONCOMPOSEALLSTONE, handler(self, self.onComposeAllStone))
    mGameWorld:getNetMgr():remRegist(RESPONSE.ONMOSAICSTONETOEQUIP, handler(self, self.onMosaicStoneToEquip))
    for _, evt in pairs(self.events) do
        mGameWorld:getEventMgr():remEvent(evt)
    end
    self.events = {}
    self.currentLayer = nil
    HEROID = 0
end

return ClientScene
