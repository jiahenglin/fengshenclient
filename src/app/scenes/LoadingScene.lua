local LoadingScene =
    class(
    "LoadingScene",
    function()
        return display.newScene("LoadingScene")
    end
)

local LoginScene = require "app.scenes.LoginScene"

function LoadingScene:ctor()
    display.newTTFLabel({text = "LOADING...", size = 64}):align(display.CENTER, display.cx, display.cy):addTo(self)
end

function LoadingScene:onEnter()
    self.time = 0
    -- tts.say("加载文件中")
    audio.loadFile(
        "res/sound/login/login.ogg",
        function()
            mGameWorld:getSoundMgr():init()
            mGameWorld:getSoundMgr():playEff("login")
            self:performWithDelay(
                function()
                    print("~~~~~~~~onEnter~~~~~~~~~~")
                    mGameWorld:getNetMgr():connect()

                    -- self:runAction(
                    --     cc.RepeatForever:create(
                    --         cc.Sequence:create(
                    --             cc.CallFunc:create(
                    --                 function()
                    --                     mGameWorld:getNetMgr():connect()
                    --                     self.time = self.time + 1
                    --                     if self.time == 6 then
                    --                         tts.say("连接不上服务器")
                    --                         self:stopAllActions()
                    --                     end
                    --                 end
                    --             ),
                    --             cc.DelayTime:create(4)
                    --         )
                    --     )
                    -- )
                end,
                6
            )
        end
    )
    self.events = {}
    self.events[1] =
        mGameWorld:getEventMgr():addEvent(
        EventConst.EVENT_SERVER_CONNECTED,
        function()
            local scene = LoginScene.new()
            display.replaceScene(scene)
        end
    )
end
function LoadingScene:onExit()
    for _, evt in pairs(self.events) do
        mGameWorld:getEventMgr():remEvent(evt)
    end
    self.events = {}
end

return LoadingScene
