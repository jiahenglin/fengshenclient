local LoginScene =
    class(
    "LoginScene",
    function()
        return display.newScene("LoginScene")
    end
)

local ClientScene = require("app.scenes.ClientScene")
require("app.utils.utils")
local mGameData = require("app.template.gamedata")

function LoginScene:ctor()
    local csbNode = cc.CSLoader:createNode("login/LoginScene.csb")
    csbNode:addTo(self)
    csbNode:align(display.CENTER, display.cx, display.cy)
    self._csbNode = csbNode
    self._bg = csbNode:getChildByName("bg")
    local a = self._bg:getContentSize()
    self._bg:setScale(display.width / a.width, display.height / a.height)
    self.boxbg = csbNode:getChildByName("boxbg")
    self.infobg = csbNode:getChildByName("infobg")
    --登入注册界面
    self.tipbg = csbNode:getChildByName("tipbg") -- 提示语界面
    self.areabg = self.infobg:getChildByName("areabg") -- 选择区服
    self.namebg = self.infobg:getChildByName("namebg") -- 取名界面
    self.ckb_boy = self.infobg:getChildByName("ckb_boy") -- 选择性别男
    self.ckb_girl = self.infobg:getChildByName("ckb_girl") -- 选择性别女

    self.tipbg:getChildByName("tip3_2"):setString(tts.speed)
    self.index = 1
    self:addEventListener()
    self:init()
    self.events = {}
    local b = display.newSprite("common/shader.png"):addTo(self, 1000):align(display.CENTER, display.cx, display.cy):setOpacity(OPACITY)
    local a = b:getContentSize()
    b:setScale(display.width / a.width, display.height / a.height)
end
--添加触摸监听
function LoginScene:addEventListener()
    local x, y, a, b
    local click = false
    local function onTouchBegan(touch, event)
        local pos = touch:getLocation()
        x = pos.x
        y = pos.y

        if click then
            click = false
            if self.touchTable[self.index] then
                self.touchTable[self.index].func()
            end
            print("双击")
            mGameWorld:getSoundMgr():playEff("click_double")
        else
            self:runAction(
                cc.Sequence:create(
                    cc.DelayTime:create(0.25),
                    cc.CallFunc:create(
                        function()
                            if click then
                                click = false
                            end
                        end
                    )
                )
            )
            click = true
        end

        return true
    end

    local function onTouchEnded(touch, event)
        local pos = touch:getLocation()
        a = pos.x
        b = pos.y
        if a - x < -TOUCH_LIMIT and math.abs(a - x) > math.abs(b - y) then
            self:leftCallback()
            click = false
        elseif a - x > math.abs(b - y) and a - x > TOUCH_LIMIT then
            self:rightCallback()
            click = false
        elseif b - y > math.abs(a - x) and b - y > TOUCH_LIMIT then
            self:upCallback()
            click = false
        elseif b - y < -TOUCH_LIMIT and math.abs(b - y) > math.abs(a - x) then
            self:downCallback()
            click = false
        end
    end

    local listener = cc.EventListenerTouchOneByOne:create()
    self.listener = listener
    listener:registerScriptHandler(onTouchBegan, cc.Handler.EVENT_TOUCH_BEGAN)
    listener:registerScriptHandler(onTouchEnded, cc.Handler.EVENT_TOUCH_ENDED)
    local eventDispatcher = self:getEventDispatcher()
    eventDispatcher:addEventListenerWithSceneGraphPriority(listener, self)
end

function LoginScene:init()
    -- GameVersion = "1.0.2"
    display.newTTFLabel({text = "GameVersion:" .. GameVersion, size = 30}):align(0, 0):addTo(self):setAnchorPoint(0,0)
    self:stage1()
end
-- 右滑
function LoginScene:rightCallback()
    mGameWorld:getSoundMgr():playEff("touch_right")
    if self.state == 1 or self.state == 5 then
        self.index = self.index + 1
        if self.index > #self.touchTable then
            self.index = 1
        end
        tts.say(self.touchTable[self.index].say)
    end

    if self.state == 2 and self.index == 2 then
        tts.speed = tts.speed + 1
        if tts.speed > MAX then
            tts.speed = MAX
        end
        cc.UserDefault:getInstance():setFloatForKey("speed", tts.speed)
        tts.say(GAME_WORDS[6] .. tts.speed)
        self.tipbg:getChildByName("tip3_2"):setString(tts.speed)
    end

    if self.state == 4 then
        GlobalUserItem.nick = self:randomname()
        print(GlobalUserItem.nick)
        self.namebg:getChildByName("text1"):setString(GlobalUserItem.nick)
        tts.say(GlobalUserItem.nick)
    end
end

--左滑
function LoginScene:leftCallback()
    mGameWorld:getSoundMgr():playEff("touch_left")
    if self.state == 1 or self.state == 5 then
        self.index = self.index - 1
        if self.index < 1 then
            self.index = #self.touchTable
        end
        tts.say(self.touchTable[self.index].say)
    end

    if self.state == 2 and self.index == 2 then
        tts.speed = tts.speed - 1
        if tts.speed < MIN then
            tts.speed = MIN
        end
        cc.UserDefault:getInstance():setFloatForKey("speed", tts.speed)
        tts.say(GAME_WORDS[6] .. tts.speed)
        self.tipbg:getChildByName("tip3_2"):setString(tts.speed)
    end

    if self.state == 4 then
        GlobalUserItem.nick = self:randomname()
        print(GlobalUserItem.nick)
        self.namebg:getChildByName("text1"):setString(GlobalUserItem.nick)
        tts.say(GlobalUserItem.nick)
    end
end
--上滑
function LoginScene:upCallback()
    -- body
    mGameWorld:getSoundMgr():playEff("touch_up")
end
--下滑
function LoginScene:downCallback()
    -- body
    mGameWorld:getSoundMgr():playEff("touch_down")
end

-- 阶段一
function LoginScene:stage1()
    tts.say(GAME_WORDS[1])
    -- body
    local deviceId = device:getOpenUDID()
    self.touchTable = {}
    self.touchTable[1] = {
        say = "自动登入",
        func = function()
            print("loading....")
            -- mGameWorld:getNetMgr():connect()
            self:performWithDelay(
                function()
                    local account = cc.UserDefault:getInstance():getStringForKey("account")
                    local psw = cc.UserDefault:getInstance():getStringForKey("password")
                    print("account:" .. account .. "\npsw:" .. psw)
                    if account ~= "" and psw ~= "" then
                        -- mGameWorld:getNetMgr():send({REQUST.REGISTER, deviceId, '9999', 'password'}) --登入
                        mGameWorld:getNetMgr():send({REQUST.REGISTER, deviceId, account, psw, 1, "1"}) --注册
                    else
                        self:openWebView(deviceId)
                    end
                end,
                -- 0.5
                0
            )
        end
    }
    self.touchTable[2] = {
        say = "切换账号",
        func = function()
            self:openWebView(deviceId)
        end
    }
    self.state = 1
    self.index = 1
end

-- 阶段二
function LoginScene:stage2()
    self.state = 2
    self.index = 1 -- 阶段二初始化index值
    self.boxbg:setVisible(false) -- 登入注册页面隐藏
    self.tipbg:setVisible(true) -- 显示提示语界面
    self.tipbg:getChildByName("tip1"):setVisible(true)

    local next = function()
        self.index = self.index + 1
        for i = 1, 4, 1 do
            self.tipbg:getChildByName("tip3_1"):setVisible(false)
            self.tipbg:getChildByName("tip3_2"):setVisible(false)
            self.tipbg:getChildByName("tip" .. i):setVisible(false)
        end
        self.tipbg:getChildByName("tip" .. self.index):setVisible(true)
        if self.index == 2 then
            self.tipbg:getChildByName("tip3_1"):setVisible(true)
            self.tipbg:getChildByName("tip3_2"):setVisible(true)
        end
        -- body
        tts.say(self.touchTable[self.index].say)
    end

    self.touchTable = {}
    self.touchTable[1] = {say = GAME_WORDS[7], func = next}
    self.touchTable[3] = {say = GAME_WORDS[8], func = next}
    self.touchTable[2] = {say = GAME_WORDS[9] .. tts.speed, func = next}
    self.touchTable[4] = {say = XIEYI, func = handler(self, self.guid)}
end

function LoginScene:setInfo()
    self.state = 5
    tts.say("个人信息，左右滑动编辑个人信息")
    self.tipbg:setVisible(false)
    self.infobg:setVisible(true)
    self.touchTable = {}
    GlobalUserItem.nick = self:randomname()
    self.namebg:getChildByName("text1"):setString(GlobalUserItem.nick)
    local bs = true
    GlobalUserItem.gender = "男"
    self.touchTable = {
        [1] = {
            say = "一区 变换莫测",
            func = function()
                print("区服编辑")
            end
        },
        [2] = {
            say = "昵称" .. GlobalUserItem.nick .. ",双击切换昵称",
            func = function()
                GlobalUserItem.nick = self:randomname()
                self.namebg:getChildByName("text1"):setString(GlobalUserItem.nick)
                tts.say(GlobalUserItem.nick)
                self.touchTable[2].say = "昵称" .. GlobalUserItem.nick .. ",双击切换昵称"
            end
        },
        [3] = {
            say = "性别，男,双击切换性别",
            func = function()
                bs = not bs
                self.ckb_boy:setSelected(bs)
                self.ckb_girl:setSelected(not bs)
                if bs then
                    GlobalUserItem.gender = "男"
                else
                    GlobalUserItem.gender = "女"
                end
                tts.say(GlobalUserItem.gender)
                self.touchTable[3].say = "性别" .. GlobalUserItem.gender .. ",双击切换性别"
            end
        },
        [4] = {
            say = "确认信息 双击提交信息",
            func = function()
                mGameWorld:getNetMgr():send({REQUST.SETSERVERID, 1})
                mGameWorld:getNetMgr():send({REQUST.SETNICK, GlobalUserItem.nick})
                mGameWorld:getNetMgr():send({REQUST.SETGENDER, self.index})
                self:guid(3)
            end
        }
    }
end
--引导阶段
function LoginScene:guid(guid_pont)
    local guid
    guid = guid_pont or GlobalUserItem.guid_pont
    print("引导阶段数值:" .. guid)
    GlobalUserItem.guid_pont = guid
    self.index = 1 -- 引导阶段初始化index值
    if guid == 3 then
        --             end
        --         )
        --     )
        -- )
        self.tipbg:setVisible(false)
        self.areabg:setVisible(false)
        self.namebg:setVisible(false)
        self.ckb_boy:setVisible(false)
        self.ckb_girl:setVisible(false)
        -- self:guidAtk()
        self.state = 6
        self.touchTable = {}
        tts.say("")
        mGameWorld:getSoundMgr():playBGM("story_0", false)
        self:performWithDelay(
            function()
                tts.say("片花，双击可跳过")
            end,
            10
        )
        -- self:runAction(
        --     cc.Sequence:create(
        --         cc.DelayTime:create(95),  -- 95
        --         cc.CallFunc:create(
        --             function()
        self:stateSixData()
    elseif guid >= 4 then
        mGameWorld:getSoundMgr():playBGM("story_0", false)
        self:performWithDelay(
            function()
                self:enterClient()
            end,
            95 -- 时间
        )
        self.touchTable = {}
        tts.say("")
        self.touchTable[1] = {say = "", func = handler(self, self.enterClient)}
    else
        self:setInfo()
    end
end
-- self.state = 6
function LoginScene:stateSixData()
    self.touchTable = {}
    self.touchTable[1] = {say = "英雄，双击屏幕开始战斗吧", func = handler(self, self.enterClient)}
    tts.say("英雄，双击屏幕开始战斗吧")
end

function LoginScene:enterClient()
    print("进入大厅")
    local scene = ClientScene.new()
    display.replaceScene(scene)
end
function LoginScene:choseServer()
    mGameWorld:getNetMgr():send({REQUST.SETSERVERID, 1})
    self:guid(1)
end
--self.state = 3
function LoginScene:stateThreeData()
    self.touchTable = {}
    self.touchTable[1] = {say = GAME_WORDS[11], func = handler(self, self.choseServer)}
end
--随机生成名字
function LoginScene:randomname()
    local mytime = os.time()
    mytime = string.reverse(mytime)
    math.randomseed(mytime)

    local name = ""
    name =
        mGameData.data["nick"][math.random(#mGameData.data["nick"])]["name1"] ..
        mGameData.data["nick"][math.random(#mGameData.data["nick"])]["name2"] .. mGameData.data["nick"][math.random(#mGameData.data["nick"])]["name3"]
    return name
end
--self.state = 4
function LoginScene:stateFourData()
    self.touchTable = {}
    GlobalUserItem.nick = self:randomname()
    self.namebg:getChildByName("text1"):setString(GlobalUserItem.nick)
    tts.say(GAME_WORDS[13] .. GlobalUserItem.nick)
    self.touchTable[1] = {func = handler(self, self.choseNick)}
end
-- 选择姓名
function LoginScene:choseNick()
    mGameWorld:getNetMgr():send({REQUST.SETNICK, GlobalUserItem.nick})
    self:guid(2)
end
--self.state = 5
function LoginScene:stateFiveData()
    self.touchTable = {}
    self.touchTable[1] = {say = "男", func = handler(self, self.choseGender)}
    self.touchTable[2] = {say = "女", func = handler(self, self.choseGender)}
end
--选择性别
function LoginScene:choseGender()
    print("发送消息性别")
    mGameWorld:getNetMgr():send({REQUST.SETGENDER, self.index})
    self:guid(3)
end

function LoginScene:onRegister(data, data1) -- 账号信息 ，英雄信息
    -- dump(data)
    GlobalUserItem.setData(data)
    self:stage2()
    tts.say(GAME_WORDS[7])

    self:tick()
end

function LoginScene:onEnter()
    mGameWorld:getNetMgr():addRegist(RESPONSE.LOGINED, handler(self, self.onRegister))
    mGameWorld:getNetMgr():addRegist(RESPONSE.TEST, handler(self, self.onTest))
    self.events[1] =
        mGameWorld:getEventMgr():addEvent(
        EventConst.EVENT_SERVER_CLOSED,
        function()
            print("~~~~~~~EVENT_SERVER_CLOSED~~~~~~~~~~")
            require("app.MyApp").new():run()
        end
    )
end

function LoginScene:onExit()
    mGameWorld:getNetMgr():remRegist(RESPONSE.LOGINED, handler(self, self.onRegister))
    mGameWorld:getNetMgr():remRegist(RESPONSE.TEST, handler(self, self.onTest))
    for _, evt in pairs(self.events) do
        mGameWorld:getEventMgr():remEvent(evt)
    end
    self.events = {}
end

function LoginScene:openWebView(deviceId)
    local url = WEB_HOST .. "fengshen/index.html"

    if device.platform == "ios" then
        luaoc.callStaticMethod("AppController", "openWebView", {url = url, cb = utils.WebViewCallback})
        return
    elseif device.platform == "android" then
        luaj.callStaticMethod("org/cocos2dx/lua/AppActivity", "openLoginView", {}, "()V")
        return
    end

    -- local pos = cc.p(display.cx, -display.cy)
    -- local wsize = cc.size(display.height,display.width)
    local pos = cc.p(display.cy, 0)
    local wsize = cc.size(display.height, display.width + 200)
    local view =
        utils.createWebView(
        url,
        pos,
        wsize,
        function(sender, data)
            -- //处理注册
            mGameWorld:getNetMgr():send({REQUST.REGISTER, deviceId, data, "password", 1, "1"}) --注册
            sender:removeSelf()
            self:performWithDelay(
                function()
                    utils.changeViewDirection(1)
                end,
                0.1
            )
            -- sender:runAction(cc.Sequence:create(cc.MoveTo:create(0.3, cc.p(display.cx, -display.cy)), cc.RemoveSelf:create()))
        end
    )
    view:addTo(self, 100000)
    -- view:runAction(cc.MoveTo:create(0.3, cc.p(display.cx, display.cy)))
end

function LoginScene:tick()
    mGameWorld:getTimeMgr():startTick()
end

return LoginScene
