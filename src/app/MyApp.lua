require("config")
require("cocos.init")
require("framework.init")

local AppBase = require("framework.AppBase")
local MyApp = class("MyApp", AppBase)

function MyApp:ctor()
    MyApp.super.ctor(self)
    require("app.const.Const")
    require("app.const.Chns")
    require("app.components.init")
    require("app.const.GlobalUserItem")
    require("app.const.AppDF")
    require("app.const.GameConfig")
    require("app.const.EventConst")
end

function MyApp:run()
    cc.FileUtils:getInstance():addSearchPath("res/")

    -- cc(self)
    mGameWorld = require("app.GameWorld").new()
    tts = require("app.const.TTSVoice")

    -- print("~~~~~~~@@@@@@@@~~~~~~~~~~~")
    -- require("app.external.platform.Bridge_android")
    -- print("~~~~~~~~~~~~~~~~~~")
    if device.platform == "windows" then
        self:enterScene("LoadingScene")
    else
        self:enterScene("UpdaterScene")
    end
end

return MyApp
