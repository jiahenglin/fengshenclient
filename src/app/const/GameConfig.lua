PLAY_MODE = cc.UserDefault:getInstance():getIntegerForKey("play_mode") or 1 -- 1 双方播报, 2 播放我方战斗战况. 3 播放敌方战斗战况
EQUIP_NUM = 1 -- 当前获取装备部位
LOADING = 0
OPACITY = 0
HEROID = 0 -- 记录获取英雄信息的ID当前
TOUCH_LIMIT = 50 -- 触摸至少50距离才算手势
FORMATIONID = 0
FORMATIONLV = 1
ClipboardText = "神" -- 粘贴板内容
TEXT_END = "\n来自封神之万神来朝"
ENEMYINFO = ""
MIN = 1
MAX = 10
INDEXTABLE = {}
FIGHTTYPE = 1 -- 1 战役   3 封神榜   4  好友对战   5 试炼战场
FORMATIONSETTING = false -- 阵法设置中
FORMATIONCHOSE = 0

ACTIVIED = false
MAKEEQUIP = true -- 装备打造  false  装备穿戴
MAKESTONE = true -- 宝石合成, false 宝石穿戴
EQUIPID = 0
STONEPOS = 1
IOSQQ = "af68ae0185ccb267c2e139c18a1a510f4fda1577362dd37bdcb42ca938d21bc7"
QQKEY = ""
QQ = "236408341" 
if device.platform == "ios" then
    QQKEY = IOSQQ
elseif device.platform == "android" then
    QQKEY = ANDROIDQQ
end

GameVersion = "1.0.2"
