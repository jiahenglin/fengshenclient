local TTSVoice = class("TTSVoice")
local scheduler = require("framework.scheduler")
TTSVoice.speed = 5
if cc.UserDefault:getInstance():getFloatForKey("speed") ~= 0 then
    TTSVoice.speed = cc.UserDefault:getInstance():getFloatForKey("speed")
end
local curSpeed = 0
function TTSVoice.say(str)
    if device.platform == "windows" then
        -- local tts = require "tts"
        -- scheduler.performWithDelayGlobal(
        --     function()
        --         tts.speech(str, TTSVoice.speed)
        --     end,
        --     0.1
        -- )
        print(str)
    elseif device.platform == "android" then
        if curSpeed == TTSVoice.speed then
            curSpeed = 2
        else
            --1/2.5-5/2.5
            curSpeed = TTSVoice.speed/5 + 0.7
        end
        luaj.callStaticMethod("org/cocos2dx/lua/AppActivity", "speech", {str, curSpeed * 10}, "(Ljava/lang/String;I)V")
    elseif device.platform == "ios" then
        if curSpeed == TTSVoice.speed then
            curSpeed = 0
        else
            curSpeed = TTSVoice.speed/2 + 5
        end
        luaoc.callStaticMethod("RootViewController", "speech", {str = str, speed = tostring(curSpeed)})
    end
end

return TTSVoice
