local Monster =
    class(
    "Monster",
    function()
        return display.newNode()
    end
)
local mGameData = require("app.template.gamedata")
function Monster:ctor(data)
    self.maxhp = data[6]
    self.hp = data[6]
    self.team = 2
    self.pos = data[3]
    self.type = data[4]
    self.id = data[5]
    -- if data[1] == GlobalUserItem.id then
    --     self.team = 1
    -- end
    for i, v in ipairs(mGameData.data["monster"]) do
        if self.id == v.id then
            self.name = v.name
            self.tid = v.tid
        end
    end

    local monster = display.newSprite("monster/" .. self.tid .. ".png")
    self.body = monster
    local size = monster:getContentSize()
    self.bodysize = size
    self:addChild(monster)
    self.hpView = cc.CSLoader:createNode("fight/blood.csb")
    self.hpView:setPosition(size.width / 2, size.height - 30):addTo(monster):setScale(0.5)
    self.hpStr = display.newTTFLabel({text = self.hp .. " / " .. self.maxhp, size = 40, color = cc.c3b(255, 255, 255), x = size.width / 2, y = size.height - 30}):addTo(monster)
end

function Monster:refreshHPView()
    local bar = self.hpView:getChildByName("blood"):getChildByName("bar")
    if self.hp < 0 then
        self.hp = 0
    end
    self.hpStr:setString(self.hp .. " / " .. self.maxhp)
    local percent = math.ceil((self.hp / self.maxhp) * 100)
    bar:setPercent(percent)
end

function Monster:injured(hurt)
    local color = cc.c3b(255, 255, 255)
    if hurt ~= 0 then
        local blood = display.newTTFLabel({text = hurt, size = 40, color = cc.c3b(255, 0, 0), x = self.bodysize.width / 2, y = self.bodysize.height}):addTo(self.body)
        color = cc.c3b(255, 0, 0)
        local move3 = cc.MoveBy:create(0.5, cc.p(0, 50))
        local fade = cc.FadeOut:create(0.5)
        local sequence2 = cc.Sequence:create(move3, fade)
        blood:runAction(sequence2)
        self.hp = self.hp + hurt
        if self.hp > self.maxhp then
            self.hp = self.maxhp
        end
        -- self.hpView:setString(self.hp.. " / " .. self.maxhp)
        self:refreshHPView()
    end
    self:setCascadeColorEnabled(true)
    local colorR =
        cc.CallFunc:create(
        function()
            self:setColor(color)
        end
    )
    local colorW =
        cc.CallFunc:create(
        function()
            self:setColor(cc.c3b(255, 255, 255))
        end
    )
    local move1 = cc.MoveBy:create(0.1, cc.p(25, 0))
    local move2 = cc.MoveBy:create(0.1, cc.p(-25, 0))
    local rot = cc.RotateBy:create(0.5, 720)
    local fly = cc.MoveBy:create(0.5, cc.p(1200, 1200))
    local spawn1 = cc.Spawn:create(move1, colorR)
    local spawn2 = cc.Spawn:create(move2, colorW)
    local spawn3 = cc.Spawn:create(rot, fly)
    local sequence = cc.Sequence:create(spawn1, spawn2)
    if self.hp <= 0 then
        self:runAction(cc.Sequence:create(sequence, sequence, spawn3))
    else
        self:runAction(cc.Sequence:create(sequence, sequence))
    end
end

return Monster
