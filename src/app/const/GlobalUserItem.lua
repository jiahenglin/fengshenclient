GlobalUserItem = {}

GlobalUserItem.account = "" --账号
GlobalUserItem.password = "123" --密码
GlobalUserItem.nick = "" --昵称
GlobalUserItem.ap = 0 --体力
GlobalUserItem.arenacnt = 0--封神榜挑战次数
GlobalUserItem.crusade_point = 0 --当前关卡数
GlobalUserItem.gem = 0 --当前金条数
GlobalUserItem.gender = "" --性别
GlobalUserItem.gold = 0 --金币
GlobalUserItem.guid_pont = 0 --引导
GlobalUserItem.id = "" --账号ID
GlobalUserItem.vipLevel = 0 --VIP等级
GlobalUserItem.skill_point = 0 -- 家园技能点
GlobalUserItem.power = 0 -- 战斗力
GlobalUserItem.familyid = -1 --公会ID
GlobalUserItem.shiliancnt = 0 -- 试炼值 
GlobalUserItem.shilianindex = 0 -- 试炼boss
 

function GlobalUserItem.setData(data)
    GlobalUserItem.account = data["name"] --账号
    GlobalUserItem.password = data["password"] --密码
    GlobalUserItem.nick = data["nick"] --昵称
    GlobalUserItem.ap = data["ap"] --体力
    GlobalUserItem.arenacnt = data["arenacnt"] --封神榜挑战次数
    GlobalUserItem.crusade_point = data["crusade_point"] --当前关卡数
    GlobalUserItem.gem = data["gem"] --当前金条数
    GlobalUserItem.gender = data["gender"] --性别
    GlobalUserItem.gold = data["gold"] --金币
    GlobalUserItem.guid_pont = data["guid_pont"] --引导
    GlobalUserItem.id = data["id"] --账号ID
    -- GlobalUserItem.vipLevel = data["vipLevel"] --VIP等级
    GlobalUserItem.skill_point = data["skill_point"]  -- 家园技能点
    GlobalUserItem.familyid = data["familyid"] -- 公会ID
    GlobalUserItem.shiliancnt = data["shiliancnt"]

    cc.UserDefault:getInstance():setStringForKey("account", GlobalUserItem.account)
    cc.UserDefault:getInstance():setStringForKey("password", GlobalUserItem.password)
end
