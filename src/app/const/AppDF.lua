appdf = {}
appdf.EXTERNAL_SRC = "app.external."
local mGameData = require("app.template.gamedata")
function appdf.onHttpJsionTable(url, methon, callback)
    print("appdf.onHttpJsionTable:" .. url)
    local onRequestFinished = function(event)
        local ok = (event.name == "completed")
        local request = event.request
        if not ok then
            -- 请求失败，显示错误代码和错误消息
            print(request:getErrorCode(), request:getErrorMessage())
            return
        end

        local code = request:getResponseStatusCode()
        if code ~= 200 then
            -- 请求结束，但没有返回 200 响应代码
            print(code)
            return
        end

        -- 请求成功，显示服务端返回的内容
        local response = request:getResponseString()
        local data = json.decode(response)
        callback(data)
    end
    network.createHTTPRequest(onRequestFinished, url, methon):start()
end
-- 模型id，数量，类型（1=物品2=装备3=碎片）
-- {tid,num,itemtype,tpl.atk,tpl.def,tpl.hp,tpl.mp,tpl.speed}
-- {tid,num,itemtype}
-- {tid,num,itemtype}
function appdf.sayEquip(data, more)
    -- print("~~~~~~~~~~~~~~sayEquip~~~~~~~~~~~~~~")
    -- dump(data)
    local bmore = more or "true"
    local str = ""
    for i, v in ipairs(data) do
        local a = mGameData.data["equip"][v[1] + 0]
        local num = v[2] + 0
        str = str .. "装备" .. a.name
        if num > 0 then
            str = str .. "数量" .. num
        end
        if bmore == "true" then
            str = str .. "攻击" .. v[4] .. "防御" .. v[5] .. "生命值" .. v[6] .. "魔法值" .. v[7] .. "速度" .. v[8]
        end
    end

    return str
end

function appdf.sayHeroChip(data)
    local str = ""
    for i, v in ipairs(data) do
        local a = mGameData.data["hero"][v[1] + 0]
        str = str .. "英雄碎片" .. a.name .. "数量" .. v[2]
    end
    return str
end

function appdf.sayHero(data)
    local str = ""
    for i, v in ipairs(data) do
        local a = mGameData.data["hero"][v + 0]
        str = str .. "英雄卡牌" .. a.name
        return str
    end
end

function appdf.sayGoods(data)
    local str = ""
    for i, v in ipairs(data) do
        local a = mGameData.data["item"][v[1] + 0]
        str = str .. "物品" .. a.name .. "数量" .. v[2]
    end
    return str
end

function appdf.sayFront(data)
    local str = ""
    for i, v in ipairs(data) do
        local a = mGameData.data["formation_simple"][v[1] + 0]
        str = str .. "阵法" .. a.name .. "数量" .. v[2]
    end
    return str
end

function appdf.sayStone(data)
    local str = ""
    for i, v in ipairs(data) do
        local a = mGameData.data["stone"][v[1] + 0]
        str = str .. a.name .. "数量" .. v[2]
    end
    return str
end

function appdf.gsub(t, repl)
    local r = {}
    local data = {}
    for i, v in ipairs(t) do
        r[i] = {}
        string.gsub(
            v,
            "[^" .. repl .. "]+",
            function(w)
                table.insert(r[i], w)
            end
        )
    end
    for i = 1, #r[1] do
        data[i] = {}
        for k = 1, #t do
            data[i][k] = r[k][i]
        end
    end
    return data
end

function appdf.sayAll(data)
    --  -1=金条0=金币1=灵魂碎片2=装备3=物品4=体力5=战斗值6=技能点7=英雄8=阵法9=宝石
    local str = ""
    for i, v in ipairs(data) do
        if v[1] == "-1" then
            str = str .. "金条数量" .. v[3]
        elseif v[1] == "0" then
            str = str .. "金币数量" .. v[3]
        elseif v[1] == "1" then
            str = str .. appdf.sayHeroChip({{v[2], v[3]}})
        elseif v[1] == "2" then
            str = str .. appdf.sayEquip({{v[2], v[3]}}, false)
        elseif v[1] == "3" then
            str = str .. appdf.sayGoods({{v[2], v[3]}})
        elseif v[1] == "4" then
            str = str .. "体力值" .. v[3]
        elseif v[1] == "5" then
            str = str .. "战斗值" .. v[3]
        elseif v[1] == "6" then
            str = str .. "技能点" .. v[3]
        elseif v[1] == "7" then
            str = str .. appdf.sayHero({v[2]})
        elseif v[1] == "8" then
            str = str .. appdf.sayFront({{v[2], v[3]}})
        elseif v[1] == "9" then
            str = str .. appdf.sayStone({{v[2], v[3]}})
        end
    end
    return str
end

function appdf.createHeroHead(id)
    local item = cc.CSLoader:createNode("common/HeroHead.csb")
    local img = item:getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    img:setTexture("hero/hero_" .. id .. ".png")
    return item
end
function appdf.createShopItem(data)
    local item = cc.CSLoader:createNode("common/ShopItem.csb")
    local img = item:getChildByName("bg"):getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    local name = item:getChildByName("bg"):getChildByName("name")
    local price = item:getChildByName("bg"):getChildByName("price")
    local type = item:getChildByName("bg"):getChildByName("img")
    local scale = 1
    if data["itemtype"] == 1 then
        scale = 0.5
    end
    if data["moneytype"] == 2 then
        type:setTexture("shop/gold.png"):setScale(0.5)
    end
    img:setTexture(data["image"] .. ".png"):setScale(scale)
    name:setString(data["name"])
    price:setString(data["price"])
    return item
end

function appdf.createPayItem(data)
    local item = cc.CSLoader:createNode("common/ShopItem.csb")
    local img = item:getChildByName("bg"):getChildByName("bg"):getChildByName("panel"):getChildByName("img")
    local name = item:getChildByName("bg"):getChildByName("name")
    local price = item:getChildByName("bg"):getChildByName("price")
    local scale = 1

    img:setTexture("common/gold_b.png"):setScale(scale)
    name:setString(data["name"])
    price:setString(data["rmb"])
    return item
end
appdf.BUFF = {
    ["hp"] = "加血加",
    ["hit"] = "受伤害几率加",
    ["speed"] = "速度加",
    ["addmp"] = "上场自带魔法加",
    ["atk"] = "攻击加",
    ["hprate"] = "加血百分之",
    ["hitrate"] = "受伤害几率百分之",
    ["speedrate"] = "速度百分之",
    ["addmprate"] = "上场自带魔法加百分之",
    ["atkrate"] = "攻击加百分之",
    ["equiprate3"] = "武器属性加成增加百分之",
    ["equiprate2"] = "饰品属性加成增加百分之",
    ["equiprate6"] = "腿部属性加成增加百分之",
    ["equiprate4"] = "护盾属性加成增加百分之",
    ["equiprate1"] = "头盔属性加成增加百分之",
    ["equiprate5"] = "上衣属性加成增加百分之"
}

function appdf.buffToString(prop, value)
    local a = appdf.gsub({prop, value}, "|")
    local str = ""
    for i, v in ipairs(a) do
        str = str .. appdf.BUFF[v[1]] .. v[2] .. ","
    end
    return str
end

--随机生成名字
function appdf.randomname()
    local mytime = os.time()
    mytime = string.reverse(mytime)
    math.randomseed(mytime)

    local name = ""
    name =
        mGameData.data["nick"][math.random(#mGameData.data["nick"])]["name1"] ..
        mGameData.data["nick"][math.random(#mGameData.data["nick"])]["name2"] .. mGameData.data["nick"][math.random(#mGameData.data["nick"])]["name3"]
    return name
end

function appdf.getIndex(num)
    local runScene = cc.Director:getInstance():getRunningScene()
    local k = #runScene.currentLayer + 1
    local index
    print("#runScene.currentLayer + 1:" .. k)
    if INDEXTABLE[k] then
        index = INDEXTABLE[k]
    else
        INDEXTABLE[k] = num
        index = INDEXTABLE[k]
    end
    return index
end
-- 表克隆
function appdf.clone(object, deep)
    local copy = {}

    for k, v in pairs(object) do
        if deep and type(v) == "table" then
            v = appdf.clone(v, deep)
        end

        copy[k] = v
    end

    return setmetatable(copy, getmetatable(object))
end
