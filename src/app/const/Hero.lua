local Hero =
    class(
    "Hero",
    function()
        return display.newNode()
    end
)
local mGameData = require("app.template.gamedata")
function Hero:ctor(data)
    self.maxhp = data[7]
    self.maxmp = data[9]
    self.hp = data[6]
    self.mp = data[8]
    self.name = data[10]
    self.team = 2
    self.pos = data[3]
    self.type = data[4]
    self.id = data[5]
    self.lv = data[11]
    self.heroid = data[12]
    self.name = mGameData.data["hero"][self.id].name
    if data[1] == GlobalUserItem.id then
        self.team = 1
    end

    local hero = display.newSprite("hero/hero_" .. self.id .. ".png")
    self.body = hero
    local size = hero:getContentSize()
    self.bodysize = size
    self:addChild(hero)
    self.head = appdf.createHeroHead(self.id)
    self.hpView = cc.CSLoader:createNode("fight/blood.csb")
    self.hpView:setPosition(size.width / 2, size.height - 30):addTo(hero):setScale(0.5)
    self.hpStr = display.newTTFLabel({text = self.hp .. " / " .. self.maxhp, size = 40, color = cc.c3b(255, 255, 255), x = size.width / 2, y = size.height - 30}):addTo(hero)
end

function Hero:refreshHPView()
    local bar = self.hpView:getChildByName("blood"):getChildByName("bar")
    if self.hp < 0 then
        self.hp = 0
    end
    self.hpStr:setString(self.hp .. " / " .. self.maxhp)
    local percent = math.ceil((self.hp / self.maxhp) * 100)
    bar:setPercent(percent)
end

function Hero:injured(hurt)
    local color = cc.c3b(255, 255, 255)

    -- if hurt ~= 0 then
    local color2 = cc.c3b(0, 255, 0)
    if hurt < 0 then
        color2 = cc.c3b(255, 0, 0)
    end
    local blood = display.newTTFLabel({text = hurt, size = 40, color = color2, x = self.bodysize.width / 2, y = self.bodysize.height}):addTo(self.body)
    color = color2
    local move3 = cc.MoveBy:create(0.5, cc.p(0, 50))
    local fade = cc.FadeOut:create(0.5)
    local sequence2 = cc.Sequence:create(move3, fade)
    blood:runAction(sequence2)
    self.hp = self.hp + hurt
    if self.hp > self.maxhp then
        self.hp = self.maxhp
    end
    self:refreshHPView()
    -- end
    self:setCascadeColorEnabled(true)
    local colorR =
        cc.CallFunc:create(
        function()
            self:setColor(color)
        end
    )
    local colorW =
        cc.CallFunc:create(
        function()
            self:setColor(cc.c3b(255, 255, 255))
        end
    )
    local move1 = cc.MoveBy:create(0.1, cc.p(25, 0))
    local move2 = cc.MoveBy:create(0.1, cc.p(-25, 0))
    local rot = cc.RotateBy:create(0.5, 720)
    local fly = cc.MoveBy:create(0.5, cc.p(math.pow(-1, self.team) * 1200, 1200))
    local spawn1 = cc.Spawn:create(move1, colorR)
    local spawn2 = cc.Spawn:create(move2, colorW)
    local spawn3 = cc.Spawn:create(rot, fly)
    local sequence = cc.Sequence:create(spawn1, spawn2)
    if self.hp <= 0 then
        self:runAction(cc.Sequence:create(sequence, sequence, spawn3))
    else
        self:runAction(cc.Sequence:create(sequence, sequence))
    end
end

return Hero
