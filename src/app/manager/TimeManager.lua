--
-- Author: Ysm
-- Date: 2019-07-19 09:50:19
--
local TimeManager = class("TimeManager")
local scheduler = require("framework.scheduler")

function TimeManager:ctor()
	self.tickSd = nil
end

function TimeManager:startTick()
	self.tickSd = scheduler.scheduleGlobal(function() 
        mGameWorld:getNetMgr():send({"tick"})
        end, 1)
end

function TimeManager:stopTick()
	if self.tickSd then
		scheduler.unscheduleGlobal(self.tickSd)
		self.tickSd = nil
	end
end

return TimeManager