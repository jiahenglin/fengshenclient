--
-- Author: Ysm
-- Date: 2019-06-17 15:40:59
--
local EventMamager = class("EventMamager")

function EventMamager:ctor()
	cc(self):addComponent("app.event.EventProtocol"):exportMethods()
end

function EventMamager:addEvent(name,cb)
	return self:addEventListener(name,cb)
end

function EventMamager:remEvent(handle)
	self:removeEventListener(handle)
end

function EventMamager:fire(name,params)
	self:dispatchEvent({name=name,params=params})
end

return EventMamager
