local SoundManager = class("SoundManager")

function SoundManager:ctor()
    -- bgm
    self.musics = {
        ["login_bg"] = "res/sound/bgm/login.ogg",
        ["client_bg3"] = "res/sound/bgm/bg3.ogg",
        ["fight_bg"] = "res/sound/bgm/fight.ogg",
        ["story_0"] = "res/sound/story/0.ogg"
    }
    --eff
    self.sounds = {
        ["login"] = "res/sound/login/login.ogg",
        ["click_double"] = "res/sound/kinds/click_double.ogg",
        ["touch_up"] = "res/sound/kinds/touch_up.ogg",
        ["touch_down"] = "res/sound/kinds/touch_down.ogg",
        ["touch_right"] = "res/sound/kinds/touch_right.ogg",
        ["touch_left"] = "res/sound/kinds/touch_left.ogg",
        ["ap"] = "res/sound/others/ap.ogg",
        ["equip"] = "res/sound/others/equip.ogg",
        ["friend"] = "res/sound/others/friend.ogg",
        ["lose"] = "res/sound/others/lose.ogg",
        ["wait"] = "res/sound/others/wait.ogg",
        ["win"] = "res/sound/others/win.ogg",
        ["monster_1"] = "res/sound/monster/1.ogg",
        ["monster_2"] = "res/sound/monster/2.ogg",
        ["monster_die1"] = "res/sound/monster_die/1.ogg",
        ["monster_die2"] = "res/sound/monster_die/2.ogg",
        ["monster_die3"] = "res/sound/monster_die/3.ogg",
        ["uplv"] = "res/sound/others/uplv.ogg",
        ["upstar"] = "res/sound/others/upstar.ogg",
        ["equipoff"] = "res/sound/others/equipoff.ogg",
        ["xiaoer"] = "res/sound/others/xiaoer.ogg",
        ["equipup"] = "res/sound/others/equipup.ogg",
        ["equipdown"] = "res/sound/others/equipdown.ogg",
        ["getstone"] = "res/sound/others/getstone.ogg",
        ["stoneon"] = "res/sound/others/stoneon.ogg",
    }
    for i = 1, 45 do
        self.sounds["hero_atk_left" .. i] = "res/sound/hero_atk/left/" .. i .. ".ogg"
        self.sounds["hero_atk_right" .. i] = "res/sound/hero_atk/right/" .. i .. ".ogg"
        self.sounds["hero_say_" .. i] = "res/sound/hero/" .. i .. ".ogg"
        self.sounds["hero_skill_left" .. i] = "res/sound/hero_skill/left/" .. i .. ".ogg"
        self.sounds["hero_skill_right" .. i] = "res/sound/hero_skill/right/" .. i .. ".ogg"
        self.sounds["hero_die_left" .. i] = "res/sound/hero_die/left/" .. i .. ".ogg"
        self.sounds["hero_die_right" .. i] = "res/sound/hero_die/right/" .. i .. ".ogg"
    end
    for i = 46, 50 do
        self.sounds["hero_atk_left" .. i .. "_1"] = "res/sound/hero_atk/left/" .. i .. "/1.ogg"
        self.sounds["hero_atk_left" .. i .. "_2"] = "res/sound/hero_atk/left/" .. i .. "/2.ogg"
        self.sounds["hero_atk_right" .. i .. "_1"] = "res/sound/hero_atk/right/" .. i .. "/1.ogg"
        self.sounds["hero_atk_right" .. i .. "_2"] = "res/sound/hero_atk/right/" .. i .. "/2.ogg"
        self.sounds["hero_say_" .. i] = "res/sound/hero/" .. i .. ".ogg"
        self.sounds["hero_skill_left" .. i] = "res/sound/hero_skill/left/" .. i .. ".ogg"
        self.sounds["hero_skill_right" .. i] = "res/sound/hero_skill/right/" .. i .. ".ogg"
        self.sounds["hero_die_left" .. i] = "res/sound/hero_die/left/" .. i .. ".ogg"
        self.sounds["hero_die_right" .. i] = "res/sound/hero_die/right/" .. i .. ".ogg"
        self.sounds["hero_begin" .. i] = "res/sound/hero_begin/" .. i .. ".ogg"
        self.sounds["hero_mid" .. i] = "res/sound/hero_mid/" .. i .. ".ogg"
    end

    for i = 1, 90 do
        local zhang = math.ceil(i / 6)
        local jie = i % 6
        if jie == 0 then
            jie = 6
        end
        self.sounds[zhang .. "_jie_" .. jie] = "res/sound/jie/" .. zhang .. "/" .. jie .. ".ogg"
    end
    for i = 1, 15 do
        self.sounds["zhang_" .. i] = "res/sound/zhang/" .. i .. ".ogg"
    end
end

function SoundManager:init()
    local bgmvol = cc.UserDefault:getInstance():getIntegerForKey("BGMVol")
    local effvol = cc.UserDefault:getInstance():getIntegerForKey("EffVol")
    if bgmvol == 0 then
        bgmvol = 50
    end
    if effvol == 0 then
        effvol = 100
    end
    print("~~~~~~~~~~~~~~~~bgmvol~~~~~~~~~~~~~~~~" .. bgmvol)
    print("~~~~~~~~~~~~~~~~bgmvol~~~~~~~~~~~~~~~~" .. effvol)
    self:setBGMVolume(bgmvol)
    self:setEffVolume(effvol)
    self.loaded = 0
    for _, filename in pairs(self.musics) do
        audio.loadFile(
            filename,
            function()
                self.loaded = self.loaded + 1
                -- print(self.loaded)
            end
        )
    end

    for _, filename in pairs(self.sounds) do
        audio.loadFile(
            filename,
            function()
                self.loaded = self.loaded + 1
                -- print(self.loaded)
                LOADING = self.loaded / (#self.sounds + #self.musics)
            end
        )
    end
end

function SoundManager:playBGM(key, isLoop)
    audio.playBGM(self.musics[key], isLoop or true)
end

function SoundManager:setBGMVolume(vol)
    audio.setBGMVolume(vol / 100)
    cc.UserDefault:getInstance():setIntegerForKey("BGMVol", vol)
end

function SoundManager:getBGMVolume()
    return audio._BGMVolume * 100
end

function SoundManager:setEffVolume(vol)
    audio.setEffectVolume(vol / 100)
    cc.UserDefault:getInstance():setIntegerForKey("EffVol", vol)
end

function SoundManager:getEffVolume()
    return audio._effectVolume * 100
end

function SoundManager:stopBGM()
    audio.stopBGM()
end

function SoundManager:playEff(key, isLoop)
    audio.playEffect(self.sounds[key], isLoop or false)
end

function SoundManager:stopEff()
    audio.stopEffect()
end

function SoundManager:stopAll()
    audio.stopAll()
end

return SoundManager
