module("app.template.data.charge")
data = {
[1]={id = 1,name = '10金条',rmb = 1,gem = 10,des = '获得10金条'},
[2]={id = 2,name = '100金条',rmb = 9,gem = 100,des = '获得100金条'},
[3]={id = 3,name = '500金条',rmb = 42,gem = 500,des = '获得500金条'},
[4]={id = 4,name = '1000金条',rmb = 80,gem = 1000,des = '获得1000金条'},
[5]={id = 5,name = '30元月卡',rmb = 30,gem = 20,des = '连续30天每天获得20金条'},
[6]={id = 6,name = '98元月卡',rmb = 98,gem = 50,des = '连续30天每天获得50金条'},
[7]={id = 7,name = '5000金条',rmb = 400,gem = 5000,des = '获得5000金条'},
[8]={id = 8,name = '10000金条',rmb = 800,gem = 10000,des = '获得10000金条'},
[9]={id = 9,name = '50000金条',rmb = 4000,gem = 50000,des = '获得50000金条'},
}
