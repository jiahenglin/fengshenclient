module("app.template.data.formation_simple")
data = {
[1]={id = 1,fid = 1,name = '筑基阵',itemid = 0,itemnum = 0},
[2]={id = 2,fid = 2,name = '天绝阵',itemid = 2,itemnum = 60},
[3]={id = 3,fid = 3,name = '地烈阵',itemid = 3,itemnum = 60},
[4]={id = 4,fid = 4,name = '风吼阵',itemid = 4,itemnum = 60},
[5]={id = 5,fid = 5,name = '寒冰阵',itemid = 5,itemnum = 60},
[6]={id = 6,fid = 6,name = '化血阵',itemid = 6,itemnum = 60},
[7]={id = 7,fid = 7,name = '烈焰阵',itemid = 7,itemnum = 60},
[8]={id = 8,fid = 8,name = '落魂阵',itemid = 8,itemnum = 60},
[9]={id = 9,fid = 9,name = '红水阵',itemid = 9,itemnum = 60},
[10]={id = 10,fid = 10,name = '红砂阵',itemid = 10,itemnum = 60},
[11]={id = 11,fid = 11,name = '金光阵',itemid = 11,itemnum = 60},
}
