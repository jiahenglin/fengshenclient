module("app.template.data.vip")
data = {
[1]={id = 1,lv = 0,charge = 1},
[2]={id = 2,lv = 1,charge = 100},
[3]={id = 3,lv = 2,charge = 500},
[4]={id = 4,lv = 3,charge = 1000},
[5]={id = 5,lv = 4,charge = 2000},
[6]={id = 6,lv = 5,charge = 5000},
[7]={id = 7,lv = 6,charge = 10000},
[8]={id = 8,lv = 7,charge = 50000},
[9]={id = 9,lv = 8,charge = 100000},
[10]={id = 10,lv = 9,charge = 200000},
[11]={id = 11,lv = 10,charge = 500000},
[12]={id = 12,lv = 11,charge = 800000},
[13]={id = 13,lv = 12,charge = 1200000},
}
