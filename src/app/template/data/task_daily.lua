module("app.template.data.task_daily")
data = {
[1]={id = 1,name = '完成一次初级召唤',param = 'callheroNormal',value = 1,itemtype = 4,itemid = 0,itemnum = 25},
[2]={id = 2,name = '完成十次初级召唤',param = 'callHeroNormal10',value = 10,itemtype = 0,itemid = 0,itemnum = 1500},
[3]={id = 3,name = '完成一次高级召唤',param = 'callHeroHard',value = 1,itemtype = -1,itemid = 0,itemnum = 20},
[4]={id = 4,name = '完成十次高级召唤',param = 'callHeroHard10',value = 10,itemtype = 3,itemid = 22,itemnum = 10},
[5]={id = 5,name = '在一场战役战斗中取得胜利',param = 'cityWin1',value = 1,itemtype = 0,itemid = 0,itemnum = 500},
[6]={id = 6,name = '在六场战役战斗中取得胜利',param = 'cityWin6',value = 6,itemtype = 4,itemid = 0,itemnum = 50},
[7]={id = 7,name = '进行五次封神榜挑战并取得胜利',param = 'arenaWin5',value = 5,itemtype = -1,itemid = 0,itemnum = 80},
[8]={id = 8,name = '进行五次试炼战斗',param = 'shilianWin5',value = 5,itemtype = 6,itemid = 0,itemnum = 10},
[9]={id = 9,name = '使用金币购买一次体力',param = 'buyGoldAp',value = 1,itemtype = 3,itemid = 1,itemnum = 20},
[10]={id = 10,name = '使用金币购买一次挑战值',param = 'buyGoldArena',value = 1,itemtype = 4,itemid = 0,itemnum = 25},
[11]={id = 11,name = '使用金币购买一次试炼值',param = 'buyGoldShilian',value = 1,itemtype = 0,itemid = 0,itemnum = 500},
[12]={id = 12,name = '使用金条购买一次体力',param = 'buyGemAp',value = 1,itemtype = 3,itemid = 25,itemnum = 10},
[13]={id = 13,name = '使用金条购买一次挑战值',param = 'buyGemArena',value = 1,itemtype = 3,itemid = 23,itemnum = 10},
[14]={id = 14,name = '使用金条购买一次试炼值',param = 'buyGemShilian',value = 1,itemtype = 3,itemid = 21,itemnum = 10},
[15]={id = 15,name = '进行一次充值',param = 'charge',value = 1,itemtype = 3,itemid = 20,itemnum = 10},
}
