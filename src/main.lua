local breakInfoFun, xpCallFun = require("LuaDebugjit")("localhost", 7004)
cc.Director:getInstance():getScheduler():scheduleScriptFunc(breakInfoFun, 0.5, false)
function __G__TRACKBACK__(errorMessage)
    print("----------------------------------------")
    print("LUA ERROR: " .. tostring(errorMessage) .. "\n")
    print(debug.traceback("", 2))
    print("----------------------------------------")
    if device.platform == "android" then
        luaj.callStaticMethod("org/cocos2dx/lua/AppActivity", "showcrash", {errorMessage .. "\n" .. debug.traceback()}, "(Ljava/lang/String;)V")
    end
    xpCallFun()
end

package.path = package.path .. ";src/?.lua;src/framework/protobuf/?.lua"
require("app.MyApp").new():run()
